import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[for-id]' // Attribute selector
})
export class ForIdDirective {
  @Input() elemTag: string;
  
  constructor(private el: ElementRef) {
    this.el.nativeElement.setAttribute('style', 'pointer-events: all!important;');
    this.el.nativeElement.addEventListener('click', this.clickLabel.bind(this));
  }

  clickLabel(event: any) {
    if (event.type === 'click') {
      const forId = this.el.nativeElement.getAttribute('for');
      let target:any = document.getElementById(forId).getElementsByTagName(this.elemTag || 'input');
      switch (this.elemTag) { 
        case 'button':
          target[0].click();
          break;
        default:
          target[0].focus();
          break;
      }
    }
  }
}
