import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/account/login/login';
import { SurveyPage } from '../pages/surveys/survey/survey';
import { AnswerProvider } from '../providers/answer/answer';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  // rootPage: any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private answerProvider: AnswerProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#ffffff');
      splashScreen.hide();

      // 로그인에 따라 RootPage 변경
      this.checkLogin();
    });
  }

  checkLogin() {
    if (localStorage.getItem('nfa') || sessionStorage.getItem('nfa')) {
      // TODO : 서베이 테스트시 사용
      // this.nav.setRoot(SurveyPage, {
        //   subjects: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        //   title: '추적관찰',
        //   id: 3
        // })  
        this.answerProvider.getSurveyResult().subscribe(response => {
          let result = response.result;
          switch (result) {
          // TODO : 초기평가 설정
          case 1:
            this.nav.setRoot(SurveyPage, {
              subjects: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
              title: '초기평가',
              id: 1
            })
            break;
          default:
            this.nav.setRoot(HomePage)
            break;
        }
      })
    } else {
      this.rootPage = LoginPage;
    }
  }
}

