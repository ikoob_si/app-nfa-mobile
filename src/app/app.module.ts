import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/account/login/login';
import { JoinPage } from '../pages/account/join/join';
import { JoinPolicyPage } from '../pages/account/join/join-policy/join-policy';
import { JoinInfoPage } from '../pages/account/join/join-info/join-info';
import { JoinSuccessPage } from '../pages/account/join/join-success/join-success';
import { EmailCertPage } from '../pages/account/join/email-cert/email-cert';
import { FindPage } from '../pages/account/find/find';

import { SurveysPage } from '../pages/surveys/surveys';
import { SurveyPage } from '../pages/surveys/survey/survey';
import { SuccessPage } from '../pages/success/success';

import { Part1Page } from '../pages/contents/part1/part1';
import { Part2Page } from '../pages/contents/part2/part2';
import { Part3Page } from '../pages/contents/part3/part3';
import { Part1DetailPage } from '../pages/contents/part1/part1-detail/part1-detail';
import { Part2DetailPage } from '../pages/contents/part2/part2-detail/part2-detail';
import { Part3DetailPage } from '../pages/contents/part3/part3-detail/part3-detail';

import { SettingsPage } from '../pages/settings/settings';
import { PersonalCodePage } from '../pages/settings/personal-code/personal-code';
import { InfoPage } from '../pages/settings/info/info';
import { InfoUpdatePage } from '../pages/settings/info-update/info-update';
import { PwUpdatePage } from '../pages/settings/pw-update/pw-update';
import { SignOutPage } from '../pages/settings/sign-out/sign-out';

import { EnlargeModalPage } from '../pages/enlarge-modal/enlarge-modal';

import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';
import { DirectivesModule } from '../directives/directives.module';
import { PinchZoomModule } from '../components/pinch-zoom/pinch-zoom.module';

import { ApiProvider } from '../providers/api/api';
import { AnswerProvider } from '../providers/answer/answer';
import { UserProvider } from '../providers/user/user';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SignOutInfoPage } from '../pages/settings/sign-out/sign-out-info/sign-out-info';

const pages = [ 
  HomePage,
  LoginPage,
  JoinPage,
  JoinSuccessPage,
  JoinInfoPage,
  JoinPolicyPage,
  EmailCertPage,
  FindPage,
  SettingsPage,
  PersonalCodePage,
  InfoPage,
  InfoUpdatePage,
  PwUpdatePage,
  SignOutPage,
  SignOutInfoPage,
  JoinSuccessPage,
  SurveysPage,
  SurveyPage,
  SuccessPage,
  Part1Page,
  Part1DetailPage,
  Part2Page,
  Part2DetailPage,
  Part3Page,
  Part3DetailPage,
  EnlargeModalPage
]

@NgModule({
  declarations: [
    MyApp,
    ...pages
  ],
  imports: [
    BrowserModule,
    PinchZoomModule,
    HttpClientModule,
    ComponentsModule,
    PipesModule,
    DirectivesModule, 
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      mode: 'ios',
      scrollPositionRestoration: 'top'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ...pages
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiProvider,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AnswerProvider,
    UserProvider,
  ]
})
export class AppModule {}
