import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController, TextInput, AlertController, Keyboard } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { FindPage } from '../find/find';
import { JoinInfoPage } from '../join/join-info/join-info';
import { UserProvider } from '../../../providers/user/user';
import { HomePage } from '../../home/home';
import { SurveyPage } from '../../surveys/survey/survey';
import { AnswerProvider } from '../../../providers/answer/answer';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('emailInput') emailInput: TextInput;
  @ViewChild('password') password: TextInput;
  
  // 로그인 FormGroup  
  auth: FormGroup = new FormBuilder().group({
    // 회원 이메일
    email: ['', Validators.compose([Validators.required])],
    password: ['', Validators.compose([Validators.required])]
  });

  keepLogin: boolean = true; // 로그인 상태유지
  errorModal: any; // 서버에러모달

  // 연결 페이지
  public findAccount = FindPage;
  public joinInfo = JoinInfoPage;

  constructor(
    statusBar: StatusBar,
    private zone: NgZone,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private userProvider: UserProvider,
    private answerProvider: AnswerProvider,
    public keyboard: Keyboard
  ) {
    statusBar.backgroundColorByHexString('#FFFFFF');
    statusBar.styleDefault();
  }

  ionViewDidLoad() {
    this.zone.runOutsideAngular(() => {
      document.getElementById('password').addEventListener('change', (event) => {
        this.auth.patchValue({
          password: event.target['value']
        });
      });
    });
  }


  // 로그인
  doLogin() {
    this.userProvider.login(this.auth.value).subscribe(
      (response: any) => {
        if (this.keepLogin) {
          localStorage.setItem('nfa', JSON.stringify(response));
        } else { 
          sessionStorage.setItem('nfa', JSON.stringify(response));
        }
        this.answerProvider.getSurveyResult().subscribe(response => {
          let result = response.result;
          switch (result) {
            case 1:
              this.navCtrl.setRoot(SurveyPage, {
                subjects: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                title: '초기평가',
                id: 1
              }, { animate: true, direction: 'forward' })
              break;
            case 2:
            default:
              this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'forward' })
              break;
          }
        })
      },
      (error: any) => {
        switch (error.status) {
          case 401: // 비밀번호 불일치
          let alert = this.alertCtrl.create({
            title: '알림',
            message: '일치하는 회원이 없습니다. 다시 시도해 주세요.',
            buttons: [{text: '확인'}]
          });
          alert.present();
            break;
          case 404: // 회원을 찾을 수 없음
            alert = this.alertCtrl.create({
              title: '알림',
              message: '일치하는 회원이 없습니다. 다시 시도해 주세요.',
              buttons: [{ text: '확인' }]
            });
            alert.present();
            break;
          default: // Server Error 400, 500
            alert = this.alertCtrl.create({
              title: '알림',
              message: '일시적인 오류입니다. 잠시후 다시 시도하세요.',
              buttons: [{ text: '확인' }]
            });
            alert.present();
            break;
        }
        console.log(error)
      }
    );
  }
}
