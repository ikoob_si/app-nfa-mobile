import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, TextInput, Events, NavParams, ModalController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../login/login';
import { UserProvider } from '../../../providers/user/user';

@Component({
  selector: 'page-find',
  templateUrl: 'find.html',
})
export class FindPage {
  @ViewChild('fullName') fullName: TextInput;

  
  findTarget: any; // find 페이지 param
  
  findIdForm: FormGroup; // 아이디 찾기 form
  resetPwForm: FormGroup; // 비밀번호 초기화 form

  sendEmail: boolean = false; // 중복 전송 방지
  resend: boolean = false; // 재전송
  errorModal: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public userProvider: UserProvider,
    public events: Events,
  ) {
    this.findTarget = navParams.get('target');

    this.findIdForm = new FormBuilder().group({
      fullName: ['', Validators.compose([Validators.required])],
      birthday: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])]
    });
    this.resetPwForm = new FormBuilder().group({
      email: ['', Validators.compose([Validators.required])],
      phone1: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone2: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone3: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(4)])]
    });
  }

  // 성별 설정
  setGender(type: string) {
    this.findIdForm.get('gender').setValue(type);
  }

  // 아이디 찾기 
  findId() {
    let params = {
      fullName: this.findIdForm.value.fullName,
      birthday: this.findIdForm.value.birthday.replace(/-/gi, ''),
      gender: this.findIdForm.value.gender
    }

    this.userProvider.findId(params).subscribe(
      response => {
        let ids = response;
        let message = '';
        ids.forEach((id: string) => {
          message += id + '<br>';
        });
        let alert = this.alertCtrl.create({
          title: '알림',
          message: '귀하의 아이디는 다음과 같습니다. <ion-label color="allteunGray">' + message + '</ion-label>',
          buttons: [{
            text: '확인',
            handler: () => { 
              this.navCtrl.pop();
            }
          }]
        });
        alert.present();
      }, 
      error => {
        console.error(error);
        switch (error.status) {
          case 400: // Parameter 오류
            break;
          case 404: // 일치하는 회원정보를 찾을 수 없음.
            let alert = this.alertCtrl.create({
              title: '알림',
              message: '입력하신 정보와 일치하는 회원정보를 찾을 수 없습니다.',
              buttons: ['확인']
            });
            alert.present();
            break;
        }
      }
    );
  }

  // 이메일 변경시 다시 시도할 수 있도록 변경
  initForm() {
    this.sendEmail = false;
    this.resend = false;
  }

  // 비밀번호 초기화
  resetPassword() {
    if (this.sendEmail) return;
    this.sendEmail = true;
    this.resend = false;
    let params = {
      email: this.resetPwForm.value.email,
      phone: this.resetPwForm.value.phone1 + this.resetPwForm.value.phone2 + this.resetPwForm.value.phone3
    }
    this.userProvider.resetPw(params).subscribe(
      response => {
        let alert = this.alertCtrl.create({
          title: '알림',
          message: '전송이 완료되었습니다.',
          cssClass: 'hk-alert',
          buttons: [{
            text: '확인',
            handler: () => {
              this.resend = true;
              this.sendEmail = false;
            }
          }]
        });
        alert.present();
      }, 
      error => {
        console.error(error);
        this.resend = false;
        this.sendEmail = false;
        switch (error.status) {
          case 404: // 일치하는 회원정보를 찾을 수 없음.
            let alert = this.alertCtrl.create({
              title: '알림',
              message: '입력하신 정보와 일치하는 회원정보를 찾을 수 없습니다.',
              buttons: ['확인']
            });
            alert.present();
            break;
          default: // Server Error 400, 500
          alert = this.alertCtrl.create({
            title: '알림',
            message: '일시적인 오류입니다. 잠시후 다시 시도해 주세요.',
            buttons: ['확인']
          });
          alert.present();
            break;
        }
      }
      );
  }

  // 로그인 하러가기
  goLogin() {
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
  }

  // 포커스 이동
  inputFocus(type: any) {
    type.setFocus();
  }
}
