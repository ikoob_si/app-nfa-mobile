import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, AlertController, ModalController } from 'ionic-angular';
import * as moment from 'moment';
import { LoginPage } from '../../login/login';
import { JoinPage } from '../join';
import { UserProvider } from '../../../../providers/user/user';

@Component({
  selector: 'page-email-cert',
  templateUrl: 'email-cert.html',
})
export class EmailCertPage {
  @ViewChild('certInput') certInput; // 인증번호 Input Element

  errorModal: any; // 서비스 에러 모달

  email: string = ''; // 이메일
  code: string = ''; // 인증번호

  certData: any; // 인증 데이타
  countInterval: any; // 타이머 Interval
  timer: string; // 남은 시간  
  sendEmailCert: boolean = false; // 인증 이메일 전송 여부
  resendOk: boolean = false; // 재전송 가능
  expired: boolean = false; // 인증시간 만료됨
  differentCode: boolean = false; // 인증번호 일치하지 않음
  addTimeCount: number = 2; // 시간 연장 가능 카운트

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public userProvider: UserProvider,
  ) { }

  ionViewDidLeave() {
    if (this.countInterval) clearInterval(this.countInterval);
    this.initPage();
  }

  // ! 모두 초기화
  // 페이지를 벗어나거나 인증 이메일이 변경되었을 경우 모두 초기화
  initPage() {
    if (!this.sendEmailCert) return; // 인증 메일을 보내지않았을 경우 return
    this.addTimeCount = 2; // 시간연장값 초기화
    this.sendEmailCert = false; // 인증 메일 확인 초기화
    this.code = ''; // 인증번호 초기화
    this.certData = { // 인증데이타 초기화
      code: '',
      expireDate: ''
    }
    // 인증 타이머 취소
    if (this.countInterval) clearInterval(this.countInterval);
    this.timer = ''; // 타이머 초기화
    this.differentCode = false; // 인증실패 내역 초기화
    this.expired = false; // 재인증 확인 초기화
    this.resendOk = false; // 재전송 초기화
  }

  // 이메일 유효성 검사
  invalidEmail() {
    let reg = /[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}/g;
    return reg.test(this.email);
  }

  // 인증번호 유효성 검사
  invalidCert() {
    if (this.expired) return !this.expired;
    let reg = /^[0-9]{4}/;
    return reg.test(this.code);
  }

  // 이메일로 인증번호 요청하기
  sendCertEmail() {
    this.expired = false;
    this.sendEmailCert = false;

    this.userProvider.sendEmail({ target: this.email }).subscribe(
      response => {
        console.log(response)
        this.addTimeCount = 2; // 시간연장값 초기화
        let minutes = 10;
        this.certData = {
          code: response.code,
          expireDate: moment().add(minutes, 'minutes')
        };
        this.expired = false;
        this.sendEmailCert = true;

        // 중복된 이메일이 없다면 인증 타이머 시작
        let interval = 1000;
        // 이미 타이머가 실행되어있다면 취소 후 시작
        if (this.countInterval) clearInterval(this.countInterval);
        this.countInterval = setInterval(() => {
          this.counting();
        }, interval);
        this.certInput.setFocus();
      },
      error => {
        console.error(error);
        switch (error.status) {
          case 409: // 중복된 이메일
            this.alertCtrl.create({
              title: '이메일 중복',
              message: '이미 사용중인 이메일입니다.<br>회원가입여부 및 이메일을 확인해주세요.',
              buttons: ['확인']
            }).present();
            break;
          default: // Server Error 400, 500
            this.alertCtrl.create({
              title: '이메일 인증 실패',
              message: '이메일 인증에 실패하였습니다.<br>다시 한번 시도해주세요.',
              buttons: ['확인']
            }).present();
            break;
        }
      }
    )
  }

  // 시간 연장 최대 20분 연장 가능
  addMinutes() {
    if (this.addTimeCount === 0) return;
    let minutes = 10; // 연장시간 10분
    this.certData.expireDate = moment(this.certData.expireDate).add(minutes, 'minutes');
    this.addTimeCount--;
  }

  // 인증번호 전송 후 Timer
  counting() {
    let now: any = moment();
    let expire: any = moment(this.certData.expireDate)
    let diffTime: any = moment.duration(expire.diff(now));
    if (diffTime.minutes() <= 0) {
      this.timer = diffTime.seconds() + '초';
    } else {
      this.resendOk = diffTime.minutes() < 7; // 재인증 가능시간
      this.timer = diffTime.minutes() + '분 ' + diffTime.seconds() + '초';
    }
    if (diffTime.minutes() <= 0 && diffTime.seconds() <= 0) { // 인증시간 만료
      clearInterval(this.countInterval);
      this.expired = true;
      this.sendEmailCert = false;
      this.differentCode = false;
      this.resendOk = false;
    }
  }

  // 인증번호 확인
  checkCert() {
    if (this.expired) return;
    if (this.sendEmailCert && this.code === this.certData.code) {
      this.differentCode = false;
      clearInterval(this.countInterval);
    this.goNext();
    } else {
      this.differentCode = true;
    }
  }

  // 회원가입 취소 / 뒤로 갈 경우
  cancelSignup() {
    let title = '<span text-left>회원가입 취소</span>';
    let message = '정말 회원가입을 취소하시겠습니까?<br>작성중인 내용을 모두 잃을 수 있습니다.';
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      cssClass: 'hk-alert',
      buttons: [
        {
          text: '네',
          handler: () => {
            this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
          }
        },
        {
          text: '아니오',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }

  // 다음단계로 이동
  goNext() {
    this.navCtrl.push(JoinPage, { email: this.email });
  }

  // 포커스 이동
  inputFocus(type: any) {
    type.setFocus();
  }
}
