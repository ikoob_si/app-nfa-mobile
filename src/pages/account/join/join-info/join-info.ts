import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { JoinPolicyPage } from '../join-policy/join-policy';

@Component({
  selector: 'page-join-info',
  templateUrl: 'join-info.html',
})
export class JoinInfoPage {

  joinInfoCheck: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goJoinPolicy() {
    this.navCtrl.push(JoinPolicyPage)
  }

}
