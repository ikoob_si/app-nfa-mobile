import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';
import { UserProvider } from '../../../providers/user/user';
import { JoinSuccessPage } from './join-success/join-success';

@Component({
  selector: 'page-join',
  templateUrl: 'join.html',
})
export class JoinPage {

  // 로그인 FormGroup 
  formData: FormGroup;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private userProvider: UserProvider
  ) {
    let email = this.navParams.get('email');
    let name = '';
    let birthday = '';
    let gender = '';
    let phone1 = '';
    let phone2 = '';
    let phone3 = '';

    if (!email) {
      let alert = this.alertCtrl.create({
        title: '알림',
        message: '잘못된 접근입니다.<br>처음부터 다시 시도해주세요.',
        cssClass: 'hk-alert',
        buttons: [
          {
            text: '확인',
            handler: () => {
              this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
            }
          }
        ]
      });
      alert.present();
    }

    this.formData = new FormBuilder().group({
      email: [email, Validators.compose([ // 회원 이메일
        Validators.required,
        Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}') // 이메일 형식 검사
      ])],
      password: ['', Validators.compose([Validators.required, Validators.pattern('^(?=.*?[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*?[0-9]).{8,20}')])], // 영문 숫자 특수문자 조합 8~20자 
      confirmPassword: ['', Validators.compose([Validators.required, Validators.pattern('^(?=.*?[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*?[0-9]).{8,20}')])], // 영문 숫자 특수문자 조합 8~20자 
      fullName: [name, Validators.compose([Validators.required])],
      birthday: [birthday, Validators.compose([Validators.required])],
      gender: [gender, Validators.compose([Validators.required])],
      phone1: [phone1, Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone2: [phone2, Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone3: [phone3, Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(4)])]
    });
  }

  // 비밀번호 일치하는지 확인 
  matchingPasswords() {
    let result = false;
    if (!this.formData.controls['confirmPassword'].value.length) {
      result = true;
    }
    if (this.formData.controls['confirmPassword'].value === this.formData.controls['password'].value) {
      result = true;
    }
    return result;
  }

  // 성별 설정
  setGender(gender: string) {
    this.formData.get('gender').setValue(gender);
  }

  // 가입 완료하기
  signup() {
    let params = {
      email: this.formData.controls['email'].value,
      password: this.formData.controls['password'].value,
      fullName: this.formData.controls['fullName'].value.trim(),
      birthday: this.formData.controls['birthday'].value.replace(/-/gi, ''),
      gender: this.formData.controls['gender'].value,
      phone: this.formData.controls['phone1'].value + this.formData.controls['phone2'].value + this.formData.controls['phone3'].value
    }
    this.userProvider.join(params).subscribe(
      (response: any) => {
        response.gender = params.gender;
        response.phone = params.phone;
        this.navCtrl.setRoot(JoinSuccessPage, { patient: response }, { animate: true, direction: 'forward' });
      },
      (error: any) => {
        console.log(error)
      }
    );
  }

  // 회원가입 취소
  cancelSignup() {
    let alert = this.alertCtrl.create({
      title: '회원가입 취소',
      message: '정말 회원가입을 취소하시겠습니까?<br>작성중인 내용을 모두 잃을 수 있습니다.',
      cssClass: 'hk-alert',
      buttons: [
        {
          text: '네',
          handler: () => {
            this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
          }
        },
        {
          text: '아니오',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }
}
