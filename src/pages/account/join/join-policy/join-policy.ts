import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EmailCertPage } from '../email-cert/email-cert';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-join-policy',
  templateUrl: 'join-policy.html',
})
export class JoinPolicyPage {

  all: boolean = false;
  use: boolean = false;
  privacy: boolean = false;;

  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {
  }

  goEmailCert() {
    this.navCtrl.push(EmailCertPage)
  }

  goLink(url) {
    if (!url) return;
    this.iab.create(url);
  }

  validCheck(type: string) {
    switch (type) {
      case 'all':
        this.use = this.all;
        this.privacy = this.all;
        break;
      default:
        this.all = this.use && this.privacy;
        break;
    }
  }

}
