import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../../login/login';

@Component({
  selector: 'page-join-success',
  templateUrl: 'join-success.html',
})
export class JoinSuccessPage {

  login: any = LoginPage

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}
