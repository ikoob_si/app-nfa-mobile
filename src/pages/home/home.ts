import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SettingsPage } from '../settings/settings';
import { SurveysPage } from '../surveys/surveys';
import { Part1Page } from '../contents/part1/part1';
import { Part2Page } from '../contents/part2/part2';
import { Part3Page } from '../contents/part3/part3';
import { AnswerProvider } from '../../providers/answer/answer';
import { SurveyPage } from '../surveys/survey/survey';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public settings = SettingsPage;
  public surveys = SurveysPage;
  public contentPart1 = Part1Page;
  public contentPart2 = Part2Page;
  public contentPart3 = Part3Page;

  public result: any; // * result 값 : 1: 초기평가 필요, 2: 외상평가 필요, 3: 일반 

  constructor(public navCtrl: NavController, public answer: AnswerProvider) { }

  ionViewDidLoad() {
    this.answer.getSurveyResult().subscribe( response => {
      this.result = response.result
    });
  }

  goSurvey() {
    if ( this.result == 2 ) {
      this.navCtrl.push(SurveyPage, {
        subjects: [1, 2, 3, 4],
        title: '외상사건',
        id: 2
      });
    } else this.navCtrl.push(SurveysPage);
  }

}
