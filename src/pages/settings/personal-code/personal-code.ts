import { Component } from '@angular/core';
import { ApiProvider } from '../../../providers/api/api';
import { UserProvider } from '../../../providers/user/user';

@Component({
  selector: 'page-personal-code',
  templateUrl: 'personal-code.html',
})
export class PersonalCodePage {

  patientId: any;
  patient: any;

  constructor(public api: ApiProvider, public userProvider: UserProvider) {
  }
  
  ionViewDidLoad() {
    this.getUser();
  }
  
  // 회원정보 가져오기
  getUser() {
    this.patientId = this.api.getStorageUser().id;
    this.userProvider.getUserInfo(this.patientId).subscribe(
      response => {
        this.patient = response
      },
      error => {
        console.error(error.status);
      }
    );
  }
}
