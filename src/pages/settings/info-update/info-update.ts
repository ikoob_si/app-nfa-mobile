import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from '../../../providers/user/user';
import { ApiProvider } from '../../../providers/api/api';

@Component({
  selector: 'page-info-update',
  templateUrl: 'info-update.html',
})
export class InfoUpdatePage {
  @ViewChild('authNumberInput') authNumberInput;

  formData: FormGroup; // 비밀번호 변경 Form
  patient: any; // 환자정보
  patientId: string;
  phoneAuth: any = {
    auth: true
  };

  oldPatient: any; // 환자 기존 정보
  isChange: boolean = false; // 변경사항이 있는가
  isAuthChange: boolean = false; // 휴대전화번호 변경

  authNumber: string;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public userProvider: UserProvider,
    public events: Events
  ) {

    navParams.get('patient').birthday = navParams.get('patient').birthday.replace(/([0-9]{4})([0-9]{2})([0-9]{2})/, '$1-$2-$3');

    this.oldPatient = Object.assign({}, navParams.get('patient'));
    this.patient = navParams.get('patient');
    this.patientId = this.api.getStorageUser().id;

    if (!this.patient) {
      let alert = this.alertCtrl.create({
        title: '알림',
        message: '잘못된 접근입니다.',
        buttons: [
          {
            text: '확인',
            handler: () => {
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
    }

    this.formData = new FormBuilder().group({
      fullName: [this.patient.fullName, Validators.compose([Validators.required])],
      birthday: [this.patient.birthday, Validators.compose([Validators.required])],
      gender: [this.patient.gender, Validators.compose([Validators.required])],
      phone1: [this.patient.phone.slice(0, 3), Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone2: [this.patient.phone.slice(3, 7), Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(3)])],
      phone3: [this.patient.phone.slice(7, 12), Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/), Validators.minLength(4)])],
    });

    // Form 변경감지
    this.formData.valueChanges.subscribe((evt) => {
      this.isChange = false;
      if (this.oldPatient.fullName !== evt.fullName
        || this.oldPatient.birthday !== evt.birthday
        || this.oldPatient.gender !== evt.gender
        || this.oldPatient.phone !== evt.phone1 + evt.phone2 + evt.phone3
      ) {
        this.isChange = true;
      }
     
    });
  }

  // 성별 설정
  setGender(gender: string) {
    this.formData.get('gender').setValue(gender);
  }

  // 개인정보 변경 취소
  cancelSignup() {
    if (this.isChange) {
      let title = '개인정보 변경 취소';
      let msg = '정말 정보 변경을 취소하시겠습니까?<br>작성중인 내용을 모두 잃을 수 있습니다.';
      let buttons = [
        {
          text: '네',
          handler: () => {
            this.navCtrl.pop();
          }
        },
        {
          text: '아니오',
          role: 'cancel'
        }
      ]
      this.updateAlert(title, msg, buttons);
    } else {
      this.navCtrl.pop();
    }
  }

  phoneNumberChanged() { 
    // 폰 번호 변경시 인증 가능여부 변경 및 기존 인증코드 삭제
    let phone = this.phoneAuth.phone ? this.phoneAuth.phone : this.oldPatient.phone;
    let inputPhone = this.formData.value.phone1 + this.formData.value.phone2 + this.formData.value.phone3;
    if (phone !== inputPhone) { // 새로운 휴대폰 번호로 변경
      delete this.phoneAuth.code;
      this.phoneAuth.auth = false;
      this.isAuthChange = true;
      this.authNumber = '';
    } else { // 기존 번호
      delete this.phoneAuth.phone
      this.phoneAuth.auth = false;
      this.isAuthChange = false;
    }
  }

  // 인증번호 요청
  sendAuthNumber() {
    if (this.formData.value.phone1 && this.formData.value.phone2 && this.formData.value.phone3) {
      let params = {
        patientId: this.patientId,
        phone: this.formData.value.phone1 + this.formData.value.phone2 + this.formData.value.phone3
      }
      this.userProvider.sendTelNumber(params).subscribe( 
        response => {
          this.phoneAuth = response;
          this.phoneAuth.auth = false;
          console.log(response)
          // 인증번호 Focus
          this.authNumberInput.setFocus();
          let title = '휴대폰 인증'
          let msg = '인증번호가 발송되었습니다.';
          this.updateAlert(title, msg, [{ text: '확인' }]);
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  // 인증번호 확인
  checkAuthNumber() {
    let message = '';
    if (!this.phoneAuth) message = '인증번호를 받지 않았습니다.';
    if (this.authNumber && this.authNumber.length > 0) {
      if (this.authNumber == this.phoneAuth.code) {
        message = '인증되었습니다.';
        this.phoneAuth.auth = true;
        this.phoneAuth.phone = this.formData.value.phone1 + this.formData.value.phone2 + this.formData.value.phone3;
      } else {
        message = '인증번호가 일치하지 않습니다.';
        this.phoneAuth.auth = false;
      }
    } else {
      message = '인증번호를 입력해주세요.';
    }

    if (message && message.length > 0) {
      let title = '휴대폰 인증';
      let buttons = [{ text: '확인' }]
      this.updateAlert(title, message, buttons);
    }
  }

  // 개인정보 변경하기
  update() {
    if (this.isChange) {
      let params = {
        fullName: this.formData.value.fullName,
        gender: this.formData.value.gender,
        birthday: this.formData.value.birthday.replace(/-/g, ''),
        phone: this.formData.value.phone1 + this.formData.value.phone2 + this.formData.value.phone3
      }

      if (this.isAuthChange) {
        if (this.phoneAuth && this.phoneAuth.auth) {
          this.userProvider.setUserInfo(params).subscribe( 
            response => {
              let title = '개인정보 변경';
              let msg = '개인정보 변경이 완료 되었습니다.';
              let buttons = [{
                text: '확인',
                handler: () => {
                  this.events.publish('user:update');
                  this.navCtrl.pop();
                }
              }]
              this.updateAlert(title, msg, buttons);
            }),
            error => {
              console.error(error);
            };
        } else {
          let title = '개인정보 변경';
          let msg = '휴대폰 번호를 인증되지 않았습니다.';
          let buttons = [{
            text: '확인',
            handler: () => {
              this.authNumberInput.setFocus();
            }
          }]
          this.updateAlert(title, msg, buttons);
        }
      } else {
        this.userProvider.setUserInfo(params).subscribe( 
          response => {
            let title = '개인정보 변경';
            let msg = '개인정보 변경이 완료 되었습니다.';
            let buttons = [{
              text: '확인',
              handler: () => {
                this.events.publish('user:update');
                this.navCtrl.pop();
              }
            }]
            this.updateAlert(title, msg, buttons);
          }),
          error => {
            console.error(error);
          };
      }
    } else {
      let title = '개인정보 변경';
      let msg = '변경된 내용이 없습니다.';
      this.updateAlert(title, msg, [{ text: '확인' }]);
    }
  }

  updateAlert(title: string, msg: string, buttons: any[]) {
    let alert = this.alertCtrl.create({
      title: '개인정보 변경',
      message: msg,
      buttons: buttons
    });
    alert.present();
  }
}
