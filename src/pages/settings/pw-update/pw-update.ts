import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';

@Component({
  selector: 'page-pw-update',
  templateUrl: 'pw-update.html',
})
export class PwUpdatePage {

  formData: FormGroup;// 비밀번호 변경 Form
  oldPasswordError: boolean = false; // 현재 비밀번호 불일치

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public userProvider: UserProvider
  ) {
    this.formData = new FormBuilder().group({
      oldPassword: ['', Validators.compose([Validators.required])], // 영문 숫자 특수문자 조합 8~20자 
      password: ['', Validators.compose([Validators.required, Validators.pattern('^(?=.*?[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*?[0-9]).{8,20}')])], // 영문 숫자 특수문자 조합 8~20자 
      confirmPassword: ['', Validators.compose([Validators.required, Validators.pattern('^(?=.*?[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*?[0-9]).{8,20}')])], // 영문 숫자 특수문자 조합 8~20자 
    });
  }

  // 비밀번호 일치하는지 확인
  matchingPasswords() {
    let result = false;
    if (!this.formData.controls['confirmPassword'].value.length) {
      result = true;
    }
    if (this.formData.controls['confirmPassword'].value === this.formData.controls['password'].value) {
      result = true;
    }
    return result;
  }

  // 비밀번호 변경 
  setPassword() {
    let params = {
      oldPassword: this.formData.value.oldPassword,
      password: this.formData.value.password
    }
    this.userProvider.setPw(params).subscribe(
      response => {
        let alert = this.alertCtrl.create({
          title: '비밀번호 변경',
          message: '비밀번호 변경이 완료 되었습니다.<br>비밀번호는 보안을 위해서 6개월 단위로 변경을 해주시길 바랍니다.',
          buttons: [{
            text: '확인',
            handler: () => { 
              this.navCtrl.pop();
            }
          }]
        });
        alert.present();
      },
      error => {
        console.error(error);
        switch (error.status) {
          case 400: //  Parameter 오류
            break;
          case 401: // 인증 토큰 오류 및 비밀번호 불일치
            this.alertCtrl.create({
              title: '알림',
              message: '현재 비밀번호가 일치하지 않습니다.<br>다시 시도해주세요.',
              buttons: ['확인']
            }).present();
            break;
          case 404: // 회원정보 없음
            break;
        }
      }
    );
  }

}
