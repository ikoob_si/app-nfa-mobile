import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../../providers/user/user';
import { LoginPage } from '../../account/login/login';

@Component({
  selector: 'page-sign-out',
  templateUrl: 'sign-out.html',
})
export class SignOutPage {

  public password: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public userProvider: UserProvider,
    public alertCtrl: AlertController) {
  }
  
  signout() {
    let params = { password: this.password }
    this.userProvider.signout(params).subscribe(
      response => {
        this.alertCtrl.create({
          title: '알림',
          message: '회원탈퇴가 완료되었습니다.',
          buttons: [{
            text: '확인',
            handler: () => { 
              this.userProvider.logout().subscribe(
                response => {
                  this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back' });
                },
                error => [
                  console.error(error)
                ]
              )
            }
          }]
        }).present();
      },
      error => {
        console.error(error);
        switch (error.status) {
          case 401: 
          case 404: // 일치하는 회원정보를 찾을 수 없음.
            let alert = this.alertCtrl.create({
              title: '알림',
              message: '입력하신 정보와 일치하는 회원정보를 찾을 수 없습니다.',
              buttons: ['확인']
            });
            alert.present();
            break;
          default: // Server Error 400, 500
          alert = this.alertCtrl.create({
            title: '알림',
            message: '일시적인 오류입니다.<br>잠시후 다시 시도해 주세요.',
            buttons: ['확인']
          });
          alert.present();
            break;
        }
      }
    )
  }

}
