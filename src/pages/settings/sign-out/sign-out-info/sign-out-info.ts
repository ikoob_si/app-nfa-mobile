import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignOutPage } from '../sign-out';

@Component({
  selector: 'page-sign-out-info',
  templateUrl: 'sign-out-info.html',
})
export class SignOutInfoPage {

  checkSignoutInfo: boolean = false;

  constructor(public navCtrl: NavController) {
  }

  goSignout() {
    this.navCtrl.push(SignOutPage)
  }
}
