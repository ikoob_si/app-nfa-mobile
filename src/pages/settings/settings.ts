import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PersonalCodePage } from './personal-code/personal-code';
import { InfoPage } from './info/info';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  public personalCode = PersonalCodePage;
  public info = InfoPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {

  }

  goLink(url) {
    if (!url) return;
    this.iab.create(url);
  }
}