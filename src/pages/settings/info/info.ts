import { Component } from '@angular/core';
import { Events, NavController, NavParams } from 'ionic-angular';
import { InfoUpdatePage } from '../info-update/info-update';
import { UserProvider } from '../../../providers/user/user';
import { LoginPage } from '../../account/login/login';
import { ApiProvider } from '../../../providers/api/api';
import { PwUpdatePage } from '../pw-update/pw-update';
import { SignOutInfoPage } from '../sign-out/sign-out-info/sign-out-info';

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {

  public infoUpdate: any = InfoUpdatePage;
  public pwUpdate: any = PwUpdatePage;
  public signoutInfo: any = SignOutInfoPage;

  public patient: any;
  public patientId: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private userProvider: UserProvider,
    public events: Events
  ) {
    events.subscribe('user:update', () => {
      this.getUser();
    });
  }

  ionViewDidLoad() {
    this.getUser();
  }
  
  // 회원정보 가져오기
  getUser() {
    this.patientId = this.api.getStorageUser().id;
    this.userProvider.getUserInfo(this.patientId).subscribe(
      response => {
        this.patient = response
      },
      error => {
        console.error(error.status);
      }
    );
  }

  goInfoUpdate() {
    this.navCtrl.push(this.infoUpdate, { patient: this.patient });
  }

  logout() {
    this.userProvider.logout().subscribe(() => {
      this.navCtrl.setRoot(LoginPage, {}, { animate: true, direction: 'back'});
    });
   }

  goSignout() {
    this.navCtrl.push(this.signoutInfo);
  }
}
