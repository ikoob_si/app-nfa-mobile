import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Part3Page } from '../part3';
import { EnlargeModalPage } from '../../../enlarge-modal/enlarge-modal';
import { HomePage } from '../../../home/home';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-part3-detail',
  templateUrl: 'part3-detail.html',
})
export class Part3DetailPage {

  public contentPart3 = Part3Page;
  public contentDetail = Part3DetailPage;
  public selectedContent: string;
  public selectedContentFilter: any;
  public subcontent: any;


  contents: any = {
    "1" : [
      {
        "title" : "1. 뇌기능 저하 예방법",
        "subtitle_bubble_q" : "자가검진 해보니 아직 증상은 없습니다. 혹시 지금도 관리가 필요한가요? 뇌기능 저하 예방법이 궁금합니다.",
        "content" : "스트레스에 노출되면 당장은 여러 가지 뇌기능 저하 증상이 없을 수 있지만, 장기간 노출될 경우 우울, 불면, 음주문제, 인지기능 저하 등 뇌기능 저하 증상을 겪을 수 있습니다. 누구나 지속적인 스트레스를 겪을 수 있으며, 스트레스로 인한 여러 증상을 미리 관리하는 것이 필요합니다. 간단한 스트레스 대처 훈련이나 가벼운 명상을 통해 스트레스 증상을 완화시켜 관련 질환으로 발전하는 것을 방지할 수 있습니다.<br><br>소방공무원은 일반인들이 흔히 겪지 않는 다양한 사건사고에 자주 노출될 수 있습니다. 따라서 충분히 힘들 수 있는 사건을 겪었음을 인정해야 합니다<sup>181</sup>. 이러한 경험 이후에 정서적·신체적 반응이 생기는 것은 당연한 현상입니다. 사고 현장이 계속 생각나거나 각성상태가 유지된다고 해서 약하거나 문제가 있는 것이 아니라, 충분히 그럴 수 있는 현상임을 인지한다면 스트레스 대처에 도움이 될 수 있습니다.<br><br>더 나아가, 온전히 휴식할 수 있는 시간을 만들어보는 것도 좋습니다. 음악을 듣거나 산책을 해도 좋고, 명상이나 요가, 점진적 근육 이완법, 호흡 운동 등 긴장을 풀 수 있는 방법을 배우는 것도 좋습니다. 그러나 상황을 견디기 위해서 술을 과하게 마시거나 담배를 피우는 행동은 더 장기적인 건강 문제를 야기할 수도 있으니 최대한 줄이는 것이 좋습니다. 스스로를 위해 건강한 습관을 길러보는 것을 권해드립니다. 충분한 휴식과 규칙적인 운동, 건강한 식습관은 정신건강과 밀접하게 연관되어 있습니다.<br><br>소방재난본부에서는 현장 출동이 잦거나, 직무 스트레스가 많은 소방공무원을 대상으로 힐링캠프를 운영하고 있습니다<sup>182</sup>. 캠프에서는 자연 친화적인 환경에서 심리적 안정감을 되찾고 신체 피로를 회복하며 신체활동을 통해 스트레스를 해소합니다. 또한 명상을 통해 스트레스에 대처하는 방법을 배울 수 있습니다. 이렇게 스트레스가 누적되지 않게 적절한 외부활동을 하는 것도 여러 질환 예방에 좋은 방법입니다.<br><br>혹시 스트레스를 받는다고 느껴지면 믿을 수 있는 사람과 이야기를 나눠보는 것을 권해드립니다<sup>183</sup>. 친구나 동료 또는 배우자나 가족, 의지할 수 있는 사람과의 대화를 통해 스스로 스트레스에 대처할 수 있는 힘을 기를 수 있습니다. 또한 업무 상 겪었던 일을 설명하기 쉽지 않을 수 있지만 혼자 견뎌야 하는 문제는 아닙니다. 많은 소방공무원이 같은 증상을 겪을 수 있고 선생님만의 문제가 아닙니다. 만약 동료 중 누군가 스트레스로 고민한다면 의지할 수 있는 동료가 되어주세요. 누구보다 서로를 잘 이해할 수 있고 공통된 경험을 공유하는 동료와의 좋은 관계는 스트레스에 대처하는 데 큰 도움이 됩니다.<br><br>소방공무원은 일반인들에 비해 크고 작은 스트레스에 쉽게 노출될 수 있으므로, 이러한 점을 염두에 두고 소소한 스트레스 해소법을 익히시는 것을 권해드립니다. 또한 누적된 스트레스는 다양한 건강 문제를 일으킬 수 있으므로 전문적인 치료가 필요합니다<sup>184</sup>. 이에 따라 소방공무원을 위한 정신 및 신체 건강 관리 프로그램을 운영하고 있습니다.<br><br>먼저, 전국적으로 300여 개의 안전센터에서 소방공무원을 위한 심신안정실이 운영되고 있습니다<sup>185</sup>. 심신안정실은 소방공무원 보건안전 및 복지 기본법 제3조(국가 등의 책무)에 근거하여, 사고 및 재난 현장에 지속적으로 노출된 소방공무원이 즉시 심리적 안정을 취하고 외상후스트레스장애 등에 대한 발병을 방지하기 위해 운영되고 있습니다<sup>186</sup>. 운영 지점에 따라 차이가 있을 수는 있으나 스트레스 측정기, 혈압 등의 활력 징후를 측정할 수 있는 자가 측정실, 산소 발생기와 척추 온열기 및 저주파 치료기, 안마 의자 등이 배치되어 있는 바디케어존, 조명 장치와 아로마, 자연영상장치와 기능성 음악 장치가 설치된 멘탈케어존, 동료와 함께 힘든 일을 나누고 격려할 수 있는 소통 공간이 마련되어 있습니다. 이러한 공간을 통해 근무 중 경험할 수 있는 고도의 긴장과 피로를 해소할 수 있습니다<sup>187</sup>. 심신안정실을 통해 자가측정 → 신체 안정 → 심리 안정 → 소통의 단계를 거치며 조금씩 누적되는 스트레스를 해소하는 것은 체계적인 정신건강 관리의 시작이 될 수 있습니다.<br><br>매년 일부 소방서를 선정하여 찾아가는 심리상담실도 운영하고 있습니다<sup>188</sup>. 화재, 구조, 구급 등의 현장 출동이 많은 소방서 또는 다수의 인명피해가 발생하는 등 대형사고가 발생한 지역의 소방서, 그리고 정신건강 설문조사 결과 위험군이 다수 근무하는 소방서를 선정합니다. 정신건강 전문의와 심리상담전문가가 소방서를 직접 방문하여 전 직원을 대상으로 외상후스트레스장애를 비롯한 정신건강 관련 통합교육 진행하고, 다양한 검사도구를 활용한 정신건강을 체크합니다. 이 결과를 안내하면서 1:1 상담을 진행하고, 고위험군이나 참여자를 대상으로 심층심리상담 또는 집단프로그램을 진행하기도 합니다. 이렇게 전문가를 통해 선생님의 상태를 객관적으로 체크해보고 필요한 경우 추가 상담이나 치료를 받으실 수 있습니다.<br><br>이후 추가적인 안정이 필요한 소방공무원을 대상으로 진행하는 캠프 프로그램도 마련되어 있습니다<sup>189</sup>. 개인별로 스트레스를 측정하고 담당하는 업무와 외상후스트레스장애, 수면장애 등의 질환과의 연관성에 대한 교육을 진행합니다. 또한 심신 안정화와 이완요법 등 불안정한 심리를 극복할 수 있는 방법을 배우고 미술, 음악 등의 예술 활동을 통한 심리치료와 집단상담을 진행합니다. 혹시 정신건강의학과 또는 전문 상담소에서 치료가 필요하신 경우에는 소방공무원이 지불한 병원비 및 약제비를 소방청에서 지원하고 있으니<sup>190</sup>, 신속하고 적극적으로 치료를 받으시기를 권해드립니다.<br>혹시 일상생활에서 긴장, 예민함, 의욕 저하, 우울감 등의 불편감을 겪고 있지만, 신변노출이 꺼려져 치료를 망설이신다면, 소방공무원을 위한 안심 프로그램 이용을 권해드립니다. 안심 프로그램 협약 병원에서는 진료비를 무기명 청구하기 때문에 신원 노출 등의 염려 없이 진료를 받을 수 있습니다. 병원에 따라 달라질 수 있지만, 인지행동치료, 경두개 자기자극술 등의 비약물치료와 증상을 조절하는 약물치료를 받을 수 있습니다.",
        "reference" : "<sup>181</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013). Recovery after Trauma – A Guide for People with Posttraumatic Stress Disorder<br><sup>182</sup> 소방재난본부, (2017). 소방공무원 힐링캠프 운영 계획<br><sup>183</sup> Norwood et al., (2012). Recognizing and combating firefighter stress. Fire Engineering.<br><sup>184</sup> Gulliver et al., (2019). Behavioral health programs in fire service: Surveying access and preferences. Psychol. Serv., 340.<br><sup>185</sup> 국회의원 김병관 보도자료, (2018)<br><sup>186</sup> 국민안전처 정책설명자료, (2015). 소방공무원 심신건강관리사업의 내실화 및 고도화<br><sup>187</sup> 경찰청, (2017). 경찰, 소방, 해경 정신건강사업 통합·운영방안 연구용역 최종보고서<br><sup>188</sup> 국민안전처 보도자료, (2016). 소방공무원‘찾아가는 심리상담실’확대·운영한다<br><sup>189</sup> 국민안전처 정책설명자료, (2016). 소방공무원 PTSD, 우울증 등 심리장애 해소<br><sup>190</sup> 경찰청, (2017). 경찰, 소방, 해경 정신건강사업 통합·운영방안 연구용역 최종보고서"
      }
    ],
    "2-2-1" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "화재현장 담당",
        "subtitle_bubble_q" : "유해물질 노출되었을 때 치료 방법에는 무엇이 있나요?",
        "content" : "화재 현장 출동을 담당하는 소방공무원은 일산화탄소, 시안화수소, 질산가스를 포함한 다양한 유해가스에 노출되기 쉽습니다. 이렇게 많은 양의 유해가스에 노출되면 호흡기 염증을 유발할 수 있고 급성 또는 만성 호흡기 질환까지 이어질 수 있습니다<sup>191</sup>.<br>유해가스 흡입을 예방하기 위하여 보호장구를 철저히 사용하는 것이 가장 중요합니다. 만약 유해가스에 노출되었다면, 호흡기계 손상이 발생하지는 않았는지 또는 지속되는 증상이 있는지 등 의료기관에서 면밀한 검사를 받아볼 필요가 있습니다.<br><br>유해가스 흡입 이후 의료기관을 방문하게 되면, 현재 주요한 증상을 먼저 확인합니다<sup>192</sup>. 노출된 유해가스의 종류에 따라 두통, 어지러움, 무력감, 오심, 구토, 비정상적인 호흡이나 심박동 등이 발생할 수 있습니다. 이러한 경우 해독 및 약물 치료가 필요할 수 있으니 가벼운 증상이라도 있는 경우, 즉시 의료기관에 방문해야 합니다.<br><br>또한 혈압, 맥박 등 활력징후 측정, 식욕부진, 불면, 우울 또는 불안이나 주의력과 같은 정신건강에 대한 검사를 진행하여 전신 상태를 평가합니다. 증상에 따라 추가적으로 심혈관, 호흡기, 신경, 이비인후과, 안과, 피부과 검진이 필요할 수 있습니다.<br><br>유해가스 흡입의 대표적인 치료법으로는 고압산소요법이 있습니다.<br><br>고압산소요법은 대기압보다 높은 기압에서 일정 시간 동안 고농도의 산소를 흡입하여 혈액 중의 산소 농도를 높이고 신체 내 원활한 산소공급을 유도하는 치료법입니다<sup>193</sup>. 일산화탄소 중독의 경우에는 혈액 속 헤모글로빈과 결합한 일산화탄소가 신체 조직이 필요한 산소 공급을 방해하여 뇌와 심장 및 신체 여러 조직에 손상을 야기할 수 있습니다<sup>194</sup>. 일산화탄소가 산소보다 헤모글로빈과의 결합력이 높기 때문에, 대기압에서는 산소를 공급한다고 해도 혈중 산소 농도를 높이기 어렵습니다. 하지만 대기압보다 높은 압력에서 높은 농도의 산소를 공급하면, 헤모글로빈과 산소의 결합을 증가시킬 수 있어 체내 산소 공급을 원활하게 할 수 있습니다.<br><br>고압산소요법은 일산화탄소 중독으로 인한 인지기능 저하 등 신경학적 후유증의 진행을 예방한다고 알려져 있습니다<sup>195</sup>.<br><br>미국 잠수고압의학회(undersea and hyperbaric medicine society, UHMS)에서는 최소 2절대기압 (atmosphere absolute, ATA)이상의 압력을 가하는 방법을 치료적 고압산소요법으로 정의하고 있고<sup>196</sup>, 우리나라에서는 일산화탄소 중독 치료를 위해 초기 45분 동안 3기압으로 100% 산소를 공급하고, 이후 2기압으로 2시간 또는 혈액 속의 일산화탄소 결합 헤모글로빈 농도가 10% 미만으로 떨어질 때까지 산소를 투여합니다<sup>197</sup>.<br>고압산소요법은 안전성이 입증된 치료법입니다.<br><br>다만, 몇 가지 상황에서는 가능 여부를 미리 확인하는 것이 필요할 수 있습니다. 폐소공포증이 있는 경우 좁은 공간인 단실 챔버에서 처치를 받는 것이 두려울 수 있습니다. 치료하지 않은 폐질환이 있는 경우에는 고압산소요법 처치를 받는 것이 가능한지 전문가의 확인이 필요합니다.",
        "reference" : "<sup>191</sup> Rosenstock et al., (1990). Respiratory mortality among firefighters. Br J Ind Med, 462-5.<br><sup>192</sup> 윤성용 외, (2017). 사고대비 화학물질 응급처치 지침서 Ⅱ(응급의료진용). 순천향대학교 구미병원 환경보건센터<br><sup>193</sup> Gill et al., (2014). Hyperbaric oxygen: its uses, mechanisms of action and outcomes. QJM, 385-95.<br><sup>194</sup> Kao et al., (2006). Toxicity associated with carbon monoxide. Clin. Lab. Med, 99-125.<br><sup>195</sup> Thom et al., (1995). Delayed neuropsychologic sequelae after carbon monoxide poisoning: prevention by treatment with hyperbaric oxygen. Ann. Emerg. Med, 474-80.<br><sup>196</sup> 허탁 외, (2019). 고압산소치료기 적정 배치 방안 정책연구(정책연구사업 최종결과보고서). 보건복지부, 대한고압의학회<br><sup>197</sup> 임상술기교육연구회, (2012). 임상술기. 군자출판사."
      },
    ],
    "2-2-2" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "화재현장 담당",
        "subtitle_bubble_q" : "새롭게 개발된 치료 방법에는 무엇이 있나요?",
        "subtitle_text" : "고압산소요법 (Hyperbaric Oxygen Therapy, HBOT)",
        "content" : "소방공무원은 화재 현장에서 일산화탄소, 시안화수소를 비롯한 여러 유해가스에 노출되어 중독 증상을 호소할 수 있습니다.<br><br>또한 이러한 유해가스는 뇌 내 산소 공급을 방해하여 혈류 저하에 취약한 뇌 부위를 손상시킬 수 있습니다.<br><br>앞서 일산화탄소중독 치료에 적용하는 것으로 소개해드린 고압산소요법은, 혈액 중의 산소 농도를 높여 조직과 장기로의 산소 전달을 용이하게 합니다<sup>198</sup>.<br>세포들은 산소가 충분한 상태에서 에너지를 낼 수 있어, 산소가 부족했던 조직에 산소를 충분히 전달하여 기능을 회복할 수 있도록 도울 수 있습니다<sup>199</sup>.<br>고압산소요법은 세포 사멸을 감소시키고, 성장인자 및 항산화제 발현을 높이며, 염증성 사이토카인 분비를 억제한다고 알려져 있습니다.<br><br>고압산소요법은 일산화탄소 중독, 당뇨 발, 상처 치료 등에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받아 쓰이고 있는 안전하고 효과적인 치료법입니다.<br><br>더 나아가, 고압산소요법은 뇌기능 개선을 도울 수 있다는 점이 최근 제기되고 있습니다.<br><br>신체 장기 중 뇌는 체중의 5% 정도에 불과하지만, 뇌는 체내 산소 중 약 25%를 소비하고 기초 대사량 중 약 20%를 차지할 정도로 대사가 활발한 기관입니다<sup>200</sup>. 기초연구를 통해 고압산소요법이 신경보호효과를 보이는 것으로 알려져, 여러 질환에서 고압산소요법의 치료 효과를 입증하기 위한 임상 연구들이 진행되고 있습니다.<br>외상성 뇌손상, 외상후스트레스장애, 돌발성 난청 등의 증상 개선 및 인지기능 개선에 고압산소요법이 시도되고 있습니다.<br><br>서울소방재난본부는 소방공무원을 대상으로 고압산소요법을 적용하는 시범사업을 진행하였습니다.<br><br>고압산소요법 적용의 편의성과 효율성을 높이기 위해 본부 내에 고압산소요법 기기를 설치하여 연구를 진행한 결과 소방공무원에서 고압산소요법의 긍정적인 효과를 확인하였습니다.<br>소방공무원들은 고압산소요법이 수면의 질 개선, 피로 회복, 집중력 향상 등에 도움이 된다고 보고하였습니다. 또한 고압산소요법 시행 후 뇌 혈류 증가를 확인한 연구결과가 있어<sup>201</sup>, 고압산소요법의 뇌기능 개선 효과를 기대할 수 있습니다.<br>또한 환자 개인별 상태에 따른 맞춤형 고압산소요법 시행 방법도 개발되고 있습니다.<br><br>최근 심박변이도(Heart Rate Variability, HRV), 맥박, 혈압, 뇌전도(Electroencephalography, EEG), 피부전도도(Skin Conductance Response, SCR) 등의 생리적 지표를 조합하여 고압산소요법 처치 중인 환자의 불안 수준을 실시간으로 감지하고, 이를 기반으로 공기의 압력을 실시간으로 제어할 수 있는 제어 장치 및 이를 포함하는 고압 산소 치료 시스템에 대한 특허가 등록되었습니다<sup>202</sup>. 이를 통해 고압산소요법 처치 중에 생길 수 있는 부작용을 최소화하고 치료 효과를 향상시킬 것으로 기대됩니다.",
        "reference" : "<sup>198</sup> Eve et al., (2016). Hyperbaric oxygen therapy as a potential treatment for post-traumatic stress disorder associated with traumatic brain injury. Neuropsychiatr Dis Treat, 2689-705.<br><sup>199</sup> Edwards, (2010). Hyperbaric oxygen therapy. Part 2: application in disease. J. Vet. Emerg. Crit. Care, 289-97.<br><sup>200</sup> Mink et al., (1981). Ratio of central nervous system to body metabolism in vertebrates: its constancy and functional basis. Am J Physiol, 203–12.<br><sup>201</sup> Golden et al., (2002). Improvement in cerebral metabolism in chronic brain injury after hyperbaric oxygen therapy. Int. J. Neurosci., 119-31.<br><sup>202</sup> 이화여자대학교 산학협력단. (2020). 특허등록 제 10-2077372. 서울:특허청.",
        "img_top" : "part3_2-1.png",
        "img_top_size" : "full"
      },
    ],
    "2-2-3" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "화재현장 담당",
        "subtitle_bubble_q" : "일상생활에서도 도움이 되는 것들이 있나요?",
        "content" : "소방공무원은 화재현장에서의 유해가스 노출을 비롯하여 각종 위험 상황에 긴급 투입되는 등 정신적인 스트레스에도 자주 노출됩니다. 이처럼 소방공무원의 흡연이나 음주<sup>203</sup>는 직무상 스트레스와 연관이 있을 수 있습니다.<br><br>소방공무원은 화재진압과정에서 노출되는 많은 유해가스로 인해 폐기능 저하와 여러 호흡기 증상을 호소할 수 있습니다.<br>호흡기 증상은 유해가스 노출뿐만 아니라 흡연의 영향도 있을 수 있습니다. 담배는 4,000개 이상의 독성물질과 발암물질을 포함하고 있어 만성 기관지염, 폐기종, 폐암 등 여러 호흡기 질환을 유발할 수 있습니다.<br>따라서 금연은 화재 진압 시 호흡기 보호구의 착용만큼이나 호흡기 질환 예방에 도움이 될 수 있습니다<sup>204</sup>.<br><br>또한, 술을 마시게 되면 순간적으로 근무 중에 있었던 상황으로부터 벗어나 기분을 좋아지게 할 수 있으나, 습관적인 음주는 뇌 손상과 기능 장애의 원인이 될 수 있고, 인지 장애, 소뇌 퇴화, 알코올성 치매 등 신경정신과적 증상을 초래할 수 있습니다<sup>205</sup>.<br>또한 알코올이 몸 안에서 분해되는 과정에서 생기는 아세트알데히드는 신경에 직접적인 영향을 주는 독성을 가진 것으로 알려져 있습니다. 따라서 습관적인 음주는 인지 기능 장애를 비롯한 신경학적 이상이나 간 질환의 원인이 될 수 있습니다.<br><br>음주 유형은 ‘적정 음주’와 ‘위험 음주’로 구분할 수 있는데, ‘적정 음주’는 음주량과 패턴을 모두 감안하였을 때, 자신과 타인에게 해가 되지 않는 수준의 음주를 말합니다<sup>206</sup>.<br>세계보건기구(World Health Organization, WHO)에 따른 하루 적정 음주 범위를 알코올 섭취량으로 환산해보면, 남자는 소주 약 3잔에 해당하는 하루 40g 미만이고 여자는 소주 약 2잔에 해당하는 20g입니다.<br><br>그러나 최근에는 소량의 음주도 다양한 암 발생 위험을 높일 수 있다는 연구결과가 발표되었으므로 음주는 최대한 지양하시는 것을 권해드립니다.",
        "reference" : "<sup>203</sup> 조선덕 외, (2012). 소방공무원의 직무스트레스와 음주사용장애와의 관련성. 한국방재학회, 133-40.<br><sup>204</sup> 김성훈 외, (2006). 부산지역 소방공무원의 폐기능과 호흡기증상. 대한직업환경의학회지, 103-11.<br><sup>205</sup> Butterworth, (1995). Pathophysiology of alcoholic brain damage: synergistic effects of ethanol, thiamine deficiency and alcoholic liver disease. Metab. Brain Dis., 1-8.<br><sup>206</sup> 질병관리본부 건강정보. (20200707). URL: <a href='http://health.cdc.go.kr/health/mobileweb/content/group_view.jsp?CID=D7J4LLVAZY'>http://health.cdc.go.kr/health/mobileweb/content/group_view.jsp?CID=D7J4LLVAZY</a>"
      },
    ],
    "2-3-1" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "교대근무",
        "subtitle_bubble_q" : "교대근무로 인한 수면장애는 어떻게 치료하나요?",
        "content" : "대부분의 소방공무원은 교대근무를 하고 있습니다. 시간과 장소를 가리지 않고 일어나는 여러 사건사고로 인해 교대근무는 소방공무원에게 필수 불가결한 요소이지만, 교대근무는 심혈관계 질환을 비롯한 의학적 문제를 야기할 수 있습니다.<br>특히, 교대근무로 인한 수면장애는 교대근무 수면장애(Shift work sleep disorder, SWSD)로 따로 구분되어 다뤄지는 등 그 심각성이 강조되고 있습니다<sup>207</sup>.<br><br>교대근무 수면장애는 일주기 리듬 수면장애의 한 종류로, 생체 리듬 상의 수면 시간과 실제 24시간 활동 사이의 불일치로 인해 발생합니다.<br><br>교대근무로 인해 낮 시간대에 자고 밤 시간대에 깨어 있는 상황이 매일 불규칙적으로 반복되면 고정된 수면 시간을 확보하기 어렵고, 생체 리듬과 실제 활동 시간이 일치하지 않게 됩니다.<br>교대근무 수면장애의 환자들은 만성적인 불면증과 과도한 주간 졸음을 호소할 수 있습니다 <sup>208</sup>. 이러한 증상은 안전상의 문제를 야기할 수 있으며, 지속적인 피로와 졸음을 유발하여 휴무일의 일상 활동에까지 영향을 주어 삶의 질을 저하시킬 수 있습니다<sup>209</sup>.<br><br>수면장애 치료는 크게 인지행동치료, 약물치료, 대체요법으로 구분할 수 있습니다.<br><br>미국 수면의학회와 유럽 수면의학회는 비약물치료 중에서 특히 불면증 인지행동치료(Cognitive Behavioral Therapy for insomnia, CBT-I)를 권장하고 있습니다<sup>210,211</sup>.<br>인지행동치료는 자극 조절, 이완 훈련, 수면 위생 교육 등 포함하는 치료법입니다. 인지행동치료는 효과가 천천히 나타난다는 제한점이 있지만, 환자 본인의 의지와 노력만 있다면 부작용이 거의 없고 불안이나 우울과 같은 정신 증상도 호전될 수 있는 효과적인 치료법입니다<sup>212</sup>.<br><br>자극 조절은, 수면 시간 및 침실이라는 환경적 자극과 수면에 방해가 될 수 있는 행동들 사이의 연결성이 수면 장애를 유발할 수 있다는 근거로 개발되었습니다<sup>213</sup>.<br>따라서 TV를 보거나 라디오를 듣는 등 수면에 부적합한 행동과 잠자리를 구분하게 하는 치료법입니다.<br>자극 조절 치료의 지시사항으로는<br>1) 피곤하기만한 경우가 아닌, 졸릴 때만 잠자리에 들 것.<br>2) 잠자리에서 10분 이내에 잠이 오지 않는다면, 잠자리에서 벗어나 다른 방으로 이동할 것. <br>3) 침대에서 계획을 짜거나 음식을 먹는 등 수면에 도움이 되지 않는 활동을 최대한 줄일 것. <br><br>등이 있습니다. 이러한 규칙은 잠자리가 수면을 위한 공간이라는 것을 인식하게 하여 잠자리 자체가 수면을 유도할 수 있게 합니다.<br><br>근육 긴장과 같은 신체적 각성과 생각이 반복적으로 떠오르거나 밀려드는 것과 같은 인지적 각성도 수면 장애를 유발할 수 있습니다<sup>214</sup>.<br>이완 훈련은 이러한 신체적, 정신적 각성 상태를 진정시키는 방법입니다. 대표적으로, 점진적 근육이완법은 신체적 각성수준을 낮추기 위해 사용되는 방법으로 신체 부위별 근육을 점진적으로 수축시키고 이완시키는 반복적인 훈련을 통해 긴장을 완화시킬 수 있습니다<sup>215</sup>. <br>손부터 시작하여 팔, 어깨, 목, 얼굴을 거쳐 복부, 엉덩이, 허벅지, 발 순서로 각 근육을 순차적으로 5초 간 수축시키고 10-15초 간 이완시킵니다.<br><br>수면장애의 치료와 예방적인 측면에서 질 좋은 수면을 촉진시킬 수 있는 습관이나 환경을 수면위생(sleep hygiene)이라고 합니다<sup>216</sup>. <br>수면위생교육에서는 일반적인 수면에 방해가 되는 행동을 피하고 좋은 수면을 촉진할 수 있는 행동을 권고합니다. 권고사항으로는, <br>1) 자는 시간을 제외하고 잠자리에서 보내는 시간을 줄일 것. <br>2) 잠을 억지로 자려고 하지 말 것. <br>3) 수면 시간에 원치 않은 인지활동을 줄이기 위해 침실에 시계를 두지 말 것. <br>4) 매일 꾸준히 운동할 것. <br>5) 커피와 술, 담배를 피할 것. <br>6) 수면 중 배고픔은 수면을 방해할 수 있으므로 따뜻한 우유와 같은 간단한 간식을 먹을 것. <br>등이 있습니다. <br><br>좋은 수면에 있어서 수면위생은 중요하며, 치료의 일환으로 개인에 맞게 적용되었을 때 효과적인 방법입니다.<br><br>기기를 통해 밝은 빛을 비추어 수면 문제를 개선하는 치료법은, 적절한 강도와 시간, 기간대에 사용하면 잠에 들거나 일과시간 중 각성상태를 유지하는 데에 큰 도움이 되는 것으로 알려져 있습니다<sup>217</sup>. 이러한 치료법을 광치료(light therapy), 또는 밝은 빛 노출 치료(bright light exposure)라고 합니다.<br><br>광치료에 사용되는 빛은 최소 2,500 lux 이상으로, 일상적으로 사용하는 형광등의 5배 이상의 밝은 빛이 필요합니다<sup>218</sup>. 치료방법은 조명장치와 90 cm 이내의 거리를 유지하고 빛을 받는 것입니다. <br>빛을 직접적으로 쳐다보려고 노력할 필요는 없으며 고정된 자세를 유지하지 않아도 됩니다. 따라서 치료가 진행되는 동안 TV 시청, 독서 등의 다른 활동을 할 수 있습니다. <br>빛을 받는 시간은 개인별로 차이가 있을 수 있으나, 일반적으로 하루 15분 이상 적용하며, 수면장애의 양상에 따라 적용하는 시간대가 달라집니다. <br>잠이 들기 어려운 경우에는 아침에, 저녁시간대에 깨어 있기 힘들고 아침에 잠에서 일찍 깨어나는 경우에는 취침 전 저녁시간에 빛에 노출되도록 권장하고 있습니다<sup>219</sup>. <br><br>효과적인 증상 호전을 위해서는 최소 2주 이상의 기간이 필요할 수 있습니다.<br><br>이처럼 다양한 비약물적 치료를 통해 교대근무 수면장애의 증상을 완화시킬 수 있습니다. 만약 수면에 어려움을 겪으신다면, 전문가의 도움을 받아 적절한 치료를 받으시는 것을 권해드립니다. 증상이 심한 경우에는 의사의 처방에 따라 적절한 약물적 치료가 필요할 수 있습니다<sup>220</sup>.",
        "reference" : "<sup>207</sup> Schwartz et al., (2006). Shift work sleep disorder. Drugs, 2357-70.<br><sup>208</sup> Sack et al., (2007). Circadian rhythm sleep disorders: part I, basic principles, shift work and jet lag disorders. Sleep, 1460-83.<br><sup>209</sup> 한국산업안전보건공단 산업안전보건연구원, (2017). 야간근무자의 수면장애 실태 및 관리방안.<br><sup>210</sup> Sateia et al., (2017). Clinical practice guideline for the pharmacologic treatment of chronic insomnia in adults: an American Academy of Sleep Medicine clinical practice guideline. J Clin Sleep Med, 307-349.<br><sup>211</sup> Riemann et al., (2017). European guideline for the diagnosis and treatment of insomnia. J Sleep Res, 675-700.<br><sup>212</sup> Boland et al., (2019). Precision medicine for insomnia. Sleep Med Clin, 291-9.<br><sup>213</sup> Peter et al., (1991). Case studies in insomnia. Springer.<br><sup>214</sup> 윤인영, (2000). 불면증의 비약물학적 치료. 수면정신생리, 5-9.<br><sup>215</sup> Jacobson, (1938). Progressive muscle relaxation. J Abnorm Psychol, 18.<br><sup>216</sup> Hauri et al., (1977). Current Concepts: The Sleep Disorders. The Upjohn Company.<br><sup>217</sup> Budnick et al., (1995). An evaluation of scheduled bright light and darkness on rotating shiftworkers: trial and limitations. Am. J. Ind. Med., 771-82.<br><sup>218</sup> 조숙행, (1998). 광치료의 기본 원리와 임상 실제. 수면정신생리, 170-6.<br><sup>219</sup> Gooley, (2008). Treatment of circadian rhythm sleep disorders with light. Ann. Acad. Med. Singap., 669-76.<br><sup>220</sup> Schwartz et al., (2006). Shift work sleep disorder. Drugs, 2357-70."
      },
    ],
    "2-3-2" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "교대근무",
        "subtitle_bubble_q" : "새롭게 개발된 치료 방법에는 무엇이 있나요?",
        "subtitle_text" : "광치료(light therapy, LT)",
        "content" : "수면과 각성, 인지기능은 우리 몸의 생체 리듬의 영향을 받을 수 있습니다. 뇌의 시상하부 중 시교차상핵(suprachiasmatic nucleus)이 우리 몸의 생체 리듬을 조절합니다.<br>시교차상핵은 눈의 망막에서 빛의 정보를 받아들여 생체 리듬과 밤낮 같은 외부의 시간을 조정합니다<sup>221</sup>. 그러므로 밝은 빛은 신체의 수면 각성 주기를 변화시킬 수 있습니다<sup>222</sup>.<br>또한 빛은 수면 유도 호르몬으로 알려진 멜라토닌 분비를 억제하여 약간의 각성 효과가 있는 것이 입증되었습니다<sup>223</sup>.<br><br>광치료(light therapy), 또는 밝은 빛 노출 치료(bright light exposure)는 햇빛과 같은 빛의 파장을 제공하는 장치를 이용하여, 하루 특정한 시간 대에 일정 시간 동안 빛에 노출되도록 하는 치료법입니다.<br><br>교대근무, 국외 출장 등으로 인하여 수면주기가 일정하지 않아 생기는 불면증, 우울감, 신체 활력이나 집중력 이 떨어지는 등의 인지기능 저하 증상을 완화하는 방법입니다.<br>일정 강도 이상의 빛을 저녁에 보게 되면 일주기리듬의 지연이 일어날 수 있고 새벽에 보게 되면 일주기 리듬이 앞당겨질 수 있다는 원리를 이용하여, 잠이 들기 어려운 경우와 잠에서 일찍 깨어나는 경우를 구분하여 빛 노출 시간을 달리할 수 있습니다<sup>224</sup>.<br><br>뇌 자기공명영상을 활용한 연구에서는 광치료가 뇌의 변화를 유도할 수 있음이 확인되었습니다. 2주간의 광치료 후 뇌 내 현출성 네트워크(salience network)의 기능적 연결성이 감소되었습니다<sup>225</sup>. 이러한 뇌 내 네트워크의 변화는 수면의 효율과 연관성이 있는 것으로 나타났습니다.<br><br>광치료는 치료자 없이 스스로 수행이 가능하고 치료 장치의 휴대가 간편하다는 장점이 있어 접근성이 높습니다.<br>또한 치료 중 다른 활동을 할 수 있기 때문에, 야간 근무 중에도 치료가 가능합니다. 따라서 교대근무 근로자의 수면 문제를 해소하기 위한 효과적인 치료법으로 여러 임상 연구들이 진행되고 있습니다<sup>226</sup>. 이에 따라 직종별 교대근무 형태에 맞춘 치료 프로토콜 개발을 기대할 수 있습니다.",
        "reference" : "<sup>221</sup> Berson et al., (2002). Phototransduction by retinal ganglion cells that set the circadian clock. Science, 1070-<br><sup>222</sup> Czeisler et al., (1989). Bright light induction of strong (type 0) resetting of the human circadian pacemaker. Science, 1328-33.<br><sup>223</sup> Tamarkin et al., (1979). Regulation of pineal melatonin in the Syrian hamster. Endocrinology, 385-9.<br><sup>224</sup> Minors et al., (1991). A human phase-response curve to light. Neurosci Lett, 36-40.<br><sup>225</sup> Jiyoung Ma et al., (2020). Decreased functional connectivity within the salience network after two-week morning bright light exposure in individuals with sleep disturbances: a preliminary randomized controlled <br><sup>226</sup> Gooley, (2008). Treatment of circadian rhythm sleep disorders with light. Ann. Acad. Med. Singap., 669-76.",
        "img_top" : "part3_2-2.jpeg",
        "img_top_size" : "full"
      }
    ],
    "2-3-3" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "교대근무",
        "subtitle_bubble_q" : "일상생활에서도 도움이 되는 것들이 있나요?",
        "content" : "교대근무를 하는 소방공무원은 매일 정해진 시간에 잠자리에 드는 것이 어려울 수 있습니다. 이러한 수면 시간의 불규칙성은 직무상 스트레스를 유발할 수 있으며, 직무 스트레스를 줄이기 위하여 흡연이나 음주를 하는 경우가 있습니다<sup>227</sup>. 하지만 숙면을 위해서는 담배와 술을 멀리하는 것이 좋습니다.<br><br>니코틴은 낮은 농도에서는 신체적 이완을 유도하지만, 농도가 높아지면 오히려 각성을 유발할 수 있습니다. 이러한 변화는 급격하게 일어나므로 야간의 흡연은 각성 효과가 있다고 할 수 있습니다. 따라서 저녁시간에는 흡연을 피하시는 편이 숙면에 좋습니다.<br><br>마찬가지로, 술을 마시면 쉽게 잠이 든다고 느껴질 수 있습니다. 하지만 소량의 알코올이라도 수면 분절(sleep fragmentation) 등의 수면문제를 유발하는 것으로 알려져 있습니다<sup>228</sup>. 수면 분절은 자는 도중 여러 번 깨어나서 수면 시간이 길지 않고 중간중간 끊어지는 증상을 의미합니다. 이러한 수면 분절을 겪으면, 자고 일어난 후 개운하지 않고 피로감이 지속됩니다. 따라서 깊은 수면을 위해서는 음주는 삼가는 것을 권해드립니다.",
        "img_bottom": "part3_2-3.png",
        "img_bottom_size": "enlarge",
        "text_add" : "<span class='img-ref'>활동계측기를 이용하여 이와 같이 수면 및 활동시간을 기록할 수 있습니다.</a>"
      },
      {
        "content" : "수면 및 활동시간을 측정할 수 있는 기기를 통해 매일 활동 패턴을 기록하는 것은 수면 습관 개선에 도움이 될 수 있습니다<sup>229</sup>.<br>사진 속에서 착용하는 기기는 수면각성활동량 검사 기기 (Actigraph)로, 착용 시간 동안의 활동량을 측정하여 수면 시간과 활동 시간을 구분하여 분석할 수 있습니다. 스스로 착용할 수 있으며, 직접 결과를 확인할 수 있습니다.<br><br>활동 기록을 점검하면서, 수면 시간 외에 침대 등 잠자리에서 하는 활동을 줄이고 운동량을 늘리는 등 숙면에 도움이 되는 방향으로 일상적인 행동을 개선할 수 있습니다. 좋은 수면 습관이 점자 길러지면 활동계측기에서 긍정적인 피드백을 기대할 수 있을 것입니다.<br><br>이처럼 좋은 수면을 위해 노력하고 수면에 대한 긍정적인 피드백을 받으면 시너지 효과가 생길 수 있습니다. 긍정적인 피드백만으로도 수면 문제 개선에 도움이 될 수 있습니다<sup>230</sup>.",
        "reference" : "<sup>227</sup> 조선덕 외, (2012). 소방공무원의 직무스트레스와 음주사용장애와의 관련성. 한국방재학회, 133-40.<br><sup>228</sup> Rouhani et al., (1989). EEG effects of a single low dose of ethanol on afternoon sleep in the nonalcohol-dependent adult. Alcohol, 87-90.<br><sup>229</sup> 김석주, (2020). 불면장애 진단과 치료의 최신 지견. 신경정신의학, 2-12.<br><sup>230</sup> Gavriloff, D., Sheaves, B., Juss, A., Espie, C. A., Miller, C. B., & Kyle, S. D. (2018). Sham sleep feedback delivered via actigraphy biases daytime symptom reports in people with insomnia: Implications for insomnia disorder and wearable devices. Journal of sleep research, 27(6), e12726.",
        "img_bottom": "part3_2-4.jpeg",
        "img_bottom_size": "full"
      },
    ],
    "2-4-1" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "유난히 생각나는 현장",
        "subtitle_bubble_q" : "외상후스트레스장애 치료는 어떻게 하나요?",
        "content" : "외상후스트레스장애 치료는 인지행동치료와 약물치료가 중심이 됩니다<sup>231</sup>. 외상후스트레스장애 증상이 나타나면 치료를 빨리 시작하여 전문가의 도움을 받는 것이 중요합니다.<br><br><b>인지행동치료에서는 무엇을 하나요?</b><br><br>인지행동치료는 생각, 감정, 행동에 초점을 맞추어, 현재의 문제를 들여다보는 심리치료입니다<sup>232</sup>.<br><br>인지행동치료 방법으로는 지연된 노출 요법, 외상에 초점을 맞춘 인지 처리 요법, 안구운동 탈감작 및 재구성 기법 등이 있습니다. 인지행동치료는 충격적인 사건으로 인해 드는 생각과 기분을 들여다볼 수 있도록 합니다. 사건과 관련된 상황을 단계적으로 마주치면서, 스스로 공포와 불안을 다스리는 방법을 배우게 됩니다.<br><br>치료자의 도움을 받아 분노, 죄책감, 공포 등의 감정을 스스로 조절할 수 있도록 사건과 관련된 기억, 기분, 몸의 반응, 그리고 행동을 천천히 변화시켜 나가는 심리치료입니다.<br><br>충격적인 사건에 대한 기억이 극도의 공포감과 불안을 불러일으키게 되면, 사람은 자연스럽게 사건과 관련된 것은 무엇이든지 피하고 싶어집니다.<br><br>회피 행동은 순간적으로는 안정감을 줄 수 있습니다. 그러나 고통을 건강하게 극복해내는 방법을 습득할 기회를 놓쳐 회복하기가 어려울 수 있습니다.<br><br>인지행동치료와 같이 사건을 마주보고 이겨내려는 시도가 필요합니다<sup>233</sup>.<br><br><b>약물치료는 어떻게 진행되나요?</b><br><br>외상후스트레스장애 치료 약물로 항우울제인 선택적 세로토닌 재흡수 저해제 등을 사용할 수 있습니다<sup>234</sup>.<br><br>우울한 기분이 없더라도 항우울제는 사건으로 인해 느낄 수 있는 불안, 공포, 충동성 경향 등의 부정적인 감정들을 다루는데 도움을 줍니다.<br>외상후스트레스장애 치료 약물은 효과가 나타나기 위하여 꾸준히 복용하는 것이 중요합니다. 또한 증상이 좋아지더라도 약물 복용을 갑자기 중단해서는 안됩니다. 복용을 갑자기 중단하는 경우 부작용이 나타날 수 있습니다. 용량을 점차 감소하여 중단할 수 있도록 담당 의사와 상의하시길 바랍니다.",
        "reference" : "<sup>231</sup> 미국심리학회(2020년 06월 30일). URL:<a href='https://www.apa.org/ptsd-guideline/treatments'>https://www.apa.org/ptsd-guideline/treatments</a><br><sup>232</sup> 미국심리학회(2020년 06월 30일). URL:<a href='https://www.apa.org/ptsd-guideline/treatments'>https://www.apa.org/ptsd-guideline/treatments</a><br><sup>233</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013) Recovery after Trauma - A guide for people with Posttraumaic stress disorder. URL:<a href='https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf'>https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf</a><br><sup>234</sup> Baldwin et al., (2005). Evidence-based guidelines for the pharmacological treatment of anxiety disorders: recommendations from the British Association for Psychopharmacology. J Psychopharmacol, 567-96."
      }
    ],
    "2-4-2" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "유난히 생각나는 현장",
        "subtitle_bubble_q" : "새롭게 개발된 치료 방법에는 무엇이 있나요?",
        "subtitle_text" : "고압산소요법 (Hyperbaric Oxygen Therapy, HBOT)",
        "content" : "고압산소요법은 대기압보다 기압이 높은 상태에서 고농도의 산소를 흡입하여 혈액 중의 산소 농도를 높이는 치료법입니다<sup>235</sup>.<br>혈액 중의 산소 농도를 높이면 조직과 장기로의 산소 전달을 용이하게 할 수 있습니다<sup>236</sup>. 세포들은 산소가 충분한 상태에서 에너지를 낼 수 있어, 고압산소요법은 산소가 부족했던 조직에 산소를 충분히 전달하여 기능을 회복할 수 있도록 돕습니다.<br>고압산소요법은 세포 사멸을 감소시키고, 성장인자 및 항산화제 발현을 높이며, 염증성 사이토카인 분비를 억제한다고 알려져 있습니다.<br><br>고압산소요법은 일산화탄소 중독, 당뇨 발, 상처 치료 등에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받아 쓰이고 있는 안전하고 효과적인 치료법입니다. <br><br>더 나아가, 고압산소요법은 뇌기능 개선을 도울 수 있습니다.<br><br>신체 장기 중 뇌는 체중의 5% 정도에 불과하지만, 뇌는 체내 산소 중 약 25%를 소비하고 기초 대사량 중 약 20%를 차지할 정도로 대사가 활발한 기관입니다<sup>237</sup>.<br>뇌에서 산소가 부족하면 신경세포가 죽을 수 있고, 한 번 죽은 신경세포는 재생이 어렵기 때문에 산소를 충분히 공급하여 기능을 보존하는 것이 중요합니다. 고압산소요법은 산소가 부족한 조직에 산소를 공급하여 뇌기능을 개선할 수 있습니다.<br><br>이처럼 고압산소요법은 신경보호효과를 보이는 것으로 알려져 있어, 여러 질환에서 고압산소요법의 치료 효과를 입증하는 임상 연구들이 진행 중에 있습니다.<br><br>외상성 뇌손상, 외상후스트레스장애, 돌발성 난청 등의 증상 개선 및 인지기능 개선에 고압산소요법이 활용될 수 있습니다. 이러한 효과에 대한 기대로, 미국 보훈부(United States Department of Veterans Affairs)는 참전 군인의 치료되지 않는 외상후스트레스장애의 치료법 중 하나로 고압산소요법을 제공하고 있습니다<sup>238</sup>.<br><br>고압산소요법의 치료는 챔버라는 원통형 공간에서 진행됩니다 <sup>239</sup>.<br>단실 챔버와 복실 챔버로 구분됩니다. 단실 챔버는 긴 원통의 기기 안에 들어가 고압산소를 흡입하며 한 번에 한 명만 치료를 받을 수 있는 형태입니다. 복실 챔버는 마스크 또는 밀폐 후드를 착용하여 고압산소를 흡입합니다. 고압산소요법 치료는 통상적으로 1-2시간 정도의 치료를 40회 이상 반복합니다.<Br><br>고압산소요법 치료 자체는 아무런 통증을 주지 않지만, 치료 중 높은 기압으로 인해 일시적으로 귀의 멍멍함이나 통증을 느낄 수 있습니다. ",
        "reference" : "<sup>235</sup> Gill et al., (2014). Hyperbaric oxygen: its uses, mechanisms of action and outcomes. QJM, 385-95.<br><sup>236</sup> Eve et al., (2016). Hyperbaric oxygen therapy as a potential treatment for post-traumatic stress disorder associated with traumatic brain injury. Neuropsychiatr Dis Treat, 2689-705.<br><sup>237</sup> Mink et al., (1981). Ratio of central nervous system to body metabolism in vertebrates: its constancy and functional basis. Am J Physiol, 203-12.<br><sup>238</sup> 미국보훈부. URL:<a href='https://www.va.gov/opa/pressrel/pressrelease.cfm?id=3978'>https://www.va.gov/opa/pressrel/pressrelease.cfm?id=3978</a><br><sup>239</sup> Gill et al., (2014). Hyperbaric oxygen: its uses, mechanisms of action and outcomes. QJM, 385-95.",
        "img_top" : "part3_2-1.png",
        "img_top_size" : "full"
      }
    ],
    "2-4-3" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "유난히 생각나는 현장",
        "subtitle_bubble_q" : "일상생활에서도 도움되는 것들이 있나요?",
        "content" : "먼저, 몸 건강을 챙기는 것이 곧 마음의 건강을 챙기는 방법입니다<sup>240</sup>. 충분한 휴식을 취하고, 운동을 규칙적으로 하고, 균형 있는 식사를 잘 챙기도록 노력하세요. 몸이 건강하면 마음 건강을 되찾는데 도움을 줄 수 있습니다.<br><br>건강한 회복을 위해서 주위 사람들의 도움이 필요합니다.<br><br>선생님을 생각해주는 사람들과 시간을 보내세요. 사건에 관한 이야기를 하지 않더라도 괜찮습니다. 가족들 또는 친구들의 지지는 선생님의 마음을 편안하게 해줄 것입니다.<br>이야기할 준비가 되신다면, 선생님을 이해할 수 있는 사람들에게 선생님의 감정을 이야기해보세요. 사건에 대해 말하는 과정은 회복 단계의 한 부분으로, 사건을 자연스럽게 받아들일 수 있도록 도와줍니다. <br><br>카페인이 함유된 차, 커피, 탄산음료, 설탕처럼 긴장을 유발하는 음식은 멀리 하는 것이 좋습니다<sup>241</sup>. 술 또는 담배로 힘듦을 견뎌내는 것은 좋지 않은 습관입니다. 이들은 마음의 고통을 해결해주지 못하며, 장기적으로 술과 담배는 몸과 마음의 건강을 해칩니다.",
        "reference" : "<sup>240</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013) Recovery after Trauma - A guide for people with Posttraumaic stress disorder. URL:<a href='https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf'>https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf</a><br><sup>241</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013) Recovery after Trauma - A guide for people with Posttraumaic stress disorder. URL:<a href='https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf'>https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf</a>",
      }
    ],
    "2-5-1" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "기분 변화",
        "subtitle_bubble_q" : "우울장애 치료는 어떻게 하나요?",
        "content" : "우울장애에 의한 의욕 저하와 우울감은 평범한 일상을 지속하기 어렵게 만듭니다. 우울장애는 일시적인 우울감과는 다르며, 개인이 약해서 생기는 것이 아니고 의지로 없앨 수 있는 기분 또한 아닙니다. 적절한 치료를 통해 정상적인 생활을 영위하는 상당한 호전을 기대할 수 있으므로, 증상이 의심되는 경우 전문가의 도움을 받길 권유드립니다<sup>242</sup>.<br><br><b>약물치료는 어떻게 진행되나요?</b><br><br>우울장애를 치료하기 위한 약물은 선택적 세로토닌 재흡수 저해제를 주로 사용합니다. 우울장애는 뇌에서 감정, 기억, 운동 등 여러 뇌기능을 조절하는 신경전달물질들의 기능 이상과 밀접한 연관이 있습니다. <br><br>신경전달물질로 가장 대표적인 것이 세로토닌이고, 그밖에 노르에피네프린, 도파민 등이 있습니다. 항우울제는 신경전달물질의 불균형을 완화하여 감정과 관련된 뇌기능 이상을 호전시키는 것으로 알려져 있습니다. <br><br>항우울제는 효과가 바로 나타나지 않기 때문에 4-6주 이상 꾸준히 복용하는 것이 중요합니다. 약물 종류 또는 약물의 용량을 바꾸는 경우 효과가 나타나기까지의 기간이 더 길어질 수 있습니다. 중간에 포기하지 않고 끝까지 치료에 임하는 것이 중요합니다. <br>또한 증상이 좋아지더라도 약물 복용을 갑자기 중단해서는 안됩니다. 복용을 갑자기 중단하는 경우 부작용이 나타날 수 있으며, 우울장애가 재발할 가능성이 증가합니다. 용량을 점차 감소하여 중단할 수 있도록 담당 의사와 상의하시길 바랍니다. <br><br><b>약물치료 외의 치료 방법에는 무엇이 있나요?</b><br><br>운동치료, 심리치료, 밝은 빛 치료 등의 비약물치료로 우울 증상을 개선할 수 있습니다. <br><br>경도-중등도의 우울장애라면 약물치료를 시작하기 전에 일차적으로 비약물치료를 시도하는 것이 좋습니다. 약물치료와 비약물치료를 함께 진행하면 치료효과를 더욱 높일 수 있습니다. <br><br>운동치료는 꾸준한 운동을 통해 우울 증상을 개선하는 치료법입니다<sup>243</sup>. 운동치료는 매일 30분 이상 운동하는 것이 좋습니다. 한 가지 운동 보다는 몇 가지 운동을 함께 진행하는 것이 운동치료에 흥미를 잃지 않고 즐겁게 하는 데 도움이 될 수 있습니다.<br><br>심리치료 중 인지행동치료는 생각, 감정, 행동에 초점을 맞추어, 현재의 문제를 들여다보는 심리치료입니다. <br>자신이 자동적으로 부정적인 생각, 비합리적인 생각을 하고 있다는 것을 인식하는 데에서 출발합니다. 부정적인 사고 방식은 우울 증상을 유발하거나 악화시킬 수 있기 때문에 먼저 자동적인 사고가 자신의 기분이나 행동에 어떤 영향을 끼치는지를 이해하는 것이 중요합니다.<br>자신의 인지 왜곡을 하나씩 알아가고 다른 측면으로 바라보면서 긍정적이고 합리적인 생각을 할 수 있는 방법을 익히게 됩니다. 최종적으로는 일상생활에서 힘든 문제를 마주할 때 스스로 인지행동기법을 사용하여 감정을 다룰 수 있는 능력을 강화하는 것이 목표입니다<sup>244</sup>.<br><br>밝은 빛 치료는 낮 시간의 밝기인 10,000 lux 정도의 밝은 빛을 눈보다 약간 밑으로 쬐어주어 시신경을 자극하는 치료법입니다. <br>시신경 자극은 중추신경계로 전달되어 마음가짐이 밝고 명랑해지도록 도와줍니다. 실내조명이 어두운 환경에서 오랜 시간을 보내거나, 햇빛이 적은 겨울에 발생하는 우울장애의 경우 밝은 빛 치료가 증상 개선에 효과가 있다고 알려져 있습니다<sup>245</sup>.",
        "reference" : "<sup>242</sup> 한국임상약학회. 약물치료학 제4개정 (2017). 제28장 주요우울장애. 583-99. <br><sup>243</sup> Cooney. (2013). Exercise for depression. Cochrane Database Syst Rev, CD004366<br><sup>244</sup> Hofmann. (2011). An introduction to modern CBT: Psychological solutions to mental health problems. Wiley-Blackwell.<br><sup>245</sup> Golden et al., (2005), The efficacy of light therapy in the treatment of mood disorders: a review and meta-analysis of the evidence. Am J Psychiatry, 656-62.",
      }
    ],
    "2-5-2" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "기분 변화",
        "subtitle_bubble_q" : "새롭게 개발된 치료 방법에는 무엇이 있나요?",
        "subtitle_text" : "반복 경두개자기자극 (Repetitive Transcranial Magnetic Stimulation, rTMS)",
        "content" : "반복적 경두개자기자극은 전자기코일로 자기장을 형성하여 신경세포의 활성도를 조절하는 방법입니다<sup>246</sup>.<br>전자기코일을 두피 위에 위치하면 전자기코일이 자기장을 형성시켜 신경세포의 탈분극을 조절합니다. <br><br>경두개자기자극은 비침습적인 방법으로 우울장애, 강박장애, 운동장애, 등 여러 질환의 증상을 개선하는 방법으로 활발하게 연구되고 있습니다. 그 중 치료 저항성 우울장애 치료에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받았습니다. <br><br>우울장애를 대상으로 하는 반복적 경두개자기자극 치료는 한 회에 약 40분이 소요되고, 일주일에 5일, 4주에서 6주 동안 치료를 받습니다<sup>247</sup>.<br>반복 경두개자기자극은 일반적으로 편안한 의자에 앉아 치료를 받게 됩니다. 반복적 경두개자기자극은 마취나 안정제가 필요 없는 치료 방법으로, 치료 후 바로 일상생활이 가능합니다.<br><br>반복적 경두개자기자극은 안전성이 입증된 치료법입니다. 다만, 발작의 경험이 있거나 발작 관련 가족력이 있는 경우, 또는 몸에 금속 또는 의료기기가 삽입되어 있는 경우에는 치료를 시작하기 전 의사와 충분히 상의하세요.",
        "reference" : "<sup>246</sup> Hallet., (2000) Transcranial magnetic stimulation and the human brain. Nature, 147-50.<br><sup>247</sup> NeuroStar. (2020년 7월 7일) URL: <a href='https://neurostar.co.kr/?page_id=76'>https://neurostar.co.kr/?page_id=76</a>",
        "img_top" : "part3_2-5.png",
        "img_top_size" : "full"
      }
    ],
    "2-5-3" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "기분 변화",
        "subtitle_bubble_q" : "일상생활에서도 도움되는 것들이 있나요?",
        "content" : "신체 활동과 운동은 우울 증상을 감소시킬 수 있습니다<sup>248</sup>. 산책, 달리기, 수영 등 선생님이 즐겁게 할 수 있는 운동을 꾸준히 할 것을 권장합니다. 술과 담배는 멀리하는 것이 좋습니다. 우울 증상을 악화시킬 수 있습니다. 규칙적이고 균형 잡힌 식습관을 가지는 것 또한 우울 증상 개선에 도움을 줄 수 있습니다. <br><br> <b>가족 또는 동료가 우울해하는 것 같아요. 어떻게 도울 수 있을까요?</b><br><br>우울장애로 인해 짜증을 쉽게 내거나, 무기력하거나, 약속을 지키지 않는 등의 행동적 변화를 나타낼 수 있습니다. <br>우울장애가 의심된다면 이러한 행동을 비난하지 않고 차분히 대화를 나누어보세요. 섣부른 충고보다는 경청하는 자세를 보여주어 가족 또는 동료가 감정을 표현할 수 있도록 돕는 것이 좋습니다. <br>우울 증상이 심하면 부정적인 생각이 지배하기 쉬워서 치료의 효과에 대해 부정적으로 생각하는 경우가 많습니다. 그렇기 때문에 가족, 동료 등 가까운 사람의 지지와 역할이 중요합니다. 우울장애 치료를 받도록 적극적으로 권유하길 바랍니다. <br>혹시 자살에 대해서 언급한다면, 즉각적으로 치료를 받도록 할 필요가 있습니다. <br>우울장애는 치료가 잘 되는 질환이라는 사실을 환기해주세요.",
        "reference" : "<sup>248</sup> 질병관리본부 국가건강정보포털. (2020년 07월 07일) URL: <a href='http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=1200'>http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=1200</a>",
      }
    ],
    "2-6-1" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "집중과 기억 어려움",
        "subtitle_bubble_q" : "인지기능 저하 치료 방법에는 무엇이 있나요?",
        "content" : "주관적 기억장애란, 새로운 정보를 기억하기 어렵거나, 금방 잊어버리는 등 기억력에 문제가 있다고 호소하지만 인지기능 검사 상 정상이고 일상생활에 장애가 없는 경우를 말합니다<sup>249</sup>.<br>물건을 어디에 두었는지 잊거나, 약속을 깜빡 하거나, 방금 말하려던 것이 생각나지 않을 수 있지만 일상생활에는 지장을 주지 않습니다. <br>스트레스를 받거나 집중력이 떨어지는 상황에서 심해질 수 있습니다. <br><br>주관적 기억장애 치료법이 따로 있지는 않으며, 불안, 우울처럼 기억력 저하의 원인이 되는 문제가 있는 경우에는 그 원인을 치료하면 기억력이 회복됩니다. 기억력 저하가 일상생활에 큰 지장을 주거나 성격변화 등의 다른 증상이 있을 때에는 정확한 진단을 받아보는 것이 좋습니다.<br><br>경도인지장애란, 정상적인 노화로 인지기능이 저하된 상태와 치매의 중간 단계를 말합니다. <br>즉, 비슷한 연령대의 사람들보다는 인지 기능이 저하되어 있지만 일상생활은 가능한 수준의 인지 저하를 겪는 단계입니다<sup>250</sup>. <br><br> 경도인지장애의 치료에 치매 치료제인 아세틸콜린 에스터레이스 억제제를 사용할 수 있습니다. 우울장애가 인지기능 저하의 원인이라면 항우울제를 사용합니다. <br>심뇌혈관질환의 위험인자가 있다면 혈전방지제나 지질강하제를 사용할 수 있습니다. <br>고혈압, 당뇨와 같은 원인 질환을 잘 관리하고, 올바른 운동습관과 식습관을 유지하며, 적절한 사회활동으로 건강을 유지하는 것이 중요합니다. <br><br>치매란, 뇌기능의 손상으로 기억력, 언어능력, 판단력 등 여러 인지 기능이 저하되어 일상생활과 사회생활을 제대로 유지하기 어려운 임상 증후군을 말합니다<sup>251</sup>. <br>인지기능 저하 증상에 대하여 아세틸콜린 에스터레이스 억제제와 N-methyl-d-aspartate (NMDA) 수용체 차단제 등을 사용할 수 있습니다. ",
        "reference" : "<sup>249</sup> Reisberg et al., (2008). The pre-mild cognitive impairment, subjective cognitive impairment stage of Alzheimer's disease. Alzheimers Dement. 98-108. <br><sup>250</sup> Gauthier et al., (2006). Mild cognitive impairment. Lancet, 1262-70.<br><sup>251</sup> Scheltens et al., (2016). Alzheimer's disease. Lancet, 505-17.",
      }
    ],
    "2-6-2" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "집중과 기억 어려움",
        "subtitle_bubble_q" : "새롭게 개발된 치료 방법에는 무엇이 있나요?",
        "subtitle_text" : "경두개 직류자극 (Transcranial Direct Current Stimulation, tDCS)",
        "content" : "경두개직류자극은 두피에 미세한 직류 전류를 흘려주어 뇌세포의 활성을 조절하는 치료법입니다<sup>252</sup>. 경두개직류자극은 대뇌 피질의 기능을 조절하고 신경가소성을 활성화하여 인지기능을 개선하는 것으로 알려져 있습니다. 경두개직류자극의 효과는 전극의 종류, 위치, 그리고 세기에 따라 달라질 수 있습니다.<br><br>경두개직류자극 기기는 손바닥보다 작은 크기이고 사용법이 손쉬워 교육을 받은 후에는 집에서 스스로 처치를 시행할 수 있다는 장점이 있습니다. <br><br>두피와 경두개직류자극 사이에 전류가 잘 흐를 수 있도록 식염수를 적신 스펀지 형태의 패치가 필요합니다. 밴드 또는 캡을 사용하여 패치와 경두개직류자극 기기를 두피에 고정할 수 있습니다. 일반적으로 처치 시간은 5분에서 30분 사이이며, 꾸준히 처치를 받는 경우 임상적인 개선 효과를 기대할 수 있습니다<sup>253</sup>. <br><br>경두개직류자극은 심각한 부작용이 거의 없는 안전한 치료법입니다<sup>254</sup>. <br><br>치료 중 또는 치료 직후 전극을 부착한 부위의 가려움, 따끔거림, 불편감, 두통 등의 증상이 일시적으로 있을 수 있지만, 오래 가지 않습니다. 처치를 시작하기 전에 두피에 상처가 없는지 확인하는 단계가 필요합니다.",
        "reference" : "<sup>252</sup> Nitsche et al., (2008). Transcranial direct current stimulation: State of the art 2008. Brain Stimul, 206-23.<br><sup>253</sup> Thair et al., (2017). Transcranial Direct Current Stimulation (tDCS): A Beginner's Guide for Design and Implementation. Front Neurosci, 641<br><sup>254</sup> Brunoni et al., (2011). A systematic review on reporting and assessment of adverse effects associated with transcranial direct current stimulation. Int J Neuropsychopharmacol, 1133-45.",
        "img_top" : "part3_2-6.png",
        "img_top_size" : "full"
      }
    ],
    "2-6-3" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_box" : "집중과 기억 어려움",
        "subtitle_bubble_q" : "일상생활에서도 도움되는 것들이 있나요?",
        "content" : "뇌건강을 지킬 수 있는 생활 습관은 다음과 같습니다.<br><br>규칙적인 운동은 혈액순환을 돕고 뇌세포를 활성화하여 뇌를 건강하게 만듭니다. 걷기, 자전거 타기 등의 유산소운동과 가벼운 근력운동을 한 회에 30분 이상, 일주일에 3회 이상 숨이 약간 찰 정도의 강도로 하는 것이 좋습니다<sup>255</sup>. <br><br>또한 두뇌활동을 지속적으로 하는 것이 좋습니다<sup>256</sup>. 책을 읽는 것, 글을 쓰는 것, 퍼즐을 푸는 것과 같이 머리를 쓰는 활동을 취미로 삼아 꾸준히 해보세요. 메모하는 습관, 자주 사용하는 물건을 정해진 위치에 보관하는 습관을 가지는 것이 도움이 됩니다. <br><br>술과 담배는 뇌건강에 해로우므로 멀리하는 것이 좋습니다. 알코올은 뇌를 손상시키고, 담배의 해로운 성분들은 작은 혈관들을 막히게 할 수 있습니다. <br><br>건강한 식습관으로 뇌를 건강하게 할 수 있습니다. 고등어, 연어, 호두 등 오메가3 지방산을 풍부하게 함유하고 있는 음식이 인지기능 개선에 도움이 될 수 있습니다<sup>257</sup>. 녹차, 은행 등이 함유하고 있는 플라보노이드는 인지기능 개선을 도울 수 있습니다. <br><br>치매는 동반 질환이 있거나 연령이 증가할 수록 발생 위험이 높아집니다. 뿐만 아니라, 흡연, 과음, 운동 부족과 같은 잘못된 생활 습관 또한 치매 발생 위험을 높입니다. 따라서 건강한 생활습관을 유지하는 것이 인지기능 저하를 예방하기 위한 지름길입니다. ",
        "reference" : "<sup>255</sup> 이윤환 외., (2009). 치매예방을 위한 생활습관. 한국노인병학회, 61-8.<br><sup>256</sup> Wilson et al., (2007). Relation of cognitive activity to risk of developing Alzheimer disease. Neurology. 1911-20.<br><sup>257</sup> Gómez-Pinilla. (2008). Brain foods: the effects of nutrients on brain function. Nat Rev Neurosci, 568-78 ",
      }
    ],
    "3" : [
      {
        "title" : "3. 뇌기능 저하 예방의 중요성",
        "subtitle_bubble_q" : "뇌기능 저하 증상을 특별한 관리를 할 필요가 있을까요?",
        "content" : "소방공무원은 업무 특성 상 유해인자에 반복적으로 노출될 수 있으며 이로 인해 신체적, 정신적 건강문제를 겪을 수 있습니다. 유해인자의 예로 화재 현장 속 유해가스를 떠올리기 쉽지만 실제로 노출될 수 있는 유해인자는 다양할 수 있습니다.<br><br>사고현장에서는 고열, 소음, 유해가스 등 물리, 화학적 유해인자에 쉽게 노출될 수 있으며, 구급현장에서는 바이러스 등 감염인자에 의한 생물학적 유해인자에 노출될 수 있습니다. <br><br>이 외에도 교대근무로 인한 수면문제, 비상대기와 긴급출동 중의 긴장감, 민원업무 등 나 기타 직무스트레스에 의한 정신적 유해인자는 뇌기능 저하의 주요한 요인이 될 수 있습니다.<br><br>업무별*로 노출될 수 있는 유해인자 종류에 따라 주로 발생할 수 있는 신체적, 정신적 건강문제는 차이가 날 수 있습니다.<br><br>외상후스트레스 유병률의 경우 구급대원이 가장 높았으며, 화재진압, 화재운전, 화재조사 등 화재 현장 관련 업무에서도 여러 질환의 유병률이 전반적으로 높게 나타났습니다. 우울증의 경우 상황실 관련 업무에서 주된 정신건강 문제로 나타났으며, 음주습관 및 수면문제는 모든 업무 현장에서 두드러지는 것으로 나타났습니다<sup>258</sup>.<br><br>따라서 업무별 주요 심리 및 신체적 문제를 파악하고 관련 증상을 꾸준히 평가하는 것은 뇌기능 저하 예방을 위해 중요합니다. 정확한 증상을 파악하고 각 증상에 맞는 적절한 관리와 치료를 통해 회복탄력성을 향상시키고 뇌기능을 개선시킬 수 있습니다.",
        "img_bottom" : "part3_2-7.jpg",
        "img_bottom_size" : "enlarge",
        "text_add" : "<span class='img-ref'>출처: 소방청, (2019). 2019년도 소방공무원 마음건강 전수 조사 결과 발표</span>"
      },
      {
        "content" : "뇌기능 저하로 인한 건강문제를 관리하는 방법 중 하나로 수면습관 평과 및 관리를 들 수 있습니다.<br><br>소방공무원의 경우 교대근무로 인해 반복적인 수면주기 변화를 경험할 수 있는 만큼 액티그라피와 같은 손목시계형 장치를 활용한 꾸준한 수면습관 평가가 필요할 수 있습니다.<br><br>증상의 정도에 따라 밝은 빛 노출치료 등을 통해 수면문제를 관리할 수 있습니다. 또한 우울, 불안과 같은 정서문제, 인지기능 저하 등 역시 앞서 소개된 반복 경두개자기자극(TMS), 경두개직류자극(tDCS) 등 다양한 방법을 적용하여 개선될 수 있습니다.<br><br>유해인자로 인한 뇌기능 저하 예방법 및 치료법의 효능은 개인별 신경화학물질 농도 차이, 심리사회적 요인, 어떤 정서 문제가 동반되는 지 등에 따라 차이 날 수 있습니다<sup>259</sup>. 따라서 뇌 자기공명영상(MRI), 구조화된 면담도구 등을 활용한 정밀한 평가를 바탕으로 개인별로 효과적인 치료 방법을 찾는 과정이 필요할 수 있습니다.<br><br>유해인자 특이적 뇌기능 저하 증상 감별 및 예측 모델은 개인별 유해인자 노출 차이와 생물학적 차이, 경험적 차이 등 각 요인에 의한 뇌 변화와 뇌 기능에 미치는 영향을 파악할 수 있어 맞춤형 치료법 발전에 기여할 수 있습니다.<br>특히 발생할 수 있는 증상을 미리 예측하는 것은 조기에 뇌기능 저하 예방법을 적용하여 뇌 건강을 보호할 수 있다는 점에서 활용 가능성이 높을 수 있습니다.<br><br>소방공무원은 업무 특성상 여러 유해인자에 노출될 수 있으며, 이로 인해 뇌기능 저하를 겪을 수 있습니다. 하지만 적절한 관리와 치료를 바탕으로 회복탄력성을 향상시킨다면 뇌기능 저하는 예방 및 개선될 수 있습니다.",
        "reference" : "<sup>258</sup> 소방청, (2019). 2019년도 소방공무원 마음건강 전수 조사 결과 발표<br><sup>259</sup> Bennabi et al., (2015). Risk factors for treatment resistance in unipolar depression: A systematic review. J Affect Disord, 137-41",
      }
    ],
    // 2-1 상태체크 결과에서 더보기를 클릭한 내용
    "4" : [
      {
        "title" : "2. 현재 나의 상태 체크",
        "subtitle_bubble_q" : "또 어떤 치료 방법들이 있나요?",
        "subtitle_box" : "화재현장 담당, 유난히 생각나는 현장",
        "subtitle_text" : "고압산소요법 (Hyperbaric Oxygen Therapy, HBOT)",
        "content" : "소방공무원은 화재 현장에서 일산화탄소, 시안화수소를 비롯한 여러 유해가스에 노출되어 중독 증상을 호소할 수 있습니다.<br><br>또한 이러한 유해가스는 뇌 내 산소 공급을 방해하여 혈류 저하에 취약한 뇌 부위를 손상시킬 수 있습니다.<br><br>앞서 일산화탄소중독 치료에 적용하는 것으로 소개해드린 고압산소요법은, 혈액 중의 산소 농도를 높여 조직과 장기로의 산소 전달을 용이하게 합니다<sup>198</sup>.<br>세포들은 산소가 충분한 상태에서 에너지를 낼 수 있어, 산소가 부족했던 조직에 산소를 충분히 전달하여 기능을 회복할 수 있도록 도울 수 있습니다<sup>199</sup>.<br>고압산소요법은 세포 사멸을 감소시키고, 성장인자 및 항산화제 발현을 높이며, 염증성 사이토카인 분비를 억제한다고 알려져 있습니다.<br><br>고압산소요법은 일산화탄소 중독, 당뇨 발, 상처 치료 등에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받아 쓰이고 있는 안전하고 효과적인 치료법입니다.<br><br>더 나아가, 고압산소요법은 뇌기능 개선을 도울 수 있다는 점이 최근 제기되고 있습니다.<br><br>신체 장기 중 뇌는 체중의 5% 정도에 불과하지만, 뇌는 체내 산소 중 약 25%를 소비하고 기초 대사량 중 약 20%를 차지할 정도로 대사가 활발한 기관입니다<sup>200</sup>. 기초연구를 통해 고압산소요법이 신경보호효과를 보이는 것으로 알려져, 여러 질환에서 고압산소요법의 치료 효과를 입증하기 위한 임상 연구들이 진행되고 있습니다.<br>외상성 뇌손상, 외상후스트레스장애, 돌발성 난청 등의 증상 개선 및 인지기능 개선에 고압산소요법이 시도되고 있습니다.<br><br>서울소방재난본부는 소방공무원을 대상으로 고압산소요법을 적용하는 시범사업을 진행하였습니다.<br><br>고압산소요법 적용의 편의성과 효율성을 높이기 위해 본부 내에 고압산소요법 기기를 설치하여 연구를 진행한 결과 소방공무원에서 고압산소요법의 긍정적인 효과를 확인하였습니다.<br>소방공무원들은 고압산소요법이 수면의 질 개선, 피로 회복, 집중력 향상 등에 도움이 된다고 보고하였습니다. 또한 고압산소요법 시행 후 뇌 혈류 증가를 확인한 연구결과가 있어<sup>201</sup>, 고압산소요법의 뇌기능 개선 효과를 기대할 수 있습니다.<br>또한 환자 개인별 상태에 따른 맞춤형 고압산소요법 시행 방법도 개발되고 있습니다.<br><br>최근 심박변이도(Heart Rate Variability, HRV), 맥박, 혈압, 뇌전도(Electroencephalography, EEG), 피부전도도(Skin Conductance Response, SCR) 등의 생리적 지표를 조합하여 고압산소요법 처치 중인 환자의 불안 수준을 실시간으로 감지하고, 이를 기반으로 공기의 압력을 실시간으로 제어할 수 있는 제어 장치 및 이를 포함하는 고압 산소 치료 시스템에 대한 특허가 등록되었습니다<sup>202</sup>. 이를 통해 고압산소요법 처치 중에 생길 수 있는 부작용을 최소화하고 치료 효과를 향상시킬 것으로 기대됩니다.",
        "reference" : "<sup>198</sup> Eve et al., (2016). Hyperbaric oxygen therapy as a potential treatment for post-traumatic stress disorder associated with traumatic brain injury. Neuropsychiatr Dis Treat, 2689-705.<br><sup>199</sup> Edwards, (2010). Hyperbaric oxygen therapy. Part 2: application in disease. J. Vet. Emerg. Crit. Care, 289-97.<br><sup>200</sup> Mink et al., (1981). Ratio of central nervous system to body metabolism in vertebrates: its constancy and functional basis. Am J Physiol, 203–12.<br><sup>201</sup> Golden et al., (2002). Improvement in cerebral metabolism in chronic brain injury after hyperbaric oxygen therapy. Int. J. Neurosci., 119-31.<br><sup>202</sup> 이화여자대학교 산학협력단. (2020). 특허등록 제 10-2077372. 서울:특허청.",
        "img_top" : "part3_2-1.png",
        "img_top_size" : "full"
      },
      {
        "content" : "고압산소요법은 대기압보다 기압이 높은 상태에서 고농도의 산소를 흡입하여 혈액 중의 산소 농도를 높이는 치료법입니다<sup>235</sup>.<br>혈액 중의 산소 농도를 높이면 조직과 장기로의 산소 전달을 용이하게 할 수 있습니다<sup>236</sup>. 세포들은 산소가 충분한 상태에서 에너지를 낼 수 있어, 고압산소요법은 산소가 부족했던 조직에 산소를 충분히 전달하여 기능을 회복할 수 있도록 돕습니다.<br>고압산소요법은 세포 사멸을 감소시키고, 성장인자 및 항산화제 발현을 높이며, 염증성 사이토카인 분비를 억제한다고 알려져 있습니다.<br><br>고압산소요법은 일산화탄소 중독, 당뇨 발, 상처 치료 등에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받아 쓰이고 있는 안전하고 효과적인 치료법입니다. <br><br>더 나아가, 고압산소요법은 뇌기능 개선을 도울 수 있습니다.<br><br>신체 장기 중 뇌는 체중의 5% 정도에 불과하지만, 뇌는 체내 산소 중 약 25%를 소비하고 기초 대사량 중 약 20%를 차지할 정도로 대사가 활발한 기관입니다<sup>237</sup>.<br>뇌에서 산소가 부족하면 신경세포가 죽을 수 있고, 한 번 죽은 신경세포는 재생이 어렵기 때문에 산소를 충분히 공급하여 기능을 보존하는 것이 중요합니다. 고압산소요법은 산소가 부족한 조직에 산소를 공급하여 뇌기능을 개선할 수 있습니다.<br><br>이처럼 고압산소요법은 신경보호효과를 보이는 것으로 알려져 있어, 여러 질환에서 고압산소요법의 치료 효과를 입증하는 임상 연구들이 진행 중에 있습니다.<br><br>외상성 뇌손상, 외상후스트레스장애, 돌발성 난청 등의 증상 개선 및 인지기능 개선에 고압산소요법이 활용될 수 있습니다. 이러한 효과에 대한 기대로, 미국 보훈부(United States Department of Veterans Affairs)는 참전 군인의 치료되지 않는 외상후스트레스장애의 치료법 중 하나로 고압산소요법을 제공하고 있습니다<sup>238</sup>.<br><br>고압산소요법의 치료는 챔버라는 원통형 공간에서 진행됩니다 <sup>239</sup>.<br>단실 챔버와 복실 챔버로 구분됩니다. 단실 챔버는 긴 원통의 기기 안에 들어가 고압산소를 흡입하며 한 번에 한 명만 치료를 받을 수 있는 형태입니다. 복실 챔버는 마스크 또는 밀폐 후드를 착용하여 고압산소를 흡입합니다. 고압산소요법 치료는 통상적으로 1-2시간 정도의 치료를 40회 이상 반복합니다.<Br><br>고압산소요법 치료 자체는 아무런 통증을 주지 않지만, 치료 중 높은 기압으로 인해 일시적으로 귀의 멍멍함이나 통증을 느낄 수 있습니다. ",
        "reference" : "<sup>235</sup> Gill et al., (2014). Hyperbaric oxygen: its uses, mechanisms of action and outcomes. QJM, 385-95.<br><sup>236</sup> Eve et al., (2016). Hyperbaric oxygen therapy as a potential treatment for post-traumatic stress disorder associated with traumatic brain injury. Neuropsychiatr Dis Treat, 2689-705.<br><sup>237</sup> Mink et al., (1981). Ratio of central nervous system to body metabolism in vertebrates: its constancy and functional basis. Am J Physiol, 203-12.<br><sup>238</sup> 미국보훈부. URL:<a href='https://www.va.gov/opa/pressrel/pressrelease.cfm?id=3978'>https://www.va.gov/opa/pressrel/pressrelease.cfm?id=3978</a><br><sup>239</sup> Gill et al., (2014). Hyperbaric oxygen: its uses, mechanisms of action and outcomes. QJM, 385-95.",
      },
      {
        "subtitle_box" : "교대근무",
        "subtitle_text" : "광치료(light therapy, LT)",
        "content" : "수면과 각성, 인지기능은 우리 몸의 생체 리듬의 영향을 받을 수 있습니다. 뇌의 시상하부 중 시교차상핵(suprachiasmatic nucleus)이 우리 몸의 생체 리듬을 조절합니다.<br>시교차상핵은 눈의 망막에서 빛의 정보를 받아들여 생체 리듬과 밤낮 같은 외부의 시간을 조정합니다<sup>221</sup>. 그러므로 밝은 빛은 신체의 수면 각성 주기를 변화시킬 수 있습니다<sup>222</sup>.<br>또한 빛은 수면 유도 호르몬으로 알려진 멜라토닌 분비를 억제하여 약간의 각성 효과가 있는 것이 입증되었습니다<sup>223</sup>.<br><br>광치료(light therapy), 또는 밝은 빛 노출 치료(bright light exposure)는 햇빛과 같은 빛의 파장을 제공하는 장치를 이용하여, 하루 특정한 시간 대에 일정 시간 동안 빛에 노출되도록 하는 치료법입니다.<br><br>교대근무, 국외 출장 등으로 인하여 수면주기가 일정하지 않아 생기는 불면증, 우울감, 신체 활력이나 집중력 이 떨어지는 등의 인지기능 저하 증상을 완화하는 방법입니다.<br>일정 강도 이상의 빛을 저녁에 보게 되면 일주기리듬의 지연이 일어날 수 있고 새벽에 보게 되면 일주기 리듬이 앞당겨질 수 있다는 원리를 이용하여, 잠이 들기 어려운 경우와 잠에서 일찍 깨어나는 경우를 구분하여 빛 노출 시간을 달리할 수 있습니다<sup>224</sup>.<br><br>뇌 자기공명영상을 활용한 연구에서는 광치료가 뇌의 변화를 유도할 수 있음이 확인되었습니다. 2주간의 광치료 후 뇌 내 현출성 네트워크(salience network)의 기능적 연결성이 감소되었습니다<sup>225</sup>. 이러한 뇌 내 네트워크의 변화는 수면의 효율과 연관성이 있는 것으로 나타났습니다.<br><br>광치료는 치료자 없이 스스로 수행이 가능하고 치료 장치의 휴대가 간편하다는 장점이 있어 접근성이 높습니다.<br>또한 치료 중 다른 활동을 할 수 있기 때문에, 야간 근무 중에도 치료가 가능합니다. 따라서 교대근무 근로자의 수면 문제를 해소하기 위한 효과적인 치료법으로 여러 임상 연구들이 진행되고 있습니다<sup>226</sup>. 이에 따라 직종별 교대근무 형태에 맞춘 치료 프로토콜 개발을 기대할 수 있습니다.",
        "reference" : "<sup>221</sup> Berson et al., (2002). Phototransduction by retinal ganglion cells that set the circadian clock. Science, 1070-<br><sup>222</sup> Czeisler et al., (1989). Bright light induction of strong (type 0) resetting of the human circadian pacemaker. Science, 1328-33.<br><sup>223</sup> Tamarkin et al., (1979). Regulation of pineal melatonin in the Syrian hamster. Endocrinology, 385-9.<br><sup>224</sup> Minors et al., (1991). A human phase-response curve to light. Neurosci Lett, 36-40.<br><sup>225</sup> Jiyoung Ma et al., (2020). Decreased functional connectivity within the salience network after two-week morning bright light exposure in individuals with sleep disturbances: a preliminary randomized controlled <br><sup>226</sup> Gooley, (2008). Treatment of circadian rhythm sleep disorders with light. Ann. Acad. Med. Singap., 669-76.",
        "img_top" : "part3_2-2.jpeg",
        "img_top_size" : "full"
      },
      {
        "subtitle_box" : "기분 변화",
        "subtitle_text" : "반복 경두개자기자극 (Repetitive Transcranial Magnetic Stimulation, rTMS)",
        "content" : "반복적 경두개자기자극은 전자기코일로 자기장을 형성하여 신경세포의 활성도를 조절하는 방법입니다<sup>246</sup>.<br>전자기코일을 두피 위에 위치하면 전자기코일이 자기장을 형성시켜 신경세포의 탈분극을 조절합니다. <br><br>경두개자기자극은 비침습적인 방법으로 우울장애, 강박장애, 운동장애, 등 여러 질환의 증상을 개선하는 방법으로 활발하게 연구되고 있습니다. 그 중 치료 저항성 우울장애 치료에 한국 식품의약품안전처와 미국 식품의약국(Food and Drug Administration, FDA)의 승인을 받았습니다. <br><br>우울장애를 대상으로 하는 반복적 경두개자기자극 치료는 한 회에 약 40분이 소요되고, 일주일에 5일, 4주에서 6주 동안 치료를 받습니다<sup>247</sup>.<br>반복 경두개자기자극은 일반적으로 편안한 의자에 앉아 치료를 받게 됩니다. 반복적 경두개자기자극은 마취나 안정제가 필요 없는 치료 방법으로, 치료 후 바로 일상생활이 가능합니다.<br><br>반복적 경두개자기자극은 안전성이 입증된 치료법입니다. 다만, 발작의 경험이 있거나 발작 관련 가족력이 있는 경우, 또는 몸에 금속 또는 의료기기가 삽입되어 있는 경우에는 치료를 시작하기 전 의사와 충분히 상의하세요.",
        "reference" : "<sup>246</sup> Hallet., (2000) Transcranial magnetic stimulation and the human brain. Nature, 147-50.<br><sup>247</sup> NeuroStar. (2020년 7월 7일) URL: <a href='https://neurostar.co.kr/?page_id=76'>https://neurostar.co.kr/?page_id=76</a>",
        "img_top" : "part3_2-5.png",
        "img_top_size" : "full"
      }, 
      {
        "subtitle_box" : "집중과 기억 어려움",
        "subtitle_text" : "경두개 직류자극 (Transcranial Direct Current Stimulation, tDCS)",
        "content" : "경두개직류자극은 두피에 미세한 직류 전류를 흘려주어 뇌세포의 활성을 조절하는 치료법입니다<sup>252</sup>. 경두개직류자극은 대뇌 피질의 기능을 조절하고 신경가소성을 활성화하여 인지기능을 개선하는 것으로 알려져 있습니다. 경두개직류자극의 효과는 전극의 종류, 위치, 그리고 세기에 따라 달라질 수 있습니다.<br><br>경두개직류자극 기기는 손바닥보다 작은 크기이고 사용법이 손쉬워 교육을 받은 후에는 집에서 스스로 처치를 시행할 수 있다는 장점이 있습니다. <br><br>두피와 경두개직류자극 사이에 전류가 잘 흐를 수 있도록 식염수를 적신 스펀지 형태의 패치가 필요합니다. 밴드 또는 캡을 사용하여 패치와 경두개직류자극 기기를 두피에 고정할 수 있습니다. 일반적으로 처치 시간은 5분에서 30분 사이이며, 꾸준히 처치를 받는 경우 임상적인 개선 효과를 기대할 수 있습니다<sup>253</sup>. <br><br>경두개직류자극은 심각한 부작용이 거의 없는 안전한 치료법입니다<sup>254</sup>. <br><br>치료 중 또는 치료 직후 전극을 부착한 부위의 가려움, 따끔거림, 불편감, 두통 등의 증상이 일시적으로 있을 수 있지만, 오래 가지 않습니다. 처치를 시작하기 전에 두피에 상처가 없는지 확인하는 단계가 필요합니다.",
        "reference" : "<sup>252</sup> Nitsche et al., (2008). Transcranial direct current stimulation: State of the art 2008. Brain Stimul, 206-23.<br><sup>253</sup> Thair et al., (2017). Transcranial Direct Current Stimulation (tDCS): A Beginner's Guide for Design and Implementation. Front Neurosci, 641<br><sup>254</sup> Brunoni et al., (2011). A systematic review on reporting and assessment of adverse effects associated with transcranial direct current stimulation. Int J Neuropsychopharmacol, 1133-45.",
        "img_top" : "part3_2-6.png",
        "img_top_size" : "full"
      },
      {
        "subtitle_bubble_q" : "또 어떤 생활습관이 도움이 되나요?",
        "subtitle_box" : "화재 현장 담당",
        "content" : "소방공무원은 화재현장에서의 유해가스 노출을 비롯하여 각종 위험 상황에 긴급 투입되는 등 정신적인 스트레스에도 자주 노출됩니다. 이처럼 소방공무원의 흡연이나 음주<sup>203</sup>는 직무상 스트레스와 연관이 있을 수 있습니다.<br><br>소방공무원은 화재진압과정에서 노출되는 많은 유해가스로 인해 폐기능 저하와 여러 호흡기 증상을 호소할 수 있습니다.<br>호흡기 증상은 유해가스 노출뿐만 아니라 흡연의 영향도 있을 수 있습니다. 담배는 4,000개 이상의 독성물질과 발암물질을 포함하고 있어 만성 기관지염, 폐기종, 폐암 등 여러 호흡기 질환을 유발할 수 있습니다.<br>따라서 금연은 화재 진압 시 호흡기 보호구의 착용만큼이나 호흡기 질환 예방에 도움이 될 수 있습니다<sup>204</sup>.<br><br>또한, 술을 마시게 되면 순간적으로 근무 중에 있었던 상황으로부터 벗어나 기분을 좋아지게 할 수 있으나, 습관적인 음주는 뇌 손상과 기능 장애의 원인이 될 수 있고, 인지 장애, 소뇌 퇴화, 알코올성 치매 등 신경정신과적 증상을 초래할 수 있습니다<sup>205</sup>.<br>또한 알코올이 몸 안에서 분해되는 과정에서 생기는 아세트알데히드는 신경에 직접적인 영향을 주는 독성을 가진 것으로 알려져 있습니다. 따라서 습관적인 음주는 인지 기능 장애를 비롯한 신경학적 이상이나 간 질환의 원인이 될 수 있습니다.<br><br>음주 유형은 ‘적정 음주’와 ‘위험 음주’로 구분할 수 있는데, ‘적정 음주’는 음주량과 패턴을 모두 감안하였을 때, 자신과 타인에게 해가 되지 않는 수준의 음주를 말합니다<sup>206</sup>.<br>세계보건기구(World Health Organization, WHO)에 따른 하루 적정 음주 범위를 알코올 섭취량으로 환산해보면, 남자는 소주 약 3잔에 해당하는 하루 40g 미만이고 여자는 소주 약 2잔에 해당하는 20g입니다.<br><br>그러나 최근에는 소량의 음주도 다양한 암 발생 위험을 높일 수 있다는 연구결과가 발표되었으므로 음주는 최대한 지양하시는 것을 권해드립니다.",
        "reference" : "<sup>203</sup> 조선덕 외, (2012). 소방공무원의 직무스트레스와 음주사용장애와의 관련성. 한국방재학회, 133-40.<br><sup>204</sup> 김성훈 외, (2006). 부산지역 소방공무원의 폐기능과 호흡기증상. 대한직업환경의학회지, 103-11.<br><sup>205</sup> Butterworth, (1995). Pathophysiology of alcoholic brain damage: synergistic effects of ethanol, thiamine deficiency and alcoholic liver disease. Metab. Brain Dis., 1-8.<br><sup>206</sup> 질병관리본부 건강정보. (20200707). URL: <a href='http://health.cdc.go.kr/health/mobileweb/content/group_view.jsp?CID=D7J4LLVAZY'>http://health.cdc.go.kr/health/mobileweb/content/group_view.jsp?CID=D7J4LLVAZY</a>"
      },
      {
        "subtitle_box" : "교대근무",
        "content" : "교대근무를 하는 소방공무원은 매일 정해진 시간에 잠자리에 드는 것이 어려울 수 있습니다. 이러한 수면 시간의 불규칙성은 직무상 스트레스를 유발할 수 있으며, 직무 스트레스를 줄이기 위하여 흡연이나 음주를 하는 경우가 있습니다<sup>227</sup>. 하지만 숙면을 위해서는 담배와 술을 멀리하는 것이 좋습니다.<br><br>니코틴은 낮은 농도에서는 신체적 이완을 유도하지만, 농도가 높아지면 오히려 각성을 유발할 수 있습니다. 이러한 변화는 급격하게 일어나므로 야간의 흡연은 각성 효과가 있다고 할 수 있습니다. 따라서 저녁시간에는 흡연을 피하시는 편이 숙면에 좋습니다.<br><br>마찬가지로, 술을 마시면 쉽게 잠이 든다고 느껴질 수 있습니다. 하지만 소량의 알코올이라도 수면 분절(sleep fragmentation) 등의 수면문제를 유발하는 것으로 알려져 있습니다<sup>228</sup>. 수면 분절은 자는 도중 여러 번 깨어나서 수면 시간이 길지 않고 중간중간 끊어지는 증상을 의미합니다. 이러한 수면 분절을 겪으면, 자고 일어난 후 개운하지 않고 피로감이 지속됩니다. 따라서 깊은 수면을 위해서는 음주는 삼가는 것을 권해드립니다.",
        "img_bottom": "part3_2-3.png",
        "img_bottom_size": "enlarge",
        "text_add" : "<span class='img-ref'>활동계측기를 이용하여 이와 같이 수면 및 활동시간을 기록할 수 있습니다.</a>"
      },
      {
        "content" : "수면 및 활동시간을 측정할 수 있는 기기를 통해 매일 활동 패턴을 기록하는 것은 수면 습관 개선에 도움이 될 수 있습니다<sup>229</sup>.<br>사진 속에서 착용하는 기기는 수면각성활동량 검사 기기 (Actigraph)로, 착용 시간 동안의 활동량을 측정하여 수면 시간과 활동 시간을 구분하여 분석할 수 있습니다. 스스로 착용할 수 있으며, 직접 결과를 확인할 수 있습니다.<br><br>활동 기록을 점검하면서, 수면 시간 외에 침대 등 잠자리에서 하는 활동을 줄이고 운동량을 늘리는 등 숙면에 도움이 되는 방향으로 일상적인 행동을 개선할 수 있습니다. 좋은 수면 습관이 점자 길러지면 활동계측기에서 긍정적인 피드백을 기대할 수 있을 것입니다.<br><br>이처럼 좋은 수면을 위해 노력하고 수면에 대한 긍정적인 피드백을 받으면 시너지 효과가 생길 수 있습니다. 긍정적인 피드백만으로도 수면 문제 개선에 도움이 될 수 있습니다<sup>230</sup>.",
        "reference" : "<sup>227</sup> 조선덕 외, (2012). 소방공무원의 직무스트레스와 음주사용장애와의 관련성. 한국방재학회, 133-40.<br><sup>228</sup> Rouhani et al., (1989). EEG effects of a single low dose of ethanol on afternoon sleep in the nonalcohol-dependent adult. Alcohol, 87-90.<br><sup>229</sup> 김석주, (2020). 불면장애 진단과 치료의 최신 지견. 신경정신의학, 2-12.<br><sup>230</sup> Gavriloff, D., Sheaves, B., Juss, A., Espie, C. A., Miller, C. B., & Kyle, S. D. (2018). Sham sleep feedback delivered via actigraphy biases daytime symptom reports in people with insomnia: Implications for insomnia disorder and wearable devices. Journal of sleep research, 27(6), e12726.",
        "img_bottom": "part3_2-4.jpeg",
        "img_bottom_size": "full"
      },
      {
        "subtitle_box" : "유난히 생각나는 현장",
        "content" : "먼저, 몸 건강을 챙기는 것이 곧 마음의 건강을 챙기는 방법입니다<sup>240</sup>. 충분한 휴식을 취하고, 운동을 규칙적으로 하고, 균형 있는 식사를 잘 챙기도록 노력하세요. 몸이 건강하면 마음 건강을 되찾는데 도움을 줄 수 있습니다.<br><br>건강한 회복을 위해서 주위 사람들의 도움이 필요합니다.<br><br>선생님을 생각해주는 사람들과 시간을 보내세요. 사건에 관한 이야기를 하지 않더라도 괜찮습니다. 가족들 또는 친구들의 지지는 선생님의 마음을 편안하게 해줄 것입니다.<br>이야기할 준비가 되신다면, 선생님을 이해할 수 있는 사람들에게 선생님의 감정을 이야기해보세요. 사건에 대해 말하는 과정은 회복 단계의 한 부분으로, 사건을 자연스럽게 받아들일 수 있도록 도와줍니다. <br><br>카페인이 함유된 차, 커피, 탄산음료, 설탕처럼 긴장을 유발하는 음식은 멀리 하는 것이 좋습니다<sup>241</sup>. 술 또는 담배로 힘듦을 견뎌내는 것은 좋지 않은 습관입니다. 이들은 마음의 고통을 해결해주지 못하며, 장기적으로 술과 담배는 몸과 마음의 건강을 해칩니다.",
        "reference" : "<sup>240</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013) Recovery after Trauma - A guide for people with Posttraumaic stress disorder. URL:<a href='https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf'>https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf</a><br><sup>241</sup> Phoenix Australia Centre for Posttraumattic Mental Health (2013) Recovery after Trauma - A guide for people with Posttraumaic stress disorder. URL:<a href='https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf'>https://phoenixaustralia.org/wp-content/uploads/2015/03/Phoenix-Adults-Guide.pdf</a>",
      },
      {
        "subtitle_box" : "기분 변화",
        "content" : "신체 활동과 운동은 우울 증상을 감소시킬 수 있습니다<sup>248</sup>. 산책, 달리기, 수영 등 선생님이 즐겁게 할 수 있는 운동을 꾸준히 할 것을 권장합니다. 술과 담배는 멀리하는 것이 좋습니다. 우울 증상을 악화시킬 수 있습니다. 규칙적이고 균형 잡힌 식습관을 가지는 것 또한 우울 증상 개선에 도움을 줄 수 있습니다. <br><br> <b>가족 또는 동료가 우울해하는 것 같아요. 어떻게 도울 수 있을까요?</b><br><br>우울장애로 인해 짜증을 쉽게 내거나, 무기력하거나, 약속을 지키지 않는 등의 행동적 변화를 나타낼 수 있습니다. <br>우울장애가 의심된다면 이러한 행동을 비난하지 않고 차분히 대화를 나누어보세요. 섣부른 충고보다는 경청하는 자세를 보여주어 가족 또는 동료가 감정을 표현할 수 있도록 돕는 것이 좋습니다. <br>우울 증상이 심하면 부정적인 생각이 지배하기 쉬워서 치료의 효과에 대해 부정적으로 생각하는 경우가 많습니다. 그렇기 때문에 가족, 동료 등 가까운 사람의 지지와 역할이 중요합니다. 우울장애 치료를 받도록 적극적으로 권유하길 바랍니다. <br>혹시 자살에 대해서 언급한다면, 즉각적으로 치료를 받도록 할 필요가 있습니다. <br>우울장애는 치료가 잘 되는 질환이라는 사실을 환기해주세요.",
        "reference" : "<sup>248</sup> 질병관리본부 국가건강정보포털. (2020년 07월 07일) URL: <a href='http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=1200'>http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=1200</a>",
      },
      {
        "subtitle_box" : "집중과 기억 어려움",
        "content" : "뇌건강을 지킬 수 있는 생활 습관은 다음과 같습니다.<br><br>규칙적인 운동은 혈액순환을 돕고 뇌세포를 활성화하여 뇌를 건강하게 만듭니다. 걷기, 자전거 타기 등의 유산소운동과 가벼운 근력운동을 한 회에 30분 이상, 일주일에 3회 이상 숨이 약간 찰 정도의 강도로 하는 것이 좋습니다<sup>255</sup>. <br><br>또한 두뇌활동을 지속적으로 하는 것이 좋습니다<sup>256</sup>. 책을 읽는 것, 글을 쓰는 것, 퍼즐을 푸는 것과 같이 머리를 쓰는 활동을 취미로 삼아 꾸준히 해보세요. 메모하는 습관, 자주 사용하는 물건을 정해진 위치에 보관하는 습관을 가지는 것이 도움이 됩니다. <br><br>술과 담배는 뇌건강에 해로우므로 멀리하는 것이 좋습니다. 알코올은 뇌를 손상시키고, 담배의 해로운 성분들은 작은 혈관들을 막히게 할 수 있습니다. <br><br>건강한 식습관으로 뇌를 건강하게 할 수 있습니다. 고등어, 연어, 호두 등 오메가3 지방산을 풍부하게 함유하고 있는 음식이 인지기능 개선에 도움이 될 수 있습니다<sup>257</sup>. 녹차, 은행 등이 함유하고 있는 플라보노이드는 인지기능 개선을 도울 수 있습니다. <br><br>치매는 동반 질환이 있거나 연령이 증가할 수록 발생 위험이 높아집니다. 뿐만 아니라, 흡연, 과음, 운동 부족과 같은 잘못된 생활 습관 또한 치매 발생 위험을 높입니다. 따라서 건강한 생활습관을 유지하는 것이 인지기능 저하를 예방하기 위한 지름길입니다. ",
        "reference" : "<sup>255</sup> 이윤환 외., (2009). 치매예방을 위한 생활습관. 한국노인병학회, 61-8.<br><sup>256</sup> Wilson et al., (2007). Relation of cognitive activity to risk of developing Alzheimer disease. Neurology. 1911-20.<br><sup>257</sup> Gómez-Pinilla. (2008). Brain foods: the effects of nutrients on brain function. Nat Rev Neurosci, 568-78 ",
      }
    ],
    // 2-1 상태체크에서 모두 아니오를 선택했을 경우
    "5" : [
      {
        "content" : "현재 겪고 있는 어려움이 없는 상태입니다.<br>\"네\"를 클릭하시면 혹시 겪으실 수도 있는 어려움에 도움이 될 수 있는 정보가 있습니다.<br><br>"
      }
    ]
  }

  checkQuestion: any = [
    {
      title: "화재 현장 출동을 담당하시나요?",
      id: "check1",
      value: 'no'
    },
    {
      title: "교대근무를 하고 계신가요?",
      id: "check2",
      value: 'no'
    },
    {
      title: "유난히 생각나는 현장이 있으신가요?",
      id: "check3",
      value: 'no'
    },
    {
      title: "기분 변화가 있는 것 같으신가요?",
      id: "check4",
      value: 'no'
    },
    {
      title: "집중하거나 기억하는 데 어려움이 있으신가요?",
      id: "check5",
      value: 'no'
    }
  ]

  constructor(
    private elRef: ElementRef,
    public renderer: Renderer,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
    private iab: InAppBrowser) {
    this.selectedContent = this.navParams.get('content');
    this.selectedContentFilter = this.navParams.get('contentFilter');
    this.subcontent = this.navParams.get('subcontent');
  }

  ionViewDidLoad() {
    // json 중 link 연결이 필요한 내용이 첨부되어있을 경우 listen, 클릭 이벤트 발생시 처리
    if (this.elRef.nativeElement.querySelectorAll('a')) {
      let links = this.elRef.nativeElement.querySelectorAll('a');
      for (let link of links) {
        this.renderer.listen(link, 'click', (evt) => {
          this.goLink(link.getAttribute('href'));
          return false;
        });
      }
    }
  }

  openEnlarge(src) {
    let enlargeModal = this.modalCtrl.create( EnlargeModalPage, { src: src });
    enlargeModal.onDidDismiss(data => {
      console.log(data);
    });
    enlargeModal.present();
  }

  getResult(){
    let checkedArr = [];
    this.checkQuestion.forEach((check, index) => {
      if (check.value == 'yes' ) checkedArr.push( '2-'+(index+2)+'-1', '2-'+(index+2)+'-2', '2-'+(index+2)+'-3' )
    });

    if(checkedArr.length != 0) this.navCtrl.push(this.contentDetail, { 'contentFilter' : checkedArr });
    else this.navCtrl.push(this.contentDetail, { 'content' : '5' });
  }

  goLink(url) {
    if (!url) return;
    this.iab.create(url);
  }

  goList(){
    this.navCtrl.push(this.contentPart3, {}, { animate: true, direction: 'back' })
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}
