import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Part3DetailPage } from './part3-detail/part3-detail';
import { HomePage } from '../../home/home';

@Component({
  selector: 'page-part3',
  templateUrl: 'part3.html',
})
export class Part3Page {

  public content = Part3DetailPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  contentList: any = {
    "subject" : "치료 및 예방",
    "btn" : [
      { "title" : "뇌기능 저하 예방법", "content" : "1" },
      { "title" : "현재 나의 상태 체크", "content" : "2-1"
        // "detail" : [
          // { "subject" : "나의 상태 체크", 
          //   "subBtn" : [
          //     { "title" : "상태 체크", "content" : "2-1" }
          //   ]
          // },
          // { "subject" : "화재현장 담당", 
          //   "subBtn" : [
          //     { "title" : "유해물질에 노출되었을 때 치료 방법에는 무엇이 있나요?", "content" : "2-2-1" },
          //     { "title" : "새롭게 개발된 치료 방법에는 무엇이 있나요?", "content" : "2-2-2" },
          //     { "title" : "일상생활에서도 도움이 되는 것들이 있나요?", "content" : "2-2-3" }
          //   ]
          // },
          // { "subject" : "교대근무", 
          //   "subBtn" : [
          //     { "title" : "교대근무로 인한 수면장애는 어떻게 치료하나요?", "content" : "2-3-1" },
          //     { "title" : "새롭게 개발된 치료 방법에는 무엇이 있나요?", "content" : "2-3-2" },
          //     { "title" : "일상생활에서도 도움이 되는 것들이 있나요?", "content" : "2-3-3" }
          //   ]
          // },
          // { "subject" : "유난히 생각나는 현장", 
          //   "subBtn" : [
          //     { "title" : "외상후스트레스장애 치료는 어떻게 하나요?", "content" : "2-4-1" },
          //     { "title" : "새롭게 개발된 치료 방법에는 무엇이 있나요?", "content" : "2-4-2" },
          //     { "title" : "일상생활에서도 도움이 되는 것들이 있나요?", "content" : "2-4-3" }
          //   ]
          // },
          // { "subject" : "기분 변화", 
          //   "subBtn" : [
          //     { "title" : "우울 장애 치료는 어떻게 하나요?", "content" : "2-5-1" },
          //     { "title" : "새롭게 개발된 치료 방법에는 무엇이 있나요?", "content" : "2-5-2" },
          //     { "title" : "일상생활에서도 도움이 되는 것들이 있나요?", "content" : "2-5-3" }
          //   ]
          // },
          // { "subject" : "집중과 기억 어려움", 
          //   "subBtn" : [
          //     { "title" : "인지기능 저하 치료 방법에는 무엇이 있나요?", "content" : "2-6-1" },
          //     { "title" : "새롭게 개발된 치료 방법에는 무엇이 있나요?", "content" : "2-6-2" },
          //     { "title" : "일상생활에서도 도움이 되는 것들이 있나요?", "content" : "2-6-3" }
          //   ]
          // },
        // ]
      },
      { "title" : "뇌기능 저하 예방의 중요성", "content" : "3" },
    ]
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }
}
