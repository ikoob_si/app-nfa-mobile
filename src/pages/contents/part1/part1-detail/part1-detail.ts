import { Component, ElementRef, Renderer, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Part1Page } from '../part1';
import { EnlargeModalPage } from '../../../enlarge-modal/enlarge-modal';
import { HomePage } from '../../../home/home';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-part1-detail',
  templateUrl: 'part1-detail.html',
})
export class Part1DetailPage {
  @ViewChild('contentElement') contentElement: ElementRef;

  public contentPart1 = Part1Page;
  public contentDetail = Part1DetailPage;
  public selectedContent: string;
  public subcontent: any;

  contents: any = {
    "1" : [
      {
        "title" : "1. 유해인자 설명",
        "subtitle_bubble_q" : "유해인자란 무엇인가요?",
        "content" : "유해인자란 근로자의 건강장해를 유발하는 화학물질 및 물리적 인자 등으로 산업안전보건법에서 규정하며, 건강장해를 예방하기 위하여 유해인자를 체계적으로 분류하여 관리토록 하고 있습니다<sup>1</sup>. 또한, 직무와 관련된 정신적 스트레스도 유해인자로 간주되어 관련 연구가 지속적으로 이루어지고 있습니다.<br><br>국제노동기구(International Labor Organization, ILO)에서는 소방공무원을 재난현장에서 활동함으로써 여러 종류의 유해인자에 노출될 수 있는 직업군으로 정의하고 있습니다<sup>2</sup>.<br><br>소방공무원이 근무 현장에서 접할 수 있는 유해인자들을 클릭하여 확인해보세요.",
        "reference" : '<sup>1</sup> 산업안전보건법 제104조(유해인자의 분류기준). <a href="http://www.law.go.kr/법령/산업안전보건법">http://www.law.go.kr/법령/산업안전보건법</a> <br><sup>2</sup> International Labour Organizations. Fire-fighter: International Hazard Datasheets on Occupation. (2020년 6월 30일). URL: <a href="https://www.ilo.org/safework/cis/WCMS_193142">https://www.ilo.org/safework/cis/WCMS_193142</a>'
      }
    ],
    "2-1-1" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "물리적 유해인자 : 고열",
        "subtitle_text" : "고열",
        "content" : "화재현장에서는 화염에 직접적으로 노출되거나, 고온의 복사열에 간접적으로 노출될 수 있습니다<sup>3</sup>.<br>방화복, 방열복과 같은 개인보호장비를 착용하여 고열로 인한 피해를 최소화할 수 있으나, 보호장비를 착용한 소방업무 시에도 열 발산이 잘 되지 않으면 체온이 37.9~39.0℃까지 이를 수 있으므로<sup>4</sup> 주의가 필요합니다<sup>5</sup>.",
        "reference" : "<sup>3</sup> 박찬석, (2014). 재난 대응 활동 시 노출가능 유해인자 종합분석-화재 현장을 중심으로. 한국재난정보학회 논문집, 420-30.<br><sup>4</sup> DUNCAN HW et al., (1979). Physiological responses of men working in fire fighting equipment in the heat. Ergonomics, 521-7.<br><sup>5</sup> McLELLAN TM et al., (2006). The management of heat stress for the firefighter: a review of work conducted on behalf of the Toronto Fire Service. Industrial health, 414-26.",
        "img_top": "part1_2-1.png",
        "img_top_size": "small"
      }, {
        "subtitle_text" : "고열이 신체에 미치는 영향",
        "content" : "체온은 인체의 항상성 기능에 의해 35.8~37.2℃(고막에서 측정한 체온 기준)으로 유지됩니다<sup>6</sup>. 실내에서 화재의 발생하면 실내 온도는 보통 500~1000℃로 올라갑니다.<br>신체 외부의 고열에 의해 발생할 수 있는 건강장해로는 열사병, 열탈진, 열경련, 열실신, 열발진(땀띠) 등이 있습니다<sup>7</sup>. 화재 진압 중 착용하는 보호장비는 안전하게 화재진압 작업을 하는 데 도움이 되지만 체내 열 발산을 방해하기도 합니다<sup>8</sup>.<br>화재현장 출동 후 어지럼증, 오심, 구토, 근육 경련 등의 증상이 있다면 병원에서의 적절한 치료가 필요합니다.<br>화재현장에서 화염에 직접적으로 노출되거나 뜨거운 증기에 노출되는 경우 화상을 입을 수 있습니다<sup>9</sup>. 손상된 조직의 깊이에 따라 1도 화상, 2도 화상, 3도 화상, 4도 화상으로 구분할 수 있습니다.",
        "reference" : "<sup>6</sup> Sund‐Levander M et al., (2002). Normal oral, rectal, tympanic and axillary body temperature in adult men and women: a systematic literature review. Scand J Caring Sci, 122-8.<br><sup>7</sup> 김정만 외, (2018). 소방관들이 화재현장에서 노출되는 유해물질들-물리적, 화학적 및 생물학적 요인. J Korean Med Assoc, 1072-7.<br><sup>8</sup> DUNCAN HW et al., (1979). Physiological responses of men working in fire fighting equipment in the heat. Ergonomics, 521-7.<br><sup>9</sup> Jeschke MG et al., (2020). Burn injury. Nat Rev Dis Primers.",
        "img_top": "part1_2-2.png",
        "img_top_size": "mid"
      }
    ],
    "2-1-2" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "물리적 유해인자 : 소음",
        "subtitle_text" : "소음",
        "content": "소방공무원들은 사이렌, 엔진, 펌프 등에서 생기는 높은 소음에 노출될 수 있습니다. 강렬한 소음 작업의 법적인 기준치는 90 dB(A)(1일 8시간 기준)입니다<sup>10</sup>. 일반적으로 지하철에서의 소음은 80 dB(A) 수준이며, 자동차의 경적 소음은 110 dB(A) 정도입니다<sup>11</sup>.<br>화재현장에서는 평균적으로 80 dB(A) 수준의 소음이 발생한다고 알려져 있으며, 간혹 출동 중 110 dB(A)를 넘는 고소음에 노출될 수 있습니다<sup>12</sup>.<br><br>*dB(A): 실제 사람의 귀로 들을 수 있는 음의 크기를 상대적인 단위(dB)로 나타낸 값",
        "reference": "<sup>10</sup> 화학물질 및 물리적 인자의 노출기준 [시행 2020. 1. 16.] [고용노동부고시 제2020-48호, 2020. 1. 14., 일부개정] <a href='http://www.law.go.kr/행정규칙/화학물질및물리적인자의노출기준/(2013-38,20130814)'>http://www.law.go.kr/행정규칙/화학물질및물리적인자의노출기준/(2013-38,20130814)</a><br><sup>11</sup> 국가소음정보시스템. (2020년 7월 2일). URL: <a href='http://www.noiseinfo.or.kr/about/info.jsp?pageNo=942'>http://www.noiseinfo.or.kr/about/info.jsp?pageNo=942</a><br><sup>12</sup> 이임규 외, (2011). 소방 공무원의 시간활동 양상과 직무에 따른 소음 노출 특성. 한국환경보건학회지, 92-101.",
        "img_top": "part1_2-3.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "소음이 신체에 미치는 영향",
        "content": "소음은 노출 강도 및 기간, 소음의 특성, 소음 민감도에 따라 인체에 다양한 영향을 미칠 수 있습니다. 소음에 지속적으로 노출될 경우 소음성 난청과 이명 증상이 나타날 수 있습니다.<br><br>85 dB(A) 이상의 소음에 지속적으로 노출되면 귀에서 소리를 감지하는 기관인 달팽이관의 유모세포가 손상되어 소음성 난청이 발생할 수 있습니다<sup>13</sup>. 유모세포는 고음을 처리하는 외유모세포와 저음을 처리하는 내유모세포로 구분할 수 있으며, 고음에 노출될 경우 일반적으로 외유모세포가 먼저 손상되고 내유모세포의 손상이 뒤따릅니다<sup>14</sup>. 그러므로 소음성 난청의 경우 공통적으로 고음 영역에서 청력 저하를 보이며, 이명을 동반하는 경우가 많습니다.<br>화재현장에서는 평균적으로 80 dB(A) 수준의 소음이 발생한다고 알려져 있으며, 출동하는 짧은 순간 동안에는 110 dB(A)를 넘는 고소음에 노출될 수 있습니다<sup>15</sup>. 소음성 난청이 발생한 경우 텔레비전이나 라디오 등을 크게 켜 놓는 모습을 보이며, 주변이 조금만 시끄러워도 상대방의 이야기를 알아듣는 것이 어려울 수 있습니다.<br><br>이명은 외부 청각적인 자극이 없는데도 불구하고 원치 않는 소리가 들리는 주관적인 느낌을 말합니다<sup>16</sup>. 소음에 의한 내이 손상은 이명의 가장 흔한 원인 중 하나로 음악가, 항공기 조종사처럼 직업으로 인해 지속적으로 내이 손상을 입는 경우에 흔하게 나타나며 큰 음악소리 등에 우발적으로 노출되는 경우에도 생길 수 있습니다.<br><br>소음성 난청과 이명 증상이 있는 경우 청력 문제 외에도 불쾌감, 불안감, 불면증, 피로, 스트레스, 두통 등의 어려움을 겪을 수 있습니다<sup>17</sup>. 2008년 한국산업안전공단 산업안전보건연구원에서 진행한 조사결과에 따르면 4,462명의 소방공무원 표본에서 난청과 이명증상 호소율이 각각 22.7%, 13.4%로 나타났습니다<sup>18</sup>.",
        "reference": "<sup>13</sup> Rabinowitz, P. (2000). Noise-induced hearing loss. Am Fam Physician, 2749-56.<br><sup>14</sup> Bohne BA et al., (1987). Cochlear damage following interrupted exposure to high-frequency noise. Hear Res, 251-64.<br><sup>15</sup> 이임규 외, (2011). 소방 공무원의 시간활동 양상과 직무에 따른 소음 노출 특성. 한국환경보건학회지, 92-101.<br><sup>16</sup> Eggermont JJ et al., (2004). The neuroscience of tinnitus. Trends Neurosci, 676-82.<br><sup>17</sup> Gomaa MAM et al., (2014). Depression, Anxiety and Stress Scale in patients with tinnitus and hearing loss. Eur Arch Oto-Rhino-Laryn, 2177-84.<br><sup>18</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.",
        "img_top": "part1_2-4.png",
        "img_top_size": "mid",
        "img_bottom": "part1_2-5.png",
        "img_bottom_size": "enlarge",
        "text_add": "<span class='img-ref'>출처: 국가소음정보시스템. (2020년 7월 2일). URL: <a href='http://www.noiseinfo.or.kr/about/info.jsp?pageNo=942'>http://www.noiseinfo.or.kr/about/info.jsp?pageNo=942</a></span>"
      }
    ],
    "2-1-3" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "물리적 유해인자 : 중량물 작업",
        "subtitle_text" : "중량물 작업",
        "content": "구조·구급·화재진압 업무는 무거운 구조장비와 진화장비, 호흡장비, 방화복 등을 착용하고 직무를 수행하게 되므로 하중이라는 물리적 유해인자에 노출될 수 있습니다<sup>19,20</sup>.<br>장시간 동안의 반복 작업, 환자 이송, 긴급한 상황에서의 부적절한 작업 자세로 인하여 근골격계에 반복적으로 무리한 하중이 가해지면 통증, 관절염, 추간판탈출증 등 여러 근골격계 질환이 발생할 수 있습니다<sup>21</sup>.",
        "reference": "<sup>19</sup> 김정만 외, (2007). 소방관의 근골격계 증상과 직무 스트레스에 관한 연구. 한국산업위생학회지, 111-9.<br><sup>20</sup> 국가인권위원회, (2015). 소방공무원의 인권상황 실태조사.<br><sup>21</sup> 윤장원, (2016). 소방공무원의 근골격계 질환: 2011 년~ 2013 년 공상신청 분석. 한국화재소방학회 논문지, 133-7.",
        "img_top": "part1_2-6.png",
        "img_top_size": "small"
      }, {
        "subtitle_text" : "중량물 작업이 신체에 미치는 영향",
        "content": "구조, 구급, 화재진압 현장에서 중량물 작업으로 인한 무리한 하중은 요통을 유발할 수 있습니다<sup>22</sup>.<br>요통은 다양한 원인에 의해 허리 부위의 통증이 발생하는 증상입니다<sup>23</sup>. 구급대원의 경우 들것을 이용하여 환자를 이송하는 업무를 수행하며, 요통 증상 호소 비율이 높은 것으로 조사된 바 있습니다<sup>24</sup>.<br>화재진압 중에는 물을 뿜는 소방호스를 다룰 때나 구조가 필요한 물체를 챙길 때, 사다리를 오르거나 중량물을 들 때, 몸통을 비틀거나 몸통을 뻗는 작업, 파괴작업, 남아있는 불씨를 찾는 등 여러 작업 시에 허리에 과도한 하중이 가중되어 요통이 발생할 수 있습니다.<br><br>척추뼈들을 연결하는 인대와 척추뼈 주위를 둘러싸고 있는 다양한 근육들은 허리를 굽히고 펴는 등의 운동을 가능하게 합니다. 척추뼈 내부 공간에는 척수가 지나가고 있으며, 척추뼈 사이로 신경들이 뻗어 나옵니다. 또한 각 척추뼈 사이에는 연골 구조물인 추간판(디스크, disc)이 들어 있어 충격을 흡수하는 쿠션과 같은 역할을 담당합니다.<br><br>요통의 가장 흔한 원인은 추간판의 퇴행입니다. 척추뼈 사이에 위치한 추간판이 손상되고 변성되는 퇴행이 진행되면서 충격을 흡수하는 능력이 감소하게 되고, 결과적으로 허리를 움직이거나 힘을 쓸 때 통증이 유발될 수 있습니다. 추간판이 파열된 경우는 추간판탈출증이 발생할 수 있습니다. 튀어나온 추간판은 척수 신경 및 주변 구조물을 자극하여 통증을 일으킵니다.<br><br>이외에도 척추의 배열척추측만증, 척추관이 서서히 좁아지면서 그 속을 지나가는 척추신경을 압박하는 척추관협착증, 퇴행성 관절염, 정신적 스트레스 등에 의해서도 요통이 발생할 수 있습니다.<br>요통은 통증의 지속 기간, 통증의 정도, 발생 빈도가 무척 다양합니다. 쑤시는 느낌, 뻣뻣함, 화끈거리는 느낌, 무감각 혹은 찌릿찌릿함, 다리 저림 등의 증상이 나타날 수 있습니다.<br><br>중량물 작업으로 인해 요통 외에도 퇴행성 관절염(골관절염)이 생길 수 있습니다<sup>25</sup>. 관절은 두 개 이상의 뼈들이 맞닿는 곳으로, 관절을 이루는 뼈들의 끝이 부드러운 재질의 연골로 싸여 있어 관절을 부드럽게 움직일 수 있습니다. 퇴행성 관절염은 뼈끝을 감싼 연골이 손상되거나 닳아 염증과 통증이 발생하는 질환입니다. 퇴행성 관절염의 일차적인 증상은 통증이며 관절 마디가 부어올라 뻣뻣하고 두꺼워질 수 있습니다. 특히 손가락, 무릎에 흔하게 나타납니다.",
        "reference": "<sup>21</sup> 윤장원, (2016). 소방공무원의 근골격계 질환: 2011 년~ 2013 년 공상신청 분석. 한국화재소방학회 논문지, 133-7.<br><sup>22</sup> National Institute for Occupational Safety and Health (NOISH), U.S. Department of Health and Human Services, Elements of ergonomics programs: A primer based on workplace evaluations of Musculoskeletal Disorders, (1997). DHHS (NOISH) Publication, 97-117.<br><sup>23</sup> 요통. 질병관리본부 국가건강정보포털, (2020년 7월 8일). URL: <a href=\"http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=2350\">http://health.cdc.go.kr/health/HealthInfoArea/HealthInfo/View.do?idx=2350&quot;</a><br><sup>24</sup> 국민안전처, (2016). 소방공무원 직무관련성 근골격계질환 발생기전 규명을 통한 공상 등 관리대책 수립연구 : 요추질환을 중심으로.<br><sup>25</sup> Felson DT et al., (2000). Osteoarthritis: new insights. Part 1: the disease and its risk factors. Annals of internal medicine, 635-46.",
        "img_top": "part1_2-7.png",
        "img_top_size": "mid"
      }
    ],
    "2-1-4" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "물리적 유해인자 : 위험한 작업 현장",
        "subtitle_text" : "위험한 작업 현장",
        "content": "소방공무원은 벽, 천장, 바닥이 붕괴될 위험이 있는 상황에서 구조·구급·화재진압 작업을 수행하며, 가스, 유류, 화공약품, 분진에 의한 폭발 등 예측 불가능한 상황이 잠재되어 있습니다<sup>26</sup>.<br>작업하는 건물이 고층이고 구조가 복잡할 경우 추락, 낙상의 위험도 커질 수 있습니다<sup>27</sup>. 또한 구조 작업 과정에서 사다리, 로프를 사용 시 실족, 추락, 낙석 등의 위험이 있을 수 있습니다.",
        "reference": "<sup>26</sup> 김규상, (2010). 소방공무원의 노출 위험과 건강영향. Hanyang Medical Reviews, 296-304.<br><sup>27</sup> 국가인권위원회, (2015). 소방공무원의 인권상황 실태조사.",
        "img_top": "part1_2-8.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "위험한 작업 현장이 신체에 미치는 영향",
        "content": "소방공무원들은 위험하고 위급한 상황 속에서 구조, 구급, 화재진압 작업을 수행하게 됩니다. 작업 도중에 벽, 천장, 바닥이 붕괴되거나 사다리, 로프를 사용하는 경우에는 실족하거나 추락하는 위험이 있을 수 있습니다. 또한 화재현장은 가스, 유류, 화공약품 등에 의한 폭발 등의 위험이 혼재되어 있습니다.<br>따라서 미끄러짐, 넘어짐, 부딪힘(충돌), 물체에 맞음(낙하, 비래), 추락, 깔림(전도), 절단, 끼임(협착), 감전, 폭발, 터짐(파열) 등의 사고가 발생할 수 있으며 그로 인한 크고 작은 부상을 입을 수 있습니다<sup>28</sup>.<br><br>소방공무원의 현장활동은 예고 없이 갑작스럽게 이루어지며, 많은 체력이 소모되는 일입니다.  시간이 경과할수록 정신적·육체적 피로가 가중되어 주의력, 사고력 감퇴가 나타날 수 있으며, 그로 인해 인적 사고의 위험성이 증가할 수 있습니다<sup>29</sup>.<br>특히 화재현장은 짙은 연기와 열기로 인해 시야 확보가 어려워 부상의 위험이 커질 수 있습니다. 작업 현장에서 발생할 수 있는 부상으로는 타박상, 열상, 찰과상, 급성 요통, 탈구, 골절 등이 있습니다. <br><br>특히 머리 부위에 직∙간접적인 충격이 가해질 경우 외상성 뇌손상이 발생할 수 있습니다. 외상성 뇌손상은 외상 후 의식 소실 기간, 기억상실 기간, 초기의 신경학적 이상 정도에 따라 경도, 중등도, 고도 외상성 뇌손상으로 분류됩니다.<br>그 중 경도 외상성 뇌손상(mild traumatic brain injury, mTBI)이 가장 흔하며, 이는 머리에 가해진 타격이 상대적으로 경미하고 뇌 조직의 손상은 적지만 여러 후유증이 발생할 수 있어 주의가 필요합니다.<br>뇌진탕 후 증후군(post-concussion syndrome)은 이러한 경도 외상성 뇌손상 이후에 발생할 수 있는 증상의 일환으로<sup>30</sup>, 두통, 어지럼증, 메스꺼움 등의 증상이 지속될 수 있습니다.<br>또한 외상성 뇌손상 후 인지기능 저하, 불안/초조, 구역/구토 증상 등을 겪기도 하며, 손상된 뇌 영역에 따라 사고력, 감정 조절 능력, 운동 능력 등이 저하될 수 있습니다.",
        "reference": "<sup>28</sup> National Fire Protection Association Fire Analysis and Research Division, (2012). PATTERNS OF FIREFIGHTER FIREGROUND INJURIES.<br><sup>29</sup> 서울소방재난본부, (2019). 2019년 신임교육과정 소방학교 소방전술Ⅰ(화재 2).<br><sup>30</sup> Bazarian JJ et al., (1999). Epidemiology and predictors of post-concussive syndrome after minor head injury in an emergency population. Brain injury, 173-89.",
        "img_top": "part1_2-9.png",
        "img_top_size": "mid",
      }
    ],
    "2-2-1" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "화학적 유해인자 : 유해가스",
        "subtitle_text" : "유해가스",
        "content": "화재현장에서는 나무, 섬유, 플라스틱 등 물질의 연소에 따라 일산화탄소, 시안화수소, 황화수소, 이산화탄소 등의 질식성 유해가스와 염화수소, 암모니아 등의 자극성 유해가스가 발생할 수 있습니다<sup>31,32</sup>.<br>예를 들어 시안화수소는 일상생활 제품(폼 단열재, 가전제품, 플라스틱 제품, 카펫, 의류 및 합성 물질)에 많이 사용되는 나일론, 폴리우레탄 등이 연소되면서 발생하며<sup>33</sup>, 염화수소는 폴리염화비닐(Polyvinyl Chloride, PVC), 아크릴과 같은 염소를 함유한 플라스틱의 연소과정에서 발생할 수 있습니다<sup>34</sup>.<br>또한 새로운 화학물질의 개발로 인하여 위험이 확장되고 있어 유해가스 노출에 특히 주의해야 합니다<sup>35</sup>.<br>뿐만 아니라, 현장에서 복귀 후 소방 차량 및 장비 등을 통해 유해가스에 2차적으로 노출되는 경우도 있어 이에 대한 연구가 진행되고 있습니다<sup>36</sup>.",
        "reference": "<sup>31</sup> 김정만 외, (2018). 소방관들이 화재현장에서 노출되는 유해물질들-물리적, 화학적 및 생물학적 요인. J Korean Med Assoc, 1072-7.<br><sup>32</sup> 서울소방재난본부, (2019). 2019년 신임교육과정 소방학교 소방전술Ⅰ(화재 3).<br><sup>33</sup> 안전보건공단, (2013). 유해위험물질 취급관리(14) [화재·폭발·누출예방]시안화수소.<br><sup>34</sup> Dyer RF, (1976). Polyvinyl chloride toxicity in fires: hydrogen chloride toxicity in fire fighters. JAMA, 393-7.<br><sup>35</sup> 국가인권위원회, (2015). 소방공무원의 인권상황 실태조사.<br><sup>36</sup> 김수진 외, (2019). 소방서 실내공간의 화학적 유해인자 2 차노출과 실내공기질 특성. 한국화재소방학회논문지, 140-51.",
        "img_top": "part1_2-10.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "유해가스가 신체에 미치는 영향",
        "content": "유해가스가 신체에 미치는 영향은 화재 연기의 성분, 유해가스의 물리적 특성, 노출 강도 및 기간, 기존 건강 상태에 따라 다양하게 나타날 수 있습니다.<br><br>그 중 일산화탄소는 대표적인 질식성 유해가스로 무색, 무취, 무미이며, 호흡을 통해 신체로 들어올 수 있습니다 . 흡수된 일산화탄소의 80~90%는 혈액 내 적혈구의 헤모글로빈과 결합하여 혈액이 산소를 운반하는 능력을 저하시킵니다<sup>38</sup>.<br>헤모글로빈은 폐로부터 들어온 산소를 신체 곳곳으로 운반하는 역할을 수행하는 단백질입니다. 일산화탄소는 산소에 비하여 헤모글로빈과 결합하는 힘이 200배 가량 강하기 때문에 적은 양의 일산화탄소에 노출되더라도 헤모글로빈의 산소 운반 능력이 저하되고 신체 조직에 산소가 부족하게 됩니다.<br>특히 뇌에 산소 공급이 잘 되지 않아 단기간 일산화탄소에 노출되더라도 두통, 방향감각 상실 등 증상이 나타날 수 있습니다<sup>39</sup>.<br><br>화재에서 생성되는 자극성 가스들은 호흡기에 손상을 일으킬 수 있습니다. 염화수소는 폐 손상을 유발하며<sup>40</sup> 눈, 코 등의 점막을 자극할 수 있어<sup>41</sup>, 저농도에 만성적으로 노출되면 만성 기관지염 등이 발생할 수 있습니다<sup>42</sup>.<br><br>화재 연소 시 산소가 소모되어 공기 중 산소의 양이 적어지게 되는데 이러한 환경은 기존 심폐 질환이나 뇌혈관 질환이 있어 취약할 수 있는 사람들에게 저산소성 손상을 초래할 수 있습니다<sup>43</sup>.",
        "reference": "<sup>37</sup> Alarie Y, (2002). Toxicity of fire smoke. Crit Rev Toxicol, 259-89.<br><sup>38</sup> Piantadosi CA, (2002). Biological chemistry of carbon monoxide. Antioxid Redox Signal, 259-70.<br><sup>39</sup> Prockop LD et al., (2007). Carbon monoxide intoxication: an updated review. J Neurol Sci, 122-30.<br><sup>40</sup> Medical management guidelines for hydrogen chloride (HCl) Agency for Toxic Substances and Disease Registry. (2020년 7월 6일). URL: <a href=\"https://www.atsdr.cdc.gov/MMG/MMG.asp?id=758&tid=147\">https://www.atsdr.cdc.gov/MMG/MMG.asp?id=758&tid=147</a><br><sup>41</sup> Medical management guidelines for hydrogen chloride (HCl) Agency for Toxic Substances and Disease Registry. (2020년 7월 6일). URL: <a href=\"https://www.atsdr.cdc.gov/MMG/MMG.asp?id=758&tid=147\">https://www.atsdr.cdc.gov/MMG/MMG.asp?id=758&tid=147</a><br><sup>42</sup> 김정만 외, (2018). 소방관들이 화재현장에서 노출되는 유해물질들-물리적, 화학적 및 생물학적 요인. J Korean Med Assoc, 1072-7.<br><sup>43</sup> Trunkey D, (1978). Inhalation injury. Surgical Clinics of North America, 1133-40.",
        "img_top": "part1_2-11.png",
        "img_top_size": "mid"
      }
    ],
    "2-2-2" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "화학적 유해인자 : 석면 등 분진",
        "subtitle_text" : "석면 등 분진",
        "content": "석면은 세계보건기구(World Health Organization, WHO) 산하 국제암연구소(International Agency for Research on Cancer, IARC) 지정 1급 발암물질<sup>44</sup>로, 최근에는 건축물에 사용되지 않습니다.<br>그러나 과거에 지어진 건물에서 단열재 등으로 사용되었기 때문에 이러한 건물들이 화재 및 재난 현장에서 파괴되면 비산 먼지에 석면이 유출될 수 있습니다.<br><br>2010년에 진행된 소방활동 중 석면의 유해성 인식도 조사 결과에 따르면 소방공무원의 64.3%가 석면의 유해성을 인식하지 못하는 것으로 나타났습니다<sup>45</sup>.<br>피부나 호흡기가 석면 등 분진 가루에 노출되는 것을 최소화하기 위해서는 공기호흡기, 방진 마스크와 같은 보호장비를 착용해야 합니다<sup>46</sup>. ",
        "reference": "<sup>44</sup> International Agency for Research on Cancer. IARC Monographs on the Evaluation of Carcinogenic Risks to Humans. Volume 14: Asbestos. 1977. (2020년 7월 1일). URL: <a href='https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Asbestos-1977'>https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Asbestos-1977</a><br><sup>45</sup> 이정일, (2010). 소방공무원들의 현장 활동 시 석면노출의 위험성과 대응방안. 한국화재소방학회 논문지, 68-78.<br><sup>46</sup> 서울소방재난본부, (2019). 2019년 신임교육과정 소방학교 소방전술Ⅰ(화재 2).",
        "img_top": "part1_2-12.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "석면 등 분진이 신체에 미치는 영향",
        "content": "석면은 세계보건기구(World Health Organization, WHO) 산하 국제암연구소(International Agency for Research on Cancer, IARC) 지정 1급 발암물질로, 현재 석면 함유 제품의 제조, 수입, 양도, 제공, 사용이 일체 금지되었습니다. 그러나 예전의 설비 및 건축물에는 석면이 사용되었을 수 있으므로 소방 현장에서 노출되지 않도록 주의가 필요합니다.<br><br>석면은 작게 부서져 분진을 통해 폐로 흡입되고 폐 손상을 일으킬 수 있습니다<sup>47</sup>. 장기적으로 노출되었을 경우 10~40년의 잠복기를 거쳐 석면폐증, 폐암, 중피종 등 여러 폐질환이 발생할 수 있습니다<sup>48</sup>.<br>석면 분진은 개인보호장비에 흡착될 수 있으므로 작업을 마친 후 지침에 따라 깨끗하게 제거하여 2차 노출에 따른 피해를 줄여야 합니다.<br><br>석면 분진 외에도 돌·모래 가루 등의 광물성 분진, 용접 중 발생되는 아주 작은 입자인 용접 흄(Fume), 곡물가루 등 여러 종류의 분진에 노출될 수 있습니다<sup>49</sup>. 특히 건물 붕괴 시에는 엄청난 양의 분진이 발생하게 되는데, 눈에 보이는 분진 외에도 눈에 보이지 않는 미세한 분진들이 대량으로 발생합니다<sup>50</sup>. 이러한 미세분진은 호흡기를 통해 신체 내부로 유입되어 일부는 체내에 축적되어 기관지와 폐를 포함한 호흡기 질환을 일으킬 수 있습니다.<br><br>*흄(Fume): 승화, 증류, 화학반응 등에 의해 발생하는 연기로, 급속히 응축되어 생성된 고체 상태의 미립자",
        "reference": "<sup>47</sup> 이정일, (2010). 소방공무원들의 현장 활동 시 석면노출의 위험성과 대응방안. 한국화재소방학회 논문지, 68-78.<br><sup>48</sup> 안전보건공단, (2012). 유해화학물질 바로알기.<br><sup>49</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>50</sup> 이정일, (2010). 소방공무원들의 현장 활동 시 석면노출의 위험성과 대응방안. 한국화재소방학회 논문지, 68-78.",
        "img_top": "part1_2-11.png",
        "img_top_size": "mid"
      }
    ],
    "2-2-3" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "화학적 유해인자 : 중금속",
        "subtitle_text" : "중금속",
        "content": "화재진압 작업과 잔화정리 작업 과정에서 소방공무원들은 화재 연소 부산물에 포함된 중금속에 노출될 수 있습니다<sup>51</sup>.<br>실제 화재현장에서 발생한 연기를 수집하여 분석한 결과, 연기 미립자에서 크롬, 카드뮴, 납, 인산염, 비소, 코발트 등의 중금속이 검출된 바 있습니다<sup>52,53</sup>.<br>특히 실내용 바닥재, 마감재, 전자제품 등이 타면서 다양한 종류의 중금속이 방출될 수 있습니다.<br>개인보호장비에 크롬, 아연, 구리 등이 흡착되는 경우 2차 노출의 위험이 있습니다<sup>54</sup>.",
        "reference": "<sup>51</sup> Bolstad-Johnson DM et al., (2000). Characterization of firefighter exposures during fire overhaul. Am Ind Hyg Assoc J, 636-41.<br><sup>52</sup> Underwriters Laboratories, (2010). Firefighter exposure to smoke particulates.<br><sup>53</sup> Bolstad-Johnson DM et al., (2000). Characterization of firefighter exposures during fire overhaul. Am Ind Hyg Assoc J, 636-41.<br><sup>54</sup> 국립소방연구원 소방정책연구실, (2019). 개인보호장비 세척관리 시스템 개발.",
        "img_top": "part1_2-13.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "중금속이 신체에 미치는 영향",
        "content": "화재 시 흔히 발생할 수 있는 중금속은 납, 크롬, 카드뮴, 비소<sup>55</sup> 등이 있습니다.<br>화재, 구급, 구조 현장에서 중금속은 먼지 형태로 코를 통하여 흡입되거나, 직접 접촉의 방식으로 피부를 통해 침투됩니다.<br>인체에 흡수된 중금속은 혈액을 통해 체내 대부분의 장기로 이동될 수 있으며, 눈, 코 및 피부 등 수분이 많은 기관과 폐, 간 및 신장 등 중금속의 이동 경로 및 대사 기관이 더 영향을 받을 수 있습니다. 인체 내로 들어온 중금속은 쉽게 분해되지 않아 체내에 축적될 위험이 있기 때문에 주의가 필요합니다.<br><br>납에 만성적으로 노출된 경우 헤모글로빈 합성장애와 적혈구 수명 단축으로 인한 빈혈증상과 망상 적혈구 증가 등의 혈액학적 증상, 식욕부진, 소화불량, 납산통(lead colic) 등의 소화기계 증상, 손저림 등의 신경근육계 증상, 두통, 어지러움, 기억력과 집중력 저하 등의 중추신경계 증상 및 고혈압, 신장질환 등의 질환이 나타날 수 있습니다<sup>56,57</sup>.<br>크롬과 카드뮴은 주로 분진과 흄(Fume)의 형태로 호흡기를 통해 흡입될 수 있습니다<sup>58</sup>. 크롬은 일반적으로 조직 내에서 축적되지는 않으나 장기간 반복적으로 노출될 경우 만성기관지염, 폐질환 등이 생길 수 있는 것으로 알려져 있습니다. 카드뮴에 급성 노출 시 발열, 오한, 무력감, 호흡곤란, 구역/구토, 설사 등의 증상이 나타날 수 있으며, 만성적으로 노출되는 경우 폐질환과 간기능 장애, 치아의 황색 착색, 후각장애 등이 나타날 수 있습니다.<br>여러 중금속 중 특히 납과 카드뮴은 소방공무원 정기건강검진 대상 유해인자로 분류되어 관리되고 있습니다<sup>59</sup>. 비소는 흡입을 통해 인체 내에 흡수되며, 적혈구와 결합하여 간, 신장, 근육, 뼈, 모발, 피부, 손발톱 등에 축적될 수 있습니다.<br><br>*흄(Fume): 승화, 증류, 화학반응 등에 의해 발생하는 연기로, 급속히 응축되어 생성된 고체 상태의 미립자",
        "reference": "<sup>55</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>56</sup> Heavy metal pollution and human biotoxic effects. International Journal of physical sciences, 112-8.<br><sup>57</sup> Järup L, (2003). Hazards of heavy metal contamination. British medical bulletin, 167-82.<br><sup>58</sup> 안전보건공단, (2012). 유해화학물질 바로알기.<br><sup>59</sup> 소방공무원 보건안전관리 규정 [시행 2020. 6. 11.] [소방청훈령 제164호, 2020. 6. 11., 전부개정] <a href=\"http://www.law.go.kr/행정규칙/소방공무원보건안전관리규정/(164,20200611)\">http://www.law.go.kr/행정규칙/소방공무원보건안전관리규정/(164,20200611)</a>",
        "img_top": "part1_2-14.png",
        "img_top_size": "mid"
      }
    ],
    "2-2-4" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "화학적 유해인자 : 벤젠 등 유기화합물",
        "subtitle_text" : "벤젠 등 유기화합물",
        "content": "소방공무원은 화재 시 발생하는 연기와 건물 내 잔존물에 함유된 벤젠과 다환방향족탄화수소(polycyclic aromatic hydrocarbon, PAH), 아크롤레인, 포름알데히드 등 유기화합물에노출될 수 있습니다<sup>60,61</sup>.<br>특히 벤젠은 세계보건기구(World Health Organization, WHO) 산하 국제암연구소(International Agency for Research on Cancer, IARC) 지정 1급 발암물질에 해당합니다<sup>62</sup>.이러한 화학적 유해인자가 피부나 호흡기를 통해 노출되는 것을 최소화하기 위해서는 공기호흡기, 방진 마스크와 같은 보호장비를 착용해야 합니다.",
        "reference": "<sup>60</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>61</sup> Fent KW, et al., (2014). Systemic exposure to PAHs and benzene in firefighters suppressing controlled structure fires. Ann Occup Hyg, 830-45.<br><sup>62</sup> International Agency for Research on Cancer. IARC Monographs on the Evaluation of Carcinogenic Risks to Humans. Volume 120: Benzene. 2018. (2020년 7월 1일). URL: <a href='https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Benzene-2018'>https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Benzene-2018</a>",
        "img_top": "part1_2-15.png",
        "img_top_size": "small"
      }, {
        "subtitle_text" : "벤젠 등 유기화합물이 신체에 미치는 영향",
        "content": "벤젠은 세계보건기구(World Health Organization, WHO) 산하 국제암연구소(International Agency for Research on Cancer, IARC) 지정 1급 발암물질에 해당합니다<sup>63</sup>.<br>벤젠은 무색이며 독특한 냄새(방향성)를 가진 물질로, 농약, 페인트, 도료, 합성수지, 계면활성제, 각종 석유화학공업의 원료로 사용됩니다<sup>64</sup>. 인화점이 낮아 정전기 스파크와 같은 아주 작은 점화원에 의해서도 불이 붙을 수 있으며, 거의 모든 화재현장에서 검출됩니다<sup>65</sup>.<br><br>벤젠은 공기 중으로 확산되어 주로 호흡기와 피부를 통해 체내로 흡수될 수 있습니다<sup>66</sup>. 증기 형태로 흡입한 경우 현기증이 나거나 질식할 수 있으며, 접촉 시 피부와 눈을 자극하거나 화상을 입힐 수 있습니다<sup>67</sup>.<br>짧은 시간 동안 노출될 경우에는 두통, 구역, 어지럼증, 의식변화, 혼수 증상이 나타날 수 있으며, 반복적으로 노출될 경우 두통, 식욕부진, 복통 등의 증상과 함께 빈혈, 백혈구감소증, 혈소판감소증, 백혈병 등의 조혈계 관련 질병이 발생할 수 있습니다. 그러므로 벤젠은 소방공무원 특수건강검진 대상 유해인자로 분류되어 관리되고 있습니다<sup>68</sup>.<br><br>화재현장에서 벤젠 외에도 다양한 유기화합물에 노출될 수 있습니다.<br>그 중 포름알데히드와 아크롤레인은 폴리에틸렌 및 셀룰로스가 타면서 발생하는 매연에 다수 포함되어 있습니다<sup>69</sup>. 폴리에틸렌은 병·포장재·전기절연체 등에 많이 사용되며, 셀룰로스는 목재, 종이, 면섬유 등에 포함됩니다.<br>포름알데히드는 호흡기 자극제로 호흡기를 통해 흡수될 경우 호흡곤란, 천식, 두통 등의 증상이 발생할 수 있고, 안구, 점막 및 피부를 자극하여 눈물, 알레르기 반응, 발진 등의 증상이 나타날 수 있습니다<sup>70</sup>.",
        "reference": "<sup>63</sup> International Agency for Research on Cancer. IARC Monographs on the Evaluation of Carcinogenic Risks to Humans. Volume 120: Benzene. 2018. (2020년 7월 1일). URL: <a href='https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Benzene-2018'>https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Benzene-2018</a><br><sup>64</sup> 서울소방재난본부, (2019). 2019년 신임교육과정 소방학교 소방전술Ⅰ(화재 2).<br><sup>65</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>66</sup> 안전보건공단, (2012). 유해화학물질 바로알기.<br><sup>67</sup> 안전보건공단 화학물질정보: 벤젠, (2020년 7월 9일). URL: <a href='http://msds.kosha.or.kr'>http://msds.kosha.or.kr</a><br><sup>68</sup> 소방공무원 보건안전관리 규정 [시행 2020. 6. 11.] [소방청훈령 제164호, 2020. 6. 11., 전부개정] <a href='http://www.law.go.kr/행정규칙/소방공무원보건안전관리규정/(164,20200611)'>http://www.law.go.kr/행정규칙/소방공무원보건안전관리규정/(164,20200611)</a><br><sup>69</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>70</sup> 화학물질안전원 화학물질정보시스템 물질정보: 포름알데히드, (2020년 7월 15일). URL: <a href='https://icis.me.go.kr/chmClsCl/chmClsClView.do?hlhsn_sn=159'>https://icis.me.go.kr/chmClsCl/chmClsClView.do?hlhsn_sn=159</a>",
        "img_top": "part1_2-16.png",
        "img_top_size": "mid"
      }
    ],
    "2-3-1" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "생물학적 유해인자 : 감염인자",
        "subtitle_text" : "감염인자",
        "content": "긴급한 구조, 구급 업무의 특성상 환자의 상태를 현장에서 모두 파악하기에는 어려움이 있으며, 인체 분비물에 의한 접촉 감염과 구급장비를 통한 교차 감염에 노출될 수 있습니다<sup>71</sup>.<br>예를 들어 피부손상, 주사침 손상 등으로 인해 B형 간염, C형 간염에 전염될 수 있으며, 공기를 통하여 결핵 등 호흡기 감염병에 노출될 수 있습니다<sup>72</sup>.",
        "reference": "<sup>71</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>72</sup> 박찬석, (2014). 재난 대응 활동 시 노출가능 유해인자 종합분석-화재 현장을 중심으로. 한국재난정보학회 논문집, 420-30.",
        "img_top": "part1_2-17.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "감염인자가 신체에 미치는 영향",
        "content": "소방공무원은 구조, 구급, 화재진압 등 여러 현장에서 감염인자에 노출될 수 있습니다.<br>혈액을 통해 전염될 수 있는 간염이나 비말을 통해 전염될 수 있는 각종 호흡기 감염, 결핵 등에 노출될 수 있어 주의가 필요합니다. 환자의 혈액, 침에 직접적으로 접촉하여 감염이 발생하거나 구급장비 등을 통해 간접적으로 감염인자에 노출될 가능성이 있습니다<sup>73</sup>.<br><br>‘소방공무원 보건안전관리 규정’과 ‘119구급대원 감염관리 표준지침’에서는 현장소방업무를 수행하는 소방공무원은 현장의 특성에 맞는 장갑, 마스크, 보호안경, 보호복 등을 착용하여 감염에 대비하고, 소방관서 차원에서도 현장 소방 활동의 과정에서 감염인자에 노출될 수 있음을 고려하여 감염관리실, 전용세탁실을 설치하고 운영해야 함을 규정하고 있습니다<sup>74,75</sup>.<br>또한 구급차 탑승요원의 경우 B형 간염, 인플루엔자 등에 대해서 예방접종을 해야하고, 결핵 검사를 매해 실시하도록 권고하고 있습니다. 또한 매년 1회 이상 감염방지 보수교육을 시행하여 구급대원의 건강상태가 잘 관리될 수 있도록 합니다.<br><br>미국 질병통제예방센터(Centers for disease control and prevention, CDC)에서는 소방공무원, 긴급구조대원과 같은 직업군을 위한 코로나바이러스감염증-19 환자 대응 지침을 제공한 바 있습니다. 이렇게 소방공무원이 감염인자 노출로 인해 겪을 수 있는 문제를 예방하기 위하여 국내외 여러 지침에서 공통적으로 개인위생 수칙 준수, 개인보호장구(personal protective equipment, PPE)의 착용, 장비, 구급차의 소독 등을 권장하고 있습니다.",
        "reference": "<sup>73</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>74</sup> 소방청(소방정책과). 소방공무원 보건안전관리 규정 [시행 2015. 1. 6.] [국민안전처훈령 제1호, 2015. 1. 6., 타법개정]<br><sup>75</sup> 국민안전처 119구급과-7001(2015.11.05.)호. 119구급대원 감염관리 표준지침 개정 시달.",
        "img_top": "part1_2-18.png",
        "img_top_size": "mid"
      }
    ],
    "2-4-1" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "정신적 유해인자 : 교대근무로 인한 불규칙한 수면",
        "subtitle_text" : "교대근무로 인한 불규칙한 수면",
        "content": "2015년 소방공무원 전수조사에 따르면 소방공무원의 약 85%가 교대근무를 수행하고 있습니다<sup>76</sup>.<br>야간 교대근무는 세계보건기구(World Health Organization, WHO) 산하 국제암연구소(International Agency for Research on Cancer, IARC)가 지정한 발암물질(2A군)에 해당합니다<sup>77</sup>.<br>교대근무에 따른 활동과 수면 주기의 변화는 기존 생체의 일주기 리듬에 영향을 주게 되며, 이는 신체의 기능 및 활동에 영향을 미칠 수 있습니다<sup>78</sup>.",
        "reference": "<sup>76</sup> 국가인권위원회, (2015). 소방공무원의 인권상황 실태조사.<br><sup>77</sup> International Agency for Research on Cancer. IARC Monographs on the Evaluation of Carcinogenic Risks to Humans. Volume 124: Night Shift Work. 2020. (2020년 7월 1일). URL: <a href='https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Night-Shift-Work-2020'>https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Night-Shift-Work-2020</a><br><sup>78</sup> 박찬석, (2014). 재난 대응 활동 시 노출가능 유해인자 종합분석-화재 현장을 중심으로. 한국재난정보학회 논문집, 420-30.",
        "img_top": "part1_2-19.png",
        "img_top_size": "small"
      }, {
        "subtitle_text" : "교대근무로 인한 불규칙한 수면이 신체에 미치는 영향",
        "content": "일주기 리듬(circadian rhythm)은 몸의 생체시계에 의해 신체의 수면-각성이 약 하루의 주기로 일어나는 현상으로, 각종 호르몬과 생리적 활동들도 이 주기에 따라 일어납니다<sup>79</sup>.<br>일주기 리듬에 가장 큰 영향을 주는 것은 빛입니다. 그렇기 때문에 교대근무, 야간근무 등은 일주기 리듬을 변화시켜 수면의 질을 떨어뜨리는 등 수면 문제를 일으킬 수 있습니다.<br>일주기 리듬이 교란되어 수면 문제가 있을 경우 근무 중 사고의 위험이 증가하고 생산성이 감소하며, 우울 및 불안 증상도 동반될 수 있습니다<sup>80</sup>.<br><br>교대근무로 인해 일주기 리듬이 교란될 경우 수면장애 외에도 시상하부-뇌하수체-부신피질 축의 이상으로 인해 다양한 신체 질환이 발생할 수 있습니다.<br>또한 시상하부-뇌하수체-부신피질 축을 통해 코티솔(cortisol)과 같은 스트레스 호르몬이 증가하게 되고, 교감신경계가 과도하게 활성화됩니다<sup>81</sup>. 이에 따라 혈압 상승이 일어나고, 고혈압, 심근경색과 같은 심혈관 질환의 발병 가능성이 높아집니다<sup>82</sup>.<br><br>그 외에도 교대근무로 인한 수면 시간 저하, 불규칙한 식이 습관, 흡연, 음주 등은 인슐린 저항성이 증가시키고 대사능력이 떨어뜨려 당뇨병, 고지혈증과 같은 대사 질환의 발병 위험도 증가할 수 있습니다.<br><br>그렇기 때문에 2007년 세계보건기구(World health organization, WHO) 산하 국제암연구소(International agency for research on cancer, IARC)는 일주기 리듬 교란이 심각할 경우 발암으로 이어질 수 있는 측면에서 인체발암추정인자(그룹 2A) 중 하나로 교대근무를 포함하였습니다<sup>83</sup>.<br><br>교대근무와 관련된 신체 질환의 발병 양상은 나이, 성별, 신체적 상태, 심리적 상태에 따라 달라질 수 있으므로 소방공무원 내에서도 개인차가 있습니다. 금연, 금주와 같은 생활 습관을 개선하거나 물론 근무 시간대 조정 등 교대근무를 개선할 경우 교대근무와 관련이 있는 여러 질환의 발병을 사전에 예방할 수 있을 것으로 기대됩니다<sup>84</sup>.",
        "reference": "<sup>79</sup> Sehgal A et al., (2011). Genetics of sleep and sleep disorders. Cell, 194-207.<br><sup>80</sup> Knutsson A, (2003). Health disorders of shift workers. Occup Med, 103-8.<br><sup>81</sup> Haus E et al., (2006). Biological clocks and shift work: circadian dysregulation and potential long-term effects. Cancer Causes Control, 489-500.<br><sup>82</sup> Fialho G et al., (2006). Effects of 24-h shift work in the emergency room on ambulatory blood pressure monitoring values of medical residents. Am J Hypertens, 1005-9.<br><sup>83</sup> International Agency for Research on Cancer. Night Shift Work: IARC Monographs on the Identification of Carcinogenic Hazards to Humans. (cited 29 June 2020). URL: <a href='https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Night-Shift-Work-2020'>https://publications.iarc.fr/Book-And-Report-Series/Iarc-Monographs-On-The-Identification-Of-Carcinogenic-Hazards-To-Humans/Night-Shift-Work-2020</a><br><sup>84</sup> Härmä M, (1993). Individual differences in tolerance to shiftwork: a review. Ergonomics, 101-9.<br>",
        "img_top": "part1_2-20.png",
        "img_top_size": "full"
      }
    ],
    "2-4-2" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "정신적 유해인자 : 비상대기 및 긴급 출동",
        "subtitle_text" : "비상대기 및 긴급 출동",
        "content": "소방공무원은 각종 위험 상황에 긴급 투입되는 등 정신적인 긴장과 부담이 큰 직업군에 속합니다<sup>85,86</sup>.<br>현장에서 구조, 구급, 화재진압 업무를 수행하는 시간 이외에도 대부분의 근무시간 동안 긴급출동 상황을 대비하여 긴장 태세를 갖추고 있습니다<sup>87</sup>. 이로 인해 정신적인 긴장과 스트레스가 높아질 수 있습니다.",
        "reference": "<sup>85</sup> 산업안전보건연구원, (2008). 특수 직종 및 업종 근로자 건강관리 실태 및 건강영향 평가 I- 소방공무원을 중심으로.<br><sup>86</sup> 문유석, (2010). 소방공무원의 스트레스 수준과 직무환경적 유발요인. 지방정부연구, 119-41.<br><sup>87</sup> 김규상, (2010). 소방공무원의 노출 위험과 건강영향. Hanyang Medical Reviews, 296-304.",
        "img_top": "part1_2-21.png",
        "img_top_size": "small"
      }, {
        "subtitle_text" : "비상대기 및 긴급 출동이 신체에 미치는 영향",
        "content": "긴급 출동벨이 울리면 소방공무원의 심박수는 급속히 증가합니다<sup>88</sup>. 급작스러운 스트레스는 심박수의 급증을 유도할 뿐 아니라 뇌의 변화를 일으키고 신체를 자극하여 몸 속 모든 세포가 긴장하도록 합니다.<br><br>비상 상황에 직면했을 때 코티솔(cortisol)과 같은 스트레스 호르몬이 증가하면 시상하부-뇌하수체-부신피질축 및 교감신경계가 급격하게 활성화됩니다<sup>89</sup>. 동시에 염증 및 면역 반응에 관여하는 여러 염증 물질들도 증가하고 혈압, 심박수 등 대사 양상이 변화하게 됩니다.<br>또한 스트레스 호르몬은 산화 스트레스(oxidative stress)를 증가시키게 되는 등 복합적인 작용을 통해 다양한 질환들의 발병 가능성을 높일 수 있습니다<sup>90</sup>.<br>비상대기 및 긴급출동은 이러한 정신적인 요인 외에도 갑작스럽게 신체 활동량을 늘리는 식으로 물리적 유해 요인으로 작용할 수 있으며 근골격계 질환의 발병 가능성을 높일 수 있습니다<sup>91</sup>.<br><br>스트레스를 받은 이후 초기에 나타날 수 있는 우울, 불안 증상은 대체적으로 일시적이고 스트레스 유발 요인이 사라짐에 따라 없어집니다.<br>그러나 소방공무원은 스트레스 유발 요인들에 장기간 반복적으로 노출되기 때문에 불안장애, 우울장애, 알코올 및 물질사용장애와 같이 만성적인 스트레스로 인해 흔히 생길 수 있는 정신 건강 문제에 취약할 수 있습니다<sup>92</sup>.<br><br>나아가 심혈관계 질환(고혈압, 급성심근경색 등), 대사 질환(당뇨병, 이상지질혈증 등), 위장관계 질환(과민성 대장증후군, 역류성 식도염 등)의 발병 가능성 또한 증가하는 것으로 보고된 바 있습니다<sup>93</sup>.<br><br>즉, 소방공무원들이 직업 상 접하는 만성적 스트레스 요인들은 신체가 다양한 질환에 취약하게 만들 수 있으므로, 스트레스를 극복하고 심리적 안정을 취할 수 있는 방법을 모색하는 개인적, 사회적 노력이 필요합니다.",
        "reference": "<sup>88</sup> Barnard RJ et al., (1975). Heart rate and ECG responses of fire fighters. J Occup Environ Med, 247-50.<br><sup>89</sup> Haus E et al., (2006). Biological clocks and shift work: circadian dysregulation and potential long-term effects. Cancer Causes Control, 489-500.   (비고: #3과 동일 reference)<br><sup>90</sup> Stentz FB et al., (2004). Proinflammatory cytokines, markers of cardiovascular risks, oxidative stress, and lipid peroxidation in patients with hyperglycemic crises. Diabetes, 2079-86.<br><sup>91</sup> Soteriades ES et al., (2019). Occupational stress and musculoskeletal symptoms in firefighters. Int J Occup Med Environ Health, 341-52.<br><sup>92</sup> Murphy SA et al., (1999). Occupational stressors, stress responses, and alcohol consumption among professional firefighters: a prospective, longitudinal analysis. Int J Stress Manag, 179-96.<br><sup>93</sup> Guidotti TL, (1992). Human factors in firefighting: ergonomic-, cardiopulmonary-, and psychogenic stress-related issues. Int Arch Occ Env Hea, 1-12.",
        "img_top": "part1_2-22.png",
        "img_top_size": "mid"
      }
    ],
    "2-4-3" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "정신적 유해인자 : 반복적인 사건 노출",
        "subtitle_text" : "반복적인 사건 노출",
        "content": "소방공무원은 직무 특성 상 반복적으로 충격적인 사건을 접하게 됩니다<sup>94</sup>.<br>본인이 직접 경험하지 않더라도 타인의 고통스러운 상황을 목격하거나 가까운 사람의 충격적인 사건에 대해 듣게 될 때도 심각한 정신적 타격을 받을 수 있습니다<sup>95</sup>.<br><br>반복적으로 이러한 충격적인 사건에 노출될 경우 수면장애, 우울 장애, 외상후스트레스장애(posttraumatic stress disorder, PTSD)와 같은 정신건강 문제를 겪을 수 있습니다<sup>96</sup>.<br>외상후스트레스장애를 겪는 사람은 흔히 사건과 관련된 불쾌한 기억이 자꾸만 떠올라 고통을 받을 수 있으며, 사건을 떠올리게 만드는 것들을 피하려는 모습을 자주 보이게 됩니다.<br>또한 예민하고 각성된 상태가 지속될 수 있으며, 자신이나 타인 또는 세상에 대한 부정적인 감정을 호소하기도 합니다.<br><br>외상후스트레스장애 뿐만 아니라 우울, 불안, 인지저하, 수면문제, 문제성 음주, 폭력적 행동 등 외상후증후군이라 일컫는 각종 문제가 발생할 수 있습니다. 특히 반복적인 사건 노출로 인해 이러한 증상이 만성화되지 않도록 적절히 대처하는 것이 중요합니다.",
        "reference": "<sup>94</sup> 소방청, (2019). 소방공무원 마음건강 전수 설문조사<br><sup>95</sup> DIAGNOSTIC AND STATISTICAL MANUAL OF MENTAL DISORDERS Fifth edition, (2013). American Psychiatric Publishing, 271-280.<br><sup>96</sup> Kwon SC 외, (2008). Posttraumatic stress symptoms and related factors in firefighters of a firestation. Korean J Occup Environ Med, 193-204.",
        "img_top": "part1_2-23.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "반복적인 사건 노출이 신체에 미치는 영향",
        "content": "소방공무원은 직접적인 경험 외에도 간접적으로 외상 사건에 노출되기 쉬운 직업군입니다.<br>본인이 직접적인 위험에 노출되거나 해를 당하지 않더라도, 타인에게 발생한 상해를 목격할 때나 가까운 사람이 겪는 심각한 일에 대해 들었을 때에도 정신적인 타격을 받을 수 있습니다.<br>이렇게 직무 특성 상 예측 불가능한 사건들에 반복적으로 노출됨에 따라 소방공무원들은 불안장애, 우울장애 및 외상후스트레스장애와 같은 정신 질환에 취약해질 수 있습니다<sup>97</sup>.<br><br>외상 사건에 노출되었던 사람이 어떤 자극에 의해 그 사건을 떠올리게 될 경우 심장이 빠르게 뛰고 식은 땀이 나는 등 다양한 생리학적 반응을 경험할 수 있습니다. 이처럼 외상후스트레스장애를 겪는 소방공무원은 쉽게 과민해지며 평소라면 잘 놀라지 않을 일에도 불안함을 느끼게 되고, 자꾸만 떠오르는 충격적인 기억을 떨쳐내기 위해 많은 노력이 필요할 수 있습니다.<br>또한 가족을 포함한 주변 사람들로부터 분리되어 있다는 느낌을 받으며 심리적인 고립감을 느끼기도 합니다<sup>98</sup>.<br>외상후스트레스장애를 오랫동안 겪는다면 스트레스를 다룰 수 있는 능력이 저하되고 집중력, 기억력 저하 등 인지기능 문제가 발생할 수도 있습니다<sup>99</sup>.<br><br>외상후스트레스장애를 겪을 경우 시상하부-뇌하수체-부신피질 축 및 교감신경계는 과하게 활성화됩니다<sup>100</sup>. 그 결과 고혈압, 급성심근경색, 심장마비와 같은 심혈관계 질환의 발병 가능성이 높아지게 됩니다.<br>또한 과민한 상태가 장기간 유지될 경우 대사 질환, 위장관계 질환에도 취약해질 수 있습니다<sup>101</sup>.<br>이처럼 외상후스트레스장애를 포함한 직무 수행 관련 스트레스 증상들은 소방공무원 개인의 신체 및 정신건강을 위협할 뿐 아니라, 업무 수행 능력을 저하시킬 수도 있습니다<sup>102</sup>.<br><br>모든 소방공무원이 외상후스트레스장애 증상을 호소하는 것은 아닙니다. 또한 외상후스트레스장애를 겪게 되더라도 극복할 수 있습니다<sup>103</sup>.<br>소방공무원의 업무 특성상 외상 사건에 노출되는 빈도를 줄이기는 쉽지 않습니다. 그렇기 때문에 체계적인 관리방안 마련과 제도적인 정비를 통해 외상후스트레스장애를 조기 발견하여 적절히 개입하는 등 소방공무원들이 겪는 고통을 줄이기 위한 노력이 필요합니다.",
        "reference": "<sup>97</sup> Haslam C et al., (2003). A preliminary investigation of post-traumatic stress symptoms among firefighters. Work Stress, 277-85.<br><sup>98</sup> Blake DD et al., (1995). The development of a clinician-administered PTSD scale. J Traumatic Stress, 75-90.<br><sup>99</sup> Barrett DH et al., (1996). Cognitive functioning and posttraumatic stress disorder. Am J Psychiatry, 1492-4.<br><sup>100</sup> Lipov E, (2013). Post traumatic stress disorder (PTSD) as an over activation of sympathetic nervous system: an alternative view. J Trauma Treat, 181.<br><sup>101</sup> Pacella ML et al., (2013). The physical health consequences of PTSD and PTSD symptoms: a meta-analytic review. J Anxiety Disord, 33-46.<br><sup>102</sup> 소방방재청, (2008). 소방공무원 외상후스트레스 실태 분석 연구. <br><sup>103</sup> Kwak M et al., (2017). Posttraumatic growth and related factors in firefighters. J Korean Acad Community Health Nurs, 124-33.par",
        "img_top": "part1_2-24.png",
        "img_top_size": "full"
      }
    ],
    "2-4-4" : [
      {
        "title": "2. 유해인자 별 설명 및 신체에 미치는 영향",
        "subtitle_bubble_a" : "근무현장에서 노출될 수 있는 유해인자",
        "subtitle_box" : "정신적 유해인자 : 기타 직무 스트레스",
        "subtitle_text" : "기타 직무 스트레스",
        "content": "직무 스트레스는 업무 수행을 위해 요구되는 사항들이 본인의 요구 사항과 충돌할 경우에 발생합니다<sup>104</sup>.<br>소방공무원은 현장 출동 시 정신적, 신체적으로 부담을 주는 사건들을 자주 접하게 됩니다. 또한 출동을 하지 않더라도 방대한 양의 민원, 행정업무로 인한 스트레스를 받을 수 있습니다<sup>105</sup>.<br>직무 스트레스를 유발하는 요인은 개인적인 수준부터 조직 차원으로 기인하는 부분까지 범위가 넓으며<sup>106</sup>, 적절히 대처하는 것이 중요합니다.",
        "reference": "<sup>104</sup> 위험성평가 지원시스템 보건 분야 평가 자료: 직무스트레스, (2020년 7월 10일). URL: <a href='http://kras.kosha.or.kr/health/health_tab02'>http://kras.kosha.or.kr/health/health_tab02</a><br><sup>105</sup> 소방청, (2019). 소방공무원 마음건강 전수 설문조사<br><sup>106</sup> 최미숙 외, (2012). 소방공무원의 직무스트레스 수준과 관련 요인. 한국산학기술학회 논문지, 4917-26.",
        "img_top": "part1_2-25.png",
        "img_top_size": "mid"
      }, {
        "subtitle_text" : "기타 직무 스트레스가 신체에 미치는 영향",
        "content": "소방공무원은 현장 출동 시에 다양한 물리·화학적 유해인자뿐 아니라 직접적인 사고 위험에 노출되어 있습니다.<br>출동을 하지 않는 경우에도 민원 처리, 행정 업무 과정에서도 많은 스트레스를 받을 수 있습니다.<br>이렇게 다양한 유해인자들에 노출되어 있기 때문에 신체 및 정신적 스트레스로 인한 영향을 많이 받을 수 있습니다<sup>107</sup>.<br><br>직무스트레스란 좁게는 직업 활동 중 발생하는 스트레스를 뜻하고, 넓게는 조직 내부 및 외부 환경에서 발생하는 부조화로 인한 스트레스를 포함하는 개념입니다<sup>108</sup>.<br>소방공무원들의 직무 특성 상 위기상황에서의 스트레스는 일정 부분 피할 수 없는 것이며 적정 수준의 스트레스는 효율적인 업무 수행에 있어 긍정적인 역할을 할 수 있습니다. <br>직무스트레스에 많이 노출되더라도 소방공무원 개인의 대처방식, 사회적 지원 정도 등에 따라 고통의 정도는 상이할 수 있습니다.<br>그리고 스트레스에 의한 단기적인 반응은 대체적으로 가역적인 생체 변화를 유발하기 때문에 이를 효과적으로 대처할 경우 직무스트레스로 인한 악영향을 충분히 줄일 수 있을 것으로 알려져 있습니다<sup>109</sup>.<br><br>그러나 만성적인 직무스트레스는 심혈관계 질환(고혈압, 급성심근경색 등), 대사 질환(당뇨병, 이상지질혈증 등), 위장관계 질환(과민성 대장증후군, 역류성 식도염 등) 등의 발병 가능성을 높일 수 있으며, 불안장애, 우울장애, 알코올 및 물질사용장애와 같은 정신건강 문제의 발병 가능성 또한 높이게 됩니다.<br><br>그 외에도 소방공무원이 자주 겪는 물리적인 직무 스트레스들(무리한 동작과 근육 사용 및 장시간 과도한 긴장 상태를 유지하는 것 등)은 근골격계 질환을 야기할 수 있습니다.<br><br>이렇게 소방공무원이 업무 과정에서 장기적으로 겪는 직무스트레스들은 개인의 삶의 질을 저하시킬 뿐 아니라 개인의 수행 능력까지 저하시킬 수 있습니다<sup>110</sup>.<br>즉, 소방공무원의 직무스트레스는 소방 업무의 질을 결정하는 주요 요인으로 작용할 수 있기에, 체계적인 관리방안 마련과 제도적인 정비를 통해 직무스트레스로 인한 문제를 최소화해야 할 것입니다.",
        "reference": "<sup>107</sup> Beaton RD et al., (1993). Sources of occupational stress among firefighter/EMTs and firefighter/paramedics and correlations with job-related outcomes. Prehosp Disaster Med, 140-50.<br><sup>108</sup> 장세진 외, (2005). 한국인 직무 스트레스 측정도구의 개발 및 표준화. 대한산업의학회지, 297-317.<br><sup>109</sup> Sawhney G et al., (2018). Occupational stress and mental health symptoms: examining the moderating effect of work recovery strategies in firefighters. Journal of occupational health psychology, 23(3), 443.<br><sup>110</sup> Guidotti TL, (1992). Human factors in firefighting: ergonomic-, cardiopulmonary-, and psychogenic stress-related issues. J Occup Health Psychol, 1-12.",
        "img_top": "part1_2-26.png",
        "img_top_size": "full"
      }
    ],
    "3" : [
      {
        "title": "3. 화학적 유해인자 체내 처리 과정",
        "subtitle_bubble_q" : "몸 속으로 들어온 화학적 유해인자들은 어떻게 처리되나요?",
        "subtitle_bubble_a" : "체내로 들어온 화학적 유해인자들은 간, 신장 등의 장기에서 대사될 수 있으며, 이후 신장, 소화기관 등의 장기를 거쳐 몸 밖으로 배출될 수 있습니다.",
        "content" : "화학적 유해인자는 호흡기, 피부, 위장관 등을 통해서 체내로 들어올 수 있습니다. 체내로 들어온 유해인자는 종류에 따라 특정 장기에만 특이적으로 혹은, 체내 전반적인 세포나 조직에 널리 손상을 입히기도 합니다. 하지만 우리 몸은 유입된 이물을 대사하여 독성을 줄이거나 몸 밖으로 배설할 수 있는 능력을 지니고 있습니다.<br><br>유해인자 처리과정에서는 ‘효소’라고 불리는 단백질들이 다양한 대사 작용을 관장하게 됩니다. 각 효소들은 각자 반응할 수 있는 특정 물질과 맞물려 물질을 대사시킵니다. 유해인자의 독성을 낮추는 대사 효소들은 간, 신장, 폐 등을 포함한 장기에 널리 분포되어 있습니다. 특히 간은 우리 몸의 주된 해독 기관으로서, 유해물질 대사의 일차적인 장기입니다. 이 외에도 신장, 폐, 피부나 위장관계에서도 일부 대사작용이 일어납니다.<br><br>유해인자들 중 특히 물에 잘 녹는 성질을 지닌 유해인자(혹은 그 대사물)의 상당 부분은 신장을 거처 소변으로 배설됩니다. 그렇지 않은 경우 간에서 대사된 뒤 담즙을 거쳐 대변으로 배설될 수도 있으며 경우에 따라서는 위장관계를 거쳐 직접적으로 배설될 수도 있습니다. 일부 유해인자는 침, 땀, 날숨 등을 통해 배설됩니다.<br><br>유해인자 고유의 성질과 노출 경로에 따라 신체에 흡수되는 정도에 차이가 있으며, 체내로 유입된 이후에도 유해인자에 따라 어떤 장기에 분포하는지가 다릅니다. 또한 체내 대사 및 배설 과정에도 유해인자 고유의 성질이 큰 영향을 미치게 됩니다. 예를 들어, 물에 잘 녹지 않는 유해인자는 세포막을 통과하여 체내로 흡수되기가 쉽고 지방에 축적될 수 있기 때문에 물에 잘 녹는 인자보다 장기간 체내에 머무를 수 있게 됩니다. <br><br>더불어 소방공무원의 개인적인 특성도 유해인자의 처리 과정에 영향을 줄 수 있습니다. 예를 들어 나이, 간, 신장과 같이 유해인자 처리 장기의 기능과 관련이 있는 기저 질환, 대사 작용에 영향을 미칠 수 있는 복용 약물에 따라 유해인자가 신체에 미치는 영향의 정도나 기간이 달라질 수 있습니다<sup>111, 112</sup>.<br><br>체내에서 유해인자 일부는 적절히 대사되고 배설될 수 있지만 유해인자 노출에 의한 피해를 최소화하기 위해서는 방화복, 방화 두건과 같은 보호장비를 철저히 사용하여 유해인자에 노출을 줄이고 유해인자에 노출되었을 경우에는 유해인자의 해독을 위한 적절한 치료를 통해 신속히 대처하는 것이 중요합니다.",
        "reference" : '<sup>111</sup> Xu C et al., (2005). Induction of phase I, II and III drug metabolism/transport by xenobiotics. Arch Pharmacal Res, 249.<br><sup>112</sup> Benet LZ et al., (1996). Goodman and Gilman’s the pharmacological basis of therapeutics. McGraw-Hill Education',
        "img_bottom" : "part1_3.png",
        "img_bottom_size": "enlarge"
      }
    ],
    "4" : [
      {
        "title": "4. 소방공무원들의 유해인자 노출로 인한 뇌기능 저하 실태",
        "subtitle_text" : "뇌기능 저하로 인해 어려움을 겪는 소방공무원들",
        "content": "소방청에서는 외상후스트레스장애(posttraumatic stress disorder, PTSD), 우울장애, 수면장애, 음주습관장애 등 4대 주요 스트레스 요인 분석을 위해 '소방공무원 마음건강 상태 설문조사'를 실시하고 있습니다.<br><br>2019년도에 발표된 조사결과에서 우울증을 제외한 3가지 항목에서 전년도에 비해 발생 빈도가 증가한 것으로 나타났습니다. 외상후스트레스장애를 호소하는 소방공무원은 5.6%로 전년 대비 1.2% 증가하였으며, 전체 응답자 중 4.9%가 자살 위험군으로 분류되었습니다. 전년대비 자살에 대한 생각을 하는 소방공무원의 비율은 1.8% 감소하였지만, 여전히 상당수의 소방공무원이 심각한 스트레스에 시달리고 있는 것으로 보입니다.<br><br>특히 자살 위험군에서 외상후스트레스장애를 호소하는 비율은 54.7%로 전체 응답자 평균에 비해 10배 가량 높은 수준이었습니다. 그 외에도 자살 위험군에서의 수면 장애, 음주 습관 장애, 우울증의 비율이 전체 평균에 비해 2~3배 이상이 높았습니다.<br>또한 화재, 구조, 구급 등 재난 대응과정에서 발생하는 ‘민원응대 과부하’로 인해 관리가 필요한 경우는 29.3%이었으며, 업무 과정에서 ‘심리적 손상’을 입었다고 호소한 경우는 20.3%이었습니다<sup>113</sup>.<br><br>소방공무원은 직무상 교대근무, 외상성 뇌손상, 스트레스 등 다양한 유해인자에 노출될 수 있습니다. 이에 따라 우울, 불안, 인지저하 및 외상후스트레스 장애 등 뇌기능 저하와 다양한 신경, 정신 건강 문제가 발생할 위험이 높습니다.<br>이는 개인의 안전뿐만 아니라 소방 업무의 질과도 직결되어 국민의 안전에도 영향을 줄 수 있기에, 체계적인 관리방안을 마련하여 뇌기능 저하로 인한 문제를 최소화해야 할 것입니다.",
        "reference": "<sup>113</sup> 소방청, (2019). 2019년도 소방공무원 마음건강 전수조사 결과 발표.",
        "img_top": "part1_4-1.png",
        "img_top_size": "enlarge"
      }, {
        "subtitle_text" : "유해인자로 인한 뇌기능 저하 사례",
        "content": "<b>공무상 재해 인정 판결 사례</b><br><br>2017년 ‘30여년간 화재진압을 수행하던 소방공무원이 장기간 유해물질에 노출되어 소뇌위축증 진단을 받은 사안’에 대해 “소방공무원으로 채용될 당시 질병에 걸릴 유전적 소인이나 가족력이 없고 질병의 원인, 근무장소에 발병원인 물질이 있는지 있었는지 여부 등을 종합적으로 판단하여 추단될 수 있다면 상당인과관계가 있다.”고 여겨짐에 따라 공무상 재해로 인정받은 판결이 있었습니다<sup>114</sup>.<br><br><b>소방공무원이 흔히 경험하는 뇌기능 저하 실태에 대한 연구</b><br><br>소방공무원들은 비상대기 및 긴급출동, 극심한 반복적인 사건 노출 등으로 인한 스트레스에 장기간 반복적으로 노출될 수 있습니다.<br>다양한 정신적 유해인자로 인해 신체 및 정신적인 타격을 받을 경우 점진적으로 스트레스를 다룰 수 있는 능력이 감소하고 집중력, 기억력 저하 등 인지기능과 관련된 문제가 발생할 수 있습니다<sup>115</sup>.<br>이 외에도 물리∙화학적 인자들에 장기간, 반복적으로 노출됨에 따라 뇌기능을 포함한 신체 및 정신건강이 저하될 수 있습니다<sup>116</sup>.<br><br>소방관을 대상으로 진행된 국내∙외 연구들에서는 소방관의 뇌기능 저하 양상을 확인하고, 구조 및 기능적으로 어떤 변화가 일어날 수 있는지 확인하고자 뇌 자기공명영상(magnetic resonance imaging, MRI)과 같은 뇌영상 기법을 활용하고 있습니다.<br>최신의 뇌 자기공명영상 기법을 사용하면 뇌 내 구조적인 특성뿐만 아니라 기능적 특성, 백질 연결성, 뇌 내 소량 존재하는 대사물질 특성 등을 정밀하게 측정할 수 있습니다.<br>소방공무원을 대상으로 이러한 방법을 적용하여 유해물질이 소방공무원의 뇌 건강에 미치는 영향을 체계적으로 규명하기 위한 연구들이 현재 활발히 진행되고 있으며, 이러한 연구들은 소방공무원의 뇌 건강 관리의 중요성 에 대한 객관적인 정보를 제공할 뿐만 아니라, 뇌 기능 저하를 예방하기 위한 제도적 방안 마련을 위해서도 활용될 수 있을 것으로 기대합니다.<br><br><b>소방공무원 뇌기능 저하와 관련된 뇌 특성 규명 연구 사례</b><br><br>국내 소방공무원을 대상으로, 외상 사건에 노출되었으나 외상후스트레스장애를 경험하지는 않는 소방공무원과 일반인의 뇌 회백질과 기능적 연결성에 대한 정밀 분석을 수행한 연구 결과가 있었습니다.<br>소방공무원의 뇌 회백질 부피가 일반인에 비해 상대적으로 작고, 휴지기 기능적 연결성이 변화되어 있으며, 인지기능검사 점수가 낮았음을 확인하였습니다<sup>117</sup>.<br>이러한 결과는 외상후스트레스장애를 경험하지 않더라도 반복적인 스트레스 노출이 뇌 구조적, 기능적 변화를 유도할 수 있음을 시사합니다.<br><br>국내 소방공무원을 대상으로 진행된 다른 연구에서는 스트레스에 대한 반응과 관련된 뇌영역들의 기능적 연결성이 ‘회복탄력성’의 정도에 따라 차이가 있을 수 있음을 확인하였습니다.<br>이러한 연구 결과는 소방공무원의 회복탄력성과 관련된 뇌 기능적 특성에 대한 정보를 제공할 뿐만 아니라, 회복탄력성 저하를 보이는 소방공무원을 대상으로 한 치료법 개발에 활용될 수 있습니다<sup>118</sup>.<br><br>또한, 국내 소방공무원에서 가벼운 수준의 염증(low-grade inflammation)을 가지고 있는 사람이 스트레스에 노출될 경우 그렇지 않은 사람에 비해 뇌기능 저하 현상이 두드러지고, 외상후스트레스장애를 포함한 스트레스성 질환의 위험성이 현저히 높아질 수 있음을 밝힌 뇌영상 연구도 있습니다.<br>해당 연구에서는 스트레스 상황에 자주 노출되는 소방관 100명과 일반인 700명을 대상으로 기능적 뇌영상 분석을 수행하였고, 가벼운 염증 상태가 있는 사람은 정상 사람에 비해 판단력, 기억력, 실행 능력, 언어능력 등 고위인지기능과 뇌의 기능적 연결성이 저하되어 있음을 확인하였습니다. <br>특히 가벼운 염증을 지닌 소방공무원 경우 뇌 내 기능적 연결성이 많이 저하되어 있었고, 이 경우 외상후스트레스장애 발병의 가능성이 높을 수 있다고 확인하였습니다.<br>해당 연구 결과는 가벼운 염증이 비만, 당뇨병, 고혈압, 심장병 등의 신체 질환뿐만 아니라, 뇌기능 저하와도 관련될 수 있으며, 특히 스트레스에 반복적으로 노출되는 소방관에서 염증 수치를 통해 뇌기능 저하의 위험을 예측할 수 있음을 객관적인 뇌영상 정보를 사용하여 확인한 의미가 있습니다<sup>119</119>.",
        "reference": "<sup>114</sup> 대법원, (2017). 공무상요양불승인처분취소. [대법원 2017. 9. 21., 선고, 2017두47878, 판결]<br><sup>115</sup> Barrett DH et al., (1996). Cognitive functioning and posttraumatic stress disorder. Am J Psychiatry, 1492-4.<br><sup>116</sup> Beaton RD et al., (1993). Sources of occupational stress among firefighter/EMTs and firefighter/paramedics and correlations with job-related outcomes. Prehosp Disaster Med, 140-50.<br><sup>117</sup> Park YW et al., (2020). Structural and resting-state brain alterations in trauma-exposed firefighters: preliminary Results. J Korean Soc Radiol, 676-87.<br><sup>118</sup> Reynaud E et al., (2013). Relationship between emotional experience and resilience: an fMRI study in fire-fighters. Neuropsychologia, 845-9.<br><sup>119</sup> Kim J et al., (2020). A double-hit of stress and low-grade inflammation on functional brain network mediates posttraumatic stress symptoms. Nat Commun, 1-12.",
      }, {
        "subtitle_text" : "두부 외상으로 인한 뇌기능 저하 사례",
        "content": "두부 외상은 머리에 충격이 가해짐에 따라 발생하는 손상을 지칭합니다.<br>부드러운 조직을 지닌 뇌는 뇌척수액이라는 액체 가운데에 있습니다. 그 위에 몇 층의 조직이 둘러싸고 있으며 가장 바깥에는 단단한 두개골이 뇌를 보호합니다.<br><br>두부 외상의 흔한 원인은 낙상, 교통사고 등이며, 소방공무원의 경우 위험한 작업 현장에서 머리에 타격을 줄 수 있는 많은 인자들에 노출될 수 있습니다. <br>뿐만 아니라 갑작스러운 가속, 감속 상황에서 조직이 연한 뇌가 단단한 두개골에 충돌하게 되는 경우도 뇌 손상이 발생할 수 있습니다.<br><br>두부 외상에 의한 뇌의 손상을 외상성 뇌손상이라 합니다. <br>외상성 뇌손상은 외상 후의 의식 소실 지속 기간, 기억 상실 지속 기간, 신경학적 이상 정도에 따라 심한 정도를 경도, 중등도, 고도 외상성 뇌손상으로 분류합니다.<br><br>이 중 경도 외상성 뇌손상(mild traumatic brain injury, mTBI)이 가장 흔하며, 소방공무원도 흔히 경험할 수 있습니다. 특히 뇌의 뚜렷한 구조적 이상 없이 두통, 어지럼증, 메스꺼움 등 뇌진탕후증후군(post-concussion syndrome) 증상이 나타날 수 있습니다.<br>또한, 안절부절 못하는 느낌, 구역, 구토 증상을 겪기도 하며, 심한 외상성 뇌손상의 경우에는 사고력, 감정 조절 능력, 운동 능력 등이 저하될 수 있습니다.<br><br>외상성 뇌손상이 의심되면 정확한 뇌 손상 정도를 확인하기 위해서는 두부 외상 이후에 컴퓨터단층촬영(computerized tomography, CT)이나 자기공명영상(magnetic resonance imaging, MRI) 촬영을 진행하는 것이 필요할 수 있습니다.<sup>120</sup><br><br>권투 선수, 미식축구 선수 등 반복적으로 두부 외상을 경험하는 운동선수를 대상의 뇌영상 연구들에서는 경도 외상성 뇌손상과 관련된 뇌 신경계의 구조적, 기능적 손상이 자주 보고되고 있습니다.<br>또한 아직 원인은 명확히 밝혀지지 않았으나 경도 외상성 뇌손상은 소방공무원에서 발병 위험이 높은 외상후스트레스장애(posttraumatic stress disorder, PTSD)의 발병과도 밀접히 관련되어 있는 것으로 알려져 있어 더욱 적절한 치료가 필요할 수 있습니다.<sup>121</sup>",
        "reference": "<sup>120</sup> Bruns Jr et al., (2003). The epidemiology of traumatic brain injury: a review. Epilepsia, 2-10.<br><sup>121</sup> Bryant R., (2011). Post-traumatic stress disorder vs traumatic brain injury. Dialogues Clin Neuro, 251.",
        "img_top": "part1_4-2.png",
        "img_top_size": "mid"
      }
    ]
  }
  part1SubContentList: any = [
    { "subject" : "물리적 유해인자", 
      "subBtn" : [ 
        { "title" : "고열", "content": "2-1-1" },
        { "title" : "소음", "content": "2-1-2" }, 
        { "title" : "중량물 작업", "content": "2-1-3"}, 
        { "title" : "위험한 작업 현장", "content": "2-1-4"} 
      ]
    },
    { "subject" : "화학적 유해인자", 
      "subBtn" : [ 
        { "title" : "유해가스", "content": "2-2-1" },
        { "title" : "석면 등 분진", "content": "2-2-2" }, 
        { "title" : "중금속", "content": "2-2-3" }, 
        { "title" : "벤젠 등 유기화합물", "content": "2-2-4" }
      ] 
    },
    { "subject" : "생물학적 유해인자", 
      "subBtn" : [ 
        { "title" : "감염인자", "content": "2-3-1" }
      ] 
    },
    { "subject" : "정신적 유해인자", 
      "subBtn" : [ 
        { "title" : "교대근무로 인한 불규칙한 수면", "content": "2-4-1" },
        { "title" : "비상대기 및 긴급 출동", "content": "2-4-2" },
        { "title" : "반복적인 사건 노출", "content": "2-4-3" },
        { "title" : "기타 직무 스트레스", "content": "2-4-4" },
      ]
    }
  ]

  constructor(
    private elRef: ElementRef,
    public renderer: Renderer,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
    private iab: InAppBrowser) {
    this.selectedContent = this.navParams.get('content');
    this.subcontent = this.navParams.get('subcontent');
  }

  ionViewDidLoad() {
    // json 중 link 연결이 필요한 내용이 첨부되어있을 경우 listen, 클릭 이벤트 발생시 처리
    if (this.elRef.nativeElement.querySelectorAll('a')) {
      let links = this.elRef.nativeElement.querySelectorAll('a');
      for (let link of links) {
        this.renderer.listen(link, 'click', (evt) => {
          this.goLink(link.getAttribute('href'));
          return false;
        });
      }
    }
  }

  openEnlarge(src) {
    let enlargeModal = this.modalCtrl.create( EnlargeModalPage, { src: src });
    enlargeModal.onDidDismiss(data => {
      console.log(data);
    });
    enlargeModal.present();
  }

  goLink(url) {
    if (!url) return;
    this.iab.create(url);

    // this.iab.create(url, '_system')
    // window.open(url, '_system');
  }

  goList(){
    this.navCtrl.push(this.contentPart1, {}, { animate: true, direction: 'back' })
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }
}
