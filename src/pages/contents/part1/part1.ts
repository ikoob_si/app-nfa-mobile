import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Part1DetailPage } from './part1-detail/part1-detail';
import { HomePage } from '../../home/home';

@Component({
  selector: 'page-part1',
  templateUrl: 'part1.html',
})
export class Part1Page {
  
  public content = Part1DetailPage;

  contentList: any = {
    "subject" : "유해인자",
    "btn" : [
      { "title" : "유해인자 설명", "content" : "1" },  
      { "title" : "유해인자 별 설명 및 신체에 미치는 영향",
        "detail": [
          { "subject" : "물리적 유해인자", 
            "subBtn" : [ 
              { "title" : "고열", "content": "2-1-1" },
              { "title" : "소음", "content": "2-1-2" }, 
              { "title" : "중량물 작업", "content": "2-1-3"}, 
              { "title" : "위험한 작업 현장", "content": "2-1-4"} 
            ]
          },
          { "subject" : "화학적 유해인자", 
            "subBtn" : [ 
              { "title" : "유해가스", "content": "2-2-1" },
              { "title" : "석면 등 분진", "content": "2-2-2" }, 
              { "title" : "중금속", "content": "2-2-3" }, 
              { "title" : "벤젠 등 유기화합물", "content": "2-2-4" }
            ] 
          },
          { "subject" : "생물학적 유해인자", 
            "subBtn" : [ 
              { "title" : "감염인자", "content": "2-3-1" }
            ] 
          },
          { "subject" : "정신적 유해인자", 
            "subBtn" : [ 
              { "title" : "교대근무로 인한 불규칙한 수면", "content": "2-4-1" },
              { "title" : "비상대기 및 긴급 출동", "content": "2-4-2" },
              { "title" : "반복적인 사건 노출", "content": "2-4-3" },
              { "title" : "기타 직무 스트레스", "content": "2-4-4" },
            ]
          },
        ]
      }, 
      { "title" : "화학적 유해인자 체내 처리 과정", "content" : "3" }, 
      { "title" : "소방공무원들의 유해인자 노출로 인한 뇌기능 저하 실태", "content" : "4" }
    ]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}
