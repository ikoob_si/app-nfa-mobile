import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Part2DetailPage } from './part2-detail/part2-detail';
import { HomePage } from '../../home/home';

@Component({
  selector: 'page-part2',
  templateUrl: 'part2.html',
})
export class Part2Page {

  public content = Part2DetailPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  contentList: any = {
    "subject" : "뇌기능 저하 및 회복 탄력성",
    "btn" : [
      { "title" : "회복 탄력성", "content" : "1" },
      { "title" : "뇌기능 저하로 나타날 수 있는 증상",
        "detail" : [
          { "subBtn" : [
              { "title" : "우울", "content" : "2-1"},
              { "title" : "수면장애", "content" : "2-2"},
              { "title" : "외상후스트레스장애(PTSD)", "content" : "2-3"},
              { "title" : "음주습관장애", "content" : "2-4"},
            ]
          }
        ]
      },
      { "title" : "뇌기능 저하 자가진단 방법",
        "detail" : [
          { "subBtn" : [
              { "title" : "우울", "content" : "3-1"},
              { "title" : "수면장애", "content" : "3-2"},
              { "title" : "외상후스트레스장애(PTSD)", "content" : "3-3"},
              { "title" : "음주습관장애", "content" : "3-4"},
              { "title" : "인지기능 저하", "content" : "3-5"},
            ]
          }
        ]
      },
      { "title" : "뇌기능 저하 평가 방법",
        "detail" : [
          { "subBtn" : [
              { "title" : "정밀 뇌영상 검사", "content" : "4-1"},
              { "title" : "혈액 검사", "content" : "4-2"},
              { "title" : "타액 코티솔 바이오키트", "content" : "4-3"},
            ]
          }
        ]
      },
      { "title" : "인공지능 모델을 통한 뇌기능 저하 평가 방법", "content" : "5" },
    ]
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}
