import { Component, ElementRef, Renderer } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Part2Page } from '../part2';
import { EnlargeModalPage } from '../../../enlarge-modal/enlarge-modal';
import { HomePage } from '../../../home/home';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-part2-detail',
  templateUrl: 'part2-detail.html',
})
export class Part2DetailPage {

  public contentPart2 = Part2Page;
  public contentDetail = Part2DetailPage;
  public selectedContent: string;
  public subcontent: any;

  contents: any = {
    "1" : [
      {
        "title" : "1. 회복탄력성",
        "subtitle_bubble_q" : "유해인자 노출에 의한 뇌기능 저하는 어떻게 회복할 수 있나요?",
        "content" : "소방공무원은 업무 특성상 사건 현장 속에서 여러 유해인자에 반복적으로 노출될 수 있습니다. 유해인자에 의한 심리적, 육체적 스트레스는 신체뿐만 아니라 뇌기능에도 영향을 미칠 수 있으며, 이로 인해 외상후스트레스장애, 우울증 등 여러 정신건강 문제가 발생할 수 있습니다.<br><br>그러나 일부 사람들은 같은 현장에 출동하더라도 특별한 증상 없는 반면 어떤 사람들은 뇌기능 저하로 오랫동안 힘들어하는 모습을 보여주기도 합니다. 이러한 차이 중 일부는 개인별로 회복탄력성에 차이가 있기 때문일 수 있습니다. <br><br>회복탄력성이란 지속적인 스트레스 상황 에서 우리의 뇌와 신체가 유해한 변화를 겪지 않도록 건강한 상태를 유지하는 능력입니다. 건강한 상태란 단순히 질병이 없는 것이 아니라 스트레스로 인해 발생할 수 있는 부정적인 변화를 극복하고 심리적, 신체적 건강을 유지하도록 하는 과정입니다<sup>122</sup>.<br><br>우리의 뇌는 신체적 변화를 수반하는 스트레스 반응에 핵심적인 기관입니다. 사건 현장에 출동하게 되면 뇌는 긴박한 상황 속에서 여러 위험 요소를 인지하고 위기에 대비한 스트레스 반응을 일으킵니다<sup>123</sup>.<br>스트레스 반응이 일어나면 심장은 더욱 빠르게 뛰고, 동공이 확장되며, 근육이 수축하는 등 보다 뛰어난 신체 능력을 발휘할 수 있게 됩니다<sup>124</sup>. 이 과정은 호르몬, 신경전달물질을 통해 자율신경계, 시상하부-뇌하수체-부신 축(Hypothalamic-Pituitary-Adrenal Axis)에 의해 조절되며, 뇌는 신경화학물질 변화를 감지하여 신체 상태를 파악하고, 외부 환경 변화에 따라 신경화학물질 분비를 조절함으로써 적절한 스트레스 반응에 중요한 역할을 합니다<sup>125,126</sup>.<br>이후 스트레스 상황에서 벗어나면 더 이상 주변을 경계하지 않고 긴장을 풀어 편안한 상태로 되돌아오며, 이렇듯 신체 상태를 일정하게 유지하는 현상을 항상성 조절 능력이라 합니다.<br><br>그러나 소방공무원의 경우 반복적으로 스트레스 상황에 노출되면 충분한 회복이 이루어지지 못한 채 심리적, 신체적 변화가 지속되면 뇌는 스트레스 반응 조절에 이상을 보일 수 있습니다<sup>127</sup>.<br>특히 소방공무원이 직간접적으로 경험할 수 있는 외상 사건 현장은 해마, 편도체, 전두엽 등 여러 뇌 영역의 변화를 유발하기 때문에 스트레스 반응 조절뿐만 아니라 인지 기능, 감정 조절 능력과 같이 주요 뇌기능에도 영향을 미칠 수 있습니다<sup>128</sup>.<br><br>추론이나 개념 형성, 전략적 실행 능력 등 고위인지기능은 여러 뇌 영역이 기능적으로 연결되어 작용하는 뇌 네트워크와 관련이 있습니다.<br>대표적인 고위인지기능 네트워크로 기본상태 네트워크(default mode network), 중앙집행기능 네트워크(central executive network), 현출성 네트워크(salience network) 등이 있으며, 이러한 네트워크는 스트레스 반응에 의해 연결에 변화가 생길 수 있고 뇌기능 저하를 유발할 수 있습니다.<br><br>뇌기능 저하가 지속될 경우 외상후스트레스장애, 우울증 등 여러 심리적, 신체적 건강 문제까지 나타날 수 있습니다<sup>129</sup>. 따라서 회복탄력성을 향상시킴으로써 뇌기능 저하를 개선하고 최적의 뇌 건강을 유지하는 것이 중요합니다.",
        "reference" : "<sup>122</sup> Russo et al., (2012). Neurobiology of resilience. Nat Neurosci, 1475-84<br><sup>123</sup> Osorio et al., (2017). Adapting to stress: understanding the neurobiology of resilience. Behav Med, 307-22<br><sup>124</sup> Schmidt et al., (1989). Human physiology. Springer<br><sup>125</sup> Osorio et al., (2017). Adapting to stress: understanding the neurobiology of resilience. Behav Med, 307-22<br><sup>126</sup> Jansen et al., (1995). Central command neurons of the sympathetic nervous system: basis of the fight-or-flight response. Science, 644-6<br><sup>127</sup> Osorio et al., (2017). Adapting to stress: understanding the neurobiology of resilience. Behav Med, 307-22<br><sup>128</sup> McEwen, Morrison, (2013). The brain on stress: vulnerability and plasticity of the prefrontal cortex over the life course. Neuron, 16-29<br><sup>129</sup> Osorio et al., (2017). Adapting to stress: understanding the neurobiology of resilience. Behav Med, 307-22",
        "img_bottom": "part2_1-1.jpg",
        "img_bottom_size": "full"
      },
      {
        "subtitle_text" : "회복탄력성에 영향을 미치는 요인",
        "content" : "회복탄력성은 고정된 능력이 아니며, 여러 가지 요인에 의해 영향을 받을 수 있습니다.<br><br><b>생물학적 요인</b><br><br>우리 뇌와 몸에서 일어나는 스트레스 반응은 신경전달물질, 호르몬 등 여러 신경화학물질을 통해 신경회로와 분자경로를 조절하여 발생합니다<sup>130</sup>.<br>따라서 신경화학물질, 호르몬, 신경회로 및 분자경로에 관여된 여러 유전자의 차이에 따라 개인별 회복탄력성에 차이가 있을 수 있습니다. 일례로, 신경화학물질 Neuropeptide Y의 유전적 차이에 따라 외상후스트레스장애, 우울증 등 정신건강 문제에 대한 취약성이 개인별로 다르다는 점이 알려져 있습니다.<br><br><b>경험적 요인</b><br><br>개인별 생물학적 차이뿐만 아니라 삶을 구성하는 과거 경험들 역시 회복탄력성을 변화시키는 주요한 요인이 될 수 있습니다.<br>과거 순차적인 스트레스를 경험한 사람들의 경우 스트레스 상황에 대한 적응에 우수한 경향을 보였으며, 성장 과정에서 문제 상황을 직면하고 극복했던 경험 또한 회복탄력성 향상의 잠재적 요인일 수 있습니다.<br>따라서 평소 스트레스에 대한 적절한 관리와 훈련은 회복탄력성을 증진시키고, 뇌기능 저하를 예방할 수 있습니다.<br><br><b>음주, 흡연</b><br><br>반면에 회복탄력성을 저해하는 요인 역시 존재합니다. 대표적인 예로 음주와 흡연을 들 수 있습니다.<br>음주의 경우 단순히 음주량뿐만 아니라 부적절한 음주 습관 역시 회복탄력성을 떨어뜨릴 수 있으며<sup>131</sup>, 흡연 또한 회복탄력성을 낮추는 위험 요소로 작용할 수 있습니다<sup>132</sup>.<br>따라서 스트레스를 해소하고자 흔히 하게 되는 음주와 흡연은 오히려 회복탄력성을 감소시키며 뇌기능 저하를 촉진시킬 수 있어 주의가 필요합니다.",
        "reference" : "<sup>130</sup> Osorio et al., (2017). Adapting to stress: understanding the neurobiology of resilience. Behav Med, 307-22<br><sup>131</sup> Morgan et al., (2018). Resilience as a moderating factor between stress and alcohol-related consequences in the Army National Guard. Addict Behav, 22-7<br><sup>132</sup> Kennedy et al., (2019). Low stress Resilience in Late Adolescence and Risk of Smoking, High Alcohol Consumption and Drug Use Later in Life. J Epidemiol Community Health, 496-501",
        "img_bottom" : "part2_1-2.jpg",
        "img_bottom_size" : "full"
      }
    ],
    "2-1" : [
      {
        "title" : "2. 뇌기능 저하로 나타날 수 있는 증상",
        "subtitle_box" : "우울",
        "text_box" : "2주 내내 매일 우울한 기분이 들거나 가라앉았던 적이 있나요?<br>우울해 보인다는 얘기를 자주 듣나요?<br>평소에는 즐겨하던 일이 거의 매일 흥미 없거나 즐겁지 않은가요?",
        "content" : "최소 2주간 지속적으로 느끼는 우울감과 흥미/즐거움의 상실은 흔히 우울증이라고 말하는 주요 우울장애(Major Depressive Disorder, MDD)의 주된 증상입니다.<br><br>주요 우울장애는 우울한 기분을 주된 증상으로 하며, 합리적인 사고 과정, 관심있는 것에 대한 의욕, 수면, 신체활동 등 전반적인 기능이 지속적으로 저하되고 일상생활에 부정적인 영향을 미치는 상태를 말합니다<sup>133</sup>.<br><br>우울한 기분 외에도 초조함과 무가치감, 죄책감 등 부정적인 기분을 함께 느낄 수 있습니다.<br>또한, 기분의 변화 외에도 한 달 동안 5% 이상의 급격한 체중 변화가 있거나 잠을 거의 자지 못하는 것 혹은 오히려 너무 많이 자는 것, 중요한 일에 집중하기 어려운 것, 죽음과 자살에 대한 생각이 많아지는 것도 주요 우울장애로 인해 나타날 수 있는 증상들입니다.<br><br>우울장애는 소방공무원의 4대 주요 스트레스 요인 중 하나로 선정된 바 있습니다<sup>134</sup>. 소방공무원 마음건강 설문 전수조사에 따르면, 우울장애 위험군은 2018년 기준 2,237명으로 2017년도 대비 227명 증가했습니다<sup>135</sup>.<br><br>반복적인 재난 상황에 노출과 장시간의 긴장, 다양한 행정업무 등은 심각한 정신적 스트레스를 유발시키고, 이러한 직무 스트레스 요인들이 우울장애의 위험을 높이며 부정적인 영향을 줄 수 있습니다 <sup>136</sup>.<br>또한, 외상후스트레스장애(PTSD) 증상을 보이는 소방대원이 증상이 없는 대원에 비해 직무 스트레스와 우울증상을 더 많이 겪을 수 있습니다<sup>137</sup>.<br>따라서, 필요 시 적절한 치료를 받고 긍정적인 활동을 통한 스트레스 관리가 필요합니다.",
        "reference" : "<sup>133</sup> Michael B. First et al., (2017). SCID-5-시리즈 DSM-5장애에 대한 구조화된 임상적 면담 전문가 지침서, ㈜인싸이트<br><sup>134</sup> 소방청, (2019). 2019년도 소방공무원 마음건강 전수 조사 결과 발표 –PTSD, 수면장애, 음주습관장애, 우울증, 삶의 만족도, 자해시도 조사-<br><sup>135</sup> 소방청, (2019). 소방관 마음건강 상태는? –‘19년도 소방공무원 마음건강 설문 전수조사 결과 1차 발표 -<br><sup>136</sup> 양찬모 외., (2014). 일 도 소방공무원의 직무 스트레스와 우울 및 삶의 질. 우울조울병, 103-109.<br>김광석, 외 (2014). 소방관의 직무스트레스가 우울 및 피로에 미치는 영향. 한국콘텐츠학회논문지, 223-231.<br>Guidotti, T. L. (1992). Human factors in firefighting: ergonomic-, cardiopulmonary-, and psychogenic stress-related issues. International archives of occupational and environmental health, 1-12.<br><sup>137</sup> 신용식. (2015). 소방공무원의 외상후스트레스장애, 직무스트레스, 우울증에 대한 고찰. 2. 한국방재학회 논문집, 15, 233-239.",
      }
    ],
    "2-2" : [
      {
        "title" : "2. 뇌기능 저하로 나타날 수 있는 증상",
        "subtitle_box" : "수면장애",
        "content" : "수면장애는 현재 수면의 양이나 질이 만족스럽지 못하고, 여러 불면 증상이 일주일에 3회 이상, 3개월 이상 지속되는 질환입니다. 이로 인해 일상생활에 부정적인 영향을 받는다면 수면장애를 의심해볼 수 있습니다<sup>138</sup>.<br><br>수면장애를 겪으면 잠을 자고 싶어도 잠에 들기 어렵고, 한밤중에 자주 깨거나 깬 후에 다시 잠들기 어려운 증상이 나타납니다.<br>또한 일어나려는 시간보다 훨씬 일찍 일어나게 되어 다시 잠들기 어려울 수 있고, 충분히 잘 수 있는 상황에서도 제대로 잠들지 못합니다.<br><br>교대 근무 수면장애는 교대근무 동안 과도하게 졸리고, 자야 할 시간에는 잠을 자지 못하는 수면 문제를 포함하는 질환입니다<sup>139</sup>.<br>교대근무 일정은 일주기 리듬을 방해하기 쉬워 수면의 질을 떨어뜨릴 수 있습니다. 소방공무원에서 일주기 리듬이 방해 받을수록 수면의 질은 더 나빠진다는 연구결과도 보고된 바 있습니다<sup>140</sup>.<br>수면의 질 저하와 수면량 부족은 업무 수행뿐만 아니라 장기적인 업무 지속에도 부정적인 영향을 끼칠 수 있습니다.<br><br>제대로 수면을 취하지 못하면 단기적으로는 업무 수행능력이 저하되며 피로가 쌓이게 됩니다.<br>또한 기억력 문제와 인지능력이 저하가 나타날 수 있습니다.<br>게다가 외부 요소에 대한 반응속도가 느려져 위험한 상황에 대해 적절한 대처를 하지 못할 수 있습니다. 특히 교대 근무 수면장애가 있는 교대 근무자는 그렇지 않은 교대 근무자에 비해 자동차 사고 발생의 비율이 높은 것을 확인한 연구결과가 있습니다<sup>141</sup>.<br><br>수면장애가 장기화된다면 심혈관계 질환의 위험성이 증가하고 비만과 당뇨 등의 발병 확률이 높아질 수 있습니다. 또한 위장 문제와 면역력 감소 등 건강에도 영향이 있을 수 있습니다<sup>142</sup>.<br><br>과도한 음주, 우울감, 불안감, 충격적인 사건의 경험 또한 수면의 질에 영향을 미칠 수 있습니다<sup>143</sup>.",
        "reference" : "<sup>138</sup> Michael B. First et al., (2017). SCID-5-시리즈 DSM-5장애에 대한 구조화된 임상적 면담 전문가 지침서, ㈜인싸이트<br><sup>139</sup> 산업안전보건연구원, (2017). 야간근무자의 수면장애 실태 및 관리방안<br>Schwartz, J. R., & Roth, T. (2006). Shift work sleep disorder. Drugs, 66(18), 2357-2370.<br><sup>140</sup> Billings, J., & Focht, W. (2016). Firefighter shift schedules affect sleep quality. Journal of occupational and environmental medicine, 58(3), 294-298.<br><sup>141</sup> 이수지 외., (2017). 인지 및 행동영역에서 교대 근무의 유해적인 영향: 비판적 고찰. 생물정신의학, 59-67.<br><sup>142</sup> 미국연방소방국(U.S. Fire Administration). (2020년 7월 3일). URL: <a href='https://www.usfa.fema.gov/current_events/081717.html'>https://www.usfa.fema.gov/current_events/081717.html</a><br><sup>143</sup> 오진욱 외., (2018). 소방공무원의 수면의 질에 대한 영향요인. 정신신체의학, 19-25.",
      }
    ],
    "2-3" : [
      {
        "title" : "2. 뇌기능 저하로 나타날 수 있는 증상",
        "subtitle_box" : "외상후스트레스장애(PTSD)",
        "text_box" : "<i>“맨 처음에 출동 나갔을 때에요. 교통사고 현장이었는데 굉장히 많이 다치신 여성분과 눈빛이 마주친 적이 있습니다. 잊혀지지 않아요.”</i><br><i>“어린 아이가 트럭에 깔린 적이 있거든요. 저희 집 애도 생각나고… 애라는 것 자체만으로도 되게 충격이 크거든요.”</i><br><i>“어느날 생각해보니까 저희 집에 가서 짜증을 내고 있더라고요.”</i><br>- 소방청TV, ‘[소방청] PTSD에 대한 소방관들의 이야기’ 중에서<sup>144</sup> - ",
        "content" : "외상후스트레스장애 (Posttraumatic Stress Disorder, PTSD)는<sup>145</sup> 생명을 위협하는 사건, 심각한 부상, 성폭력, 재난 등으로 인한 심리적인 외상 사건을 ‘트라우마’ 라고 정의할 수 있습니다.<br>트라우마를 직접 경험하거나 목격 또는 가까운 지인이 겪은 것을 알게 된 이후, 불쾌한 기억이 반복적으로 떠오르거나 생각과 감정이 부정적으로 변화하는 등 일상생활에서 불편감이 한달 이상 지속되는 경우 외상후스트레스장애를 의심할 수 있습니다.<br><br>외상후스트레스장애는 재경험, 회피, 생각과 감정의 부정적인 변화, 과각성이 대표적인 증상입니다. 재경험은 원하지 않는 기억이 반복적으로 떠오르는 현상입니다. 이 증상이 있으면 외상 사건과 관련된 악몽을 꾸거나 사건이 다시 일어나는 것처럼 느끼며 행동하게 될 수 있습니다.<br>이와 더불어 외상 사건을 떠오르게 하는 특정 단서를 보았을 때, 불쾌한 감정이 들거나 부정적인 신체적 증상 (식은땀, 가슴 두근거림 등)이 나타날 수 있습니다.<br>또한, 외상 사건과 밀접하게 관련된 기억, 생각, 감정들을 피하기 위해 많은 노력을 하게 되는 회피 증상이 나타날 수 있습니다. <br>생각과 감정의 변화도 나타날 수 있는데 자신과 주변 사람들, 세상에 대한 생각이 부정적으로 바뀌고, 부정적인 감정이 지속되며 긍정적인 감정을 느끼는 것이 어렵게 됩니다. <br>평소에는 즐거웠던 활동에 흥미가 떨어지고, 사람들로부터 단절되어 있다는 느낌을 받을 수 있습니다. <br>이뿐만 아니라, 과각성 증상으로 무모하고 과격한 행동을 하거나 작은 소리나 움직임에도 깜짝 놀라는 등 예민하게 반응 할 수 있습니다. 무언가를 기억하고 집중하기 힘들어지거나 수면 관련 문제들도 외상후스트레스장애의 증상일 수 있습니다.<br><br>소방공무원은 외상 사건들을 반복적으로 겪고, 교대근무와 직무 스트레스 등으로 인해 외상후스트레스장애에 노출될 위험이 큰 직업군입니다.<br>2019년도 소방공무원 마음건강 설문 전수조사 결과에 따르면, 소방공무원의 외상후스트레스장애 유병률은 전년도 대비 1.2% 증가한 5.6% (2,804명)으로 나타났습니다<sup>146</sup>.<br>외상후스트레스장애를 악화시키는 요인으로는 우울, 불안, 수면장애, 직무스트레스, 음주 등이 있으며, 외상후스트레스를 극복하기 위해서는 사회적 지지와 건강한 심리사회적 근무환경이 무엇보다 중요합니다<sup>147</sup>.",
        "reference" : "<sup>144</sup> 소방청TV 공식 유튜브 채널, (2020년 7월 4일). URL: <a href='https://www.youtube.com/watch?v=QXwxGNlrbpg'>https://www.youtube.com/watch?v=QXwxGNlrbpg</a><br><sup>145</sup> Michael B. First et al., (2017). SCID-5-시리즈 DSM-5장애에 대한 구조화된 임상적 면담 전문가 지침서, ㈜인싸이트<br><sup>146</sup> 소방청, (2019). 소방관 마음건강 상태는? –‘19년도 소방공무원 마음건강 설문 전수조사 결과 1차 발표 <br><sup>147</sup> 류지아 외., (2017). 소방공무원과 외상 후 스트레스 장애. 생물정신의학, 10-18.",
      }
    ],
    "2-4" : [
      {
        "title" : "2. 뇌기능 저하로 나타날 수 있는 증상",
        "subtitle_box" : "음주습관장애",
        "content" : "술을 마시는 양, 음주 시간, 음주 횟수, 음주를 조절하거나 통제하려는 노력, 음주로 인해 생긴 변화 등 음주 사용 습관과 그로 인한 영향들을 통해 음주습관장애 여부를 확인할 수 있습니다.<br><br>음주로 인해 일상생활에 부정적인 영향을 줄 수 있는 아래의 증상들이 1년 이내의 기간 동안 2개 이상 있다면 음주습관장애를 의심할 수 있습니다<sup>148</sup>.<br><br><table><tr><td>의도했던 것보다 술을 많이 마시거나 오래 마시게 된다.</td></tr><tr><td>술을 줄이거나 조절하려고 노력하지만 매번 실패한다.</td></tr><tr><td>술을 사고, 술에 취하고, 숙취에 시달리는 데 많은 시간을 보낸다.</td></tr><tr><td>음주를 하지 않을 때도 술이 계속 생각나서 다른 것을 생각하는 데 어렵다.</td></tr><tr><td>음주로 인해 직장, 학교 또는 집에서의 주요한 역할을 수행하지 못한다.</td></tr><tr><td>음주로 인해 사회적 또는 대인 간 문제가 있음에도 계속 마신다.</td></tr><tr><td>술을 마시기 위해 직장, 학교 또는 가정에서 중요한 일들을 포기하거나 줄인다.</td></tr><tr><td>집중력이 필요한 일을 하기 전에도 술을 마셔서 본인 또는 타인에게 해가 될 수 있는 행동을 한 적이 있다.</td></tr><tr><td>음주로 인해 신체적 또는 정신적 문제가 생겼음을 알고 있음에도 계속 마신다.</td></tr><tr><td>술에 취하기 위해 평소보다 마시거나, 평소에 마시는 양을 마셔도 술에 취하지 않는다.</td></tr><tr><td>술을 그만 마시거나 마시는 양을 줄인 직후에 금단현상이 나타나거나, 금단현상이 나타나는 것을 피하려고 술 또는 다른 약을 복용한 적이 있다.</td></tr></table><br>소방공무원은 위험성이 높은 현장, 조직체계, 직장동료와의 갈등과 같은 직무스트레스를 겪고 있으며, 이를 해소하는 방법 중에 하나로 음주를 선택하는 경우가 많습니다<sup>149</sup>. <br>2019년도 소방공무원들의 스트레스 요인에 대한 분석 결과, 소방공무원의 음주습관장애 유병률은 전년도 대비 1.6% 증가한 29.9% (14,841명)으로 다른 요인들(외상후스트레스장애, 우울장애, 수면장애)과 비교해 가장 높은 빈도로 나타났습니다<sup>150</sup>.<br>또한, 흡연, 외상 사건 경험, 외상후스트레스장애 증상이 음주와 유의미한 관련성이 있다는 연구 결과도 보고된 바 있습니다<sup>151</sup>. 음주습관장애로 인한 정신적, 개인의 건강 차원을 넘어서 시민의 안전을 책임지는 직업군인 소방공무원의 음주습관장애 관리는 특히 중요합니다.",
        "reference" : "<sup>148</sup> Michael B. First et al., (2017). SCID-5-시리즈 DSM-5장애에 대한 구조화된 임상적 면담 전문가 지침서, ㈜인싸이트<br><sup>149</sup> 심규식 외., (2019). 소방공무원의 음주 실태와 직무스트레스 관계 분석. 한국화재소방학회논문지, 33, 132-38.<br><sup>150</sup> 소방청, (2019). 소방관 마음건강 상태는? –‘19년도 소방공무원 마음건강 설문 전수조사 결과 1차 발표 <br><sup>151</sup> 오현정 외., (2011). 남자 소방공무원의 외상적 사건 경험과 알코올 사용과의 관련성. 신경정신의학, 316-22.",
      }
    ],
    "3-1" : [
      {
        "title" : "3. 뇌기능 저하 자가진단 방법",
        "subtitle_bubble_a" : "업무 스트레스로 인해 겪을 수 있는 증상에 대해 스스로 확인해볼 수 있는 방법들을 소개합니다.",
        "subtitle_box" : "우울",
        "content": "벡 우울척도 (Beck Depression Inventory-II, BDI-II)는 우울증상이 있는지, 얼마나 심각한지를 알아볼 수 있는 자기보고형 척도입니다.<br>1961년 Aaron T. Beck에 의해 최초로 개발되었고 1996년 새로운 우울증 진단 기준에 근거하여 수정된 BDI-II가 현재까지 사용되고 있습니다<sup>152</sup>.<br><br>지난 2주 간의 증상을 평가하며, 증상의 정도를 표현하는 21개의 구체적인 문장으로 구성되어 있어 증상에 따라 스스로 표시하고 점수화할 수 있습니다<sup>153</sup>.<br><br>벡 우울척도에서 각 우울증상을 대표하는 예시문항는 다음과 같습니다.<br><br><table><tr><th>슬픔</th><td>나는 자주 마음이 슬프다 (1점)</td></tr><tr><th>즐거움을 잃어버림</th><td>나는 예전만큼 즐거움을 느끼지 못한다 (1점)</td></tr><tr><th>초조함</th><td>나는 너무 초조하고 안절부절못하여 가만히 있기가 힘들다 (2점)</td></tr><tr><th>집중하기 어려움</th><td>나는 어떤 일에도 집중할 수가 없다 (3점)</td></tr></table><br>각 문항 당 심각도에 따라 0~3점으로 채점되며 모두 합산하여 총점을 구합니다.<br>점수가 높을수록 우울 증상이 심각하다는 것을 의미합니다(점수 범위: 0 ~ 63점).<br><br><table class='text-center'><tr><th>점수</th><th>결과 해석</th></tr><tr><td class='text-center'>0~13점</td><td class='text-center'>우울하지 않은 상태</td></tr><tr><td class='text-center'>14~19점</td><td class='text-center'>가벼운 우울 상태</td></tr><tr><td class='text-center'>20~28점</td><td class='text-center'>중등도 우울 상태</td></tr><tr><td class='text-center'>29~63점</td><td class='text-center'>심한 우울 상태</td></tr></table><br>츨처: Beck, A. T., Steer, R. A., and Brown, G. K. (1996). BDI-II: Beck Depression Inventory Manual, 2nd Edn, San Antonio, TX: Psychological Corporation.<br><br>이 설문 도구는 우울 증상을 정확하고 일관성 있게 측정 가능하다고 검증되어 있어 병원, 연구원, 전문 기관 등에서 일차적인 우울증상 선별이나 평가에 널리 활용되고 있습니다<sup>154</sup>.우울증에 대한 보다 객관적이고 정확한 진단은 숙련된 전문가의 구조화된 면담을 통해 이루어지게 됩니다<sup>155</sup>.",
        "reference": "<sup>152</sup> 성형모 외., (2008). 한국어판 백 우울 설문지 2판의 신뢰도 및 타당도 연구. 생물치료정신의학, 201-212.<br><sup>153</sup> 한홍무 외., (1986). Beck Depression Inventory 의 한국판 표준화 연구-정상집단을 중심으로 (I)-. 신경정신의학, 487-502.<br><sup>154</sup> 유병관 외., (2011). 한국어판 벡 우울 설문지 2 판의 신뢰도 및 요인분석: 대학생을 대상으로. 대한생물정신의학회, 126-133.<br><sup>155</sup> Park K et al., Diagnostic Utility and Psychometric Properties of the Beck Depression Inventory-II Among Korean Adults. Front Psychol, 2934."
      }
    ],
    "3-2" : [
      {
        "title" : "3. 뇌기능 저하 자가진단 방법",
        "subtitle_bubble_a" : "업무 스트레스로 인해 겪을 수 있는 증상에 대해 스스로 확인해볼 수 있는 방법들을 소개합니다.",
        "subtitle_box" : "수면장애",
        "content": "피츠버그 수면의 질 평가척도 (Pittsburgh Sleep Quality Index, PSQI)는 수면의 질과 방해를 평가하기 위한 도구로 가장 널리 사용되고 있는 설문도구입니다<sup>156</sup>.<br>1989년 Daniel J. Buysse와 그의 동료들이 개발했으며, 한국어로 번역된 버전도 그 신뢰도와 타당도에 대한 검증을 거쳐 전문 기관에서 사용되고 있습니다<sup>157</sup>.<br><br>7가지 수면 요소들(수면의 질, 수면 지연시간, 총 수면 시간, 수면 효율, 수면 시 어려움, 약물에 대한 필요, 낮 시간의 수면)을 포함한 18개의 질문들로 이루어져 있습니다<sup>158</sup>. 이를 통해 지난 한달 동안의 수면 관련 증상을 평가하게 됩니다.<br>모든 질문은 0~3점으로 구성되어 있으며 7가지 수면 요소들에 따라 계산됩니다.<br>총점 5점 이상일 경우 수면의 질이 낮다고 평가할 수 있습니다(점수 범위: 0~21점).",
        "reference": "<sup>156</sup> 신수진 외., (2008). 불면증을 호소하는 성인의 주관적 수면 평가와 객관적 수면 평가. 대한임상건강증진학회지, 141-149.<br><sup>157</sup> Buysse, D. J et al., (1989). The Pittsburgh Sleep Quality Index: a new instrument for psychiatric practice and research. Psychiatry Res, 193-213.<br><sup>158</sup> Sohn, S. et al., (2012). The reliability and validity of the Korean version of the Pittsburgh Sleep Quality Index. Sleep Breath, 803-812."
      },
      {
        "img_top": "part2_3-1.png",
        "img_top_size": "enlarge",
        "content": "수면각성활동량 검사(Actigraphy)는 손목시계형 장치를 사용하는 검사로, 장치 내 압전가속장치가 움직임을 측정해 일상생활 동안 수면 및 활동 상태를 평가할 수 있습니다. 특히 수면-각성 주기와 수면의 질을 측정하는데 유용한 검사입니다.<br><br>이 검사는 식품의약품안전평가원에서 실시한 ‘건강기능식품 기능성 평가 가이드’에 수면 건강 기능성을 확인하기 위한 임상 평가 항목으로 등재되어 있어 객관적인 수면 지표 중의 하나로 입증되었습니다<sup>159</sup>.<br>검사 방법 또한 간단해 장기간 연속적으로 수면 및 활동 상태를 평가할 수 있고, 자연스러운 환경에서 일상적인 수면을 평가할 수 있는 장점이 있습니다<sup>160</sup>.<br>이를 통해 활동과 비활동 상태, 일주기 리듬 지표(진폭, 정점 시각 등), 수면 요소들(수면시간, 수면 효율, 총 각성 시간, 깨어 있던 시간의 비율, 수면 후 각성 횟수 등)을 산출하고 확인할 수 있습니다<sup>161</sup>.<br><br>수면각성활동량 검사에서 측정하는 요소들은 다음과 같습니다.<br><br><table><tr><th>수면 지연시간</th><td>잠을 자기 위해 침대에 누운 후 실제로 잠들기 까지 걸린 시간을 의미합니다. 지난 한 달 동안 30분 이내로 잠들지 못한 횟수가 일주일에 3번 이상일 경우 수면 문제가 있는 것으로 생각할 수 있습니다.</td></tr><tr><th>총 수면 시간</th><td>실제로 잠을 잔 시간으로 수면의 전체적인 양을 측정하는 지표입니다. 미국수면재단(National sleep foundation, NSF)에서는 광범위한 전문가의 의견을 수렴하여 성인의 수면 시간을 7-9 시간으로 권장하고 있습니다<sup>162</sup>.</td></tr><tr><th>수면 효율</th><td>수면의 질과 효율성을 객관적으로 측정하는 지표로, 잠을 자기 위해 침대에서 보낸 시간 중 실제로 잠을 잔 시간의 비율을 의미합니다. 수면 효율이 85% 이상일 때 높은 수면 효율이라고 할 수 있습니다.</td></tr><tr><th>수면 시 어려움</th><td>잠을 자던 중 잠시 깨서 화장실을 다녀오는 것과 같이, 잠이 든 이후에 한밤 중 또는 이른 아침에 깨는 것은 수면 효율과 수면의 질을 낮추는 원인이 될 수 있습니다.<br>수면 후 중간에 깨어 있는 시간이 30분 이상이고 그 횟수가 일주일에 3번 이상일 때 수면 문제가 있는 것으로 판단할 수 있습니다. 또한 숨을 편히 쉬지 못하는 것, 너무 춥거나 너무 더운 것, 기침, 코골이, 악몽, 통증 등이 수면을 방해하는 요인이 됩니다.</td></tr><tr><th>낮 시간의 수면</th><td>낮 시간에 깨어 있는 것이 어렵고 과도하게 졸음을 느껴 집중하기 어려운 것 또한 수면 문제일 수 있습니다. 지난 한 달 동안 이러한 증상이 일주일에 3번 이상인 경우 수면 개선을 위한 노력이 필요합니다.</td></tr><table>",
        "reference": "<sup>159</sup> 식품의약품안전평가원, (2017). 건강기능식품 기능성 평가 가이드 –‘수면건강에 도움을 줄 수 있음’편-<br><sup>160</sup> Sadeh, et al., (1995). The role of actigraphy in the evaluation of sleep disorders. Sleep, 288-302.<br><sup>161</sup> Ancoli-Israel, S., et al., (2003). The role of actigraphy in the study of sleep and circadian rhythms. Sleep, 342-392.<br><sup>162</sup> 미국수면재단 (National Sleep Foundation), (2020년 7월 6일). URL:<a href='https://www.sleepfoundation.org/articles/how-much-sleep-do-we-really-need'>https://www.sleepfoundation.org/articles/how-much-sleep-do-we-really-need</a>"
      },
    ],
    "3-3" : [
      {
        "title" : "3. 뇌기능 저하 자가진단 방법",
        "subtitle_bubble_a" : "업무 스트레스로 인해 겪을 수 있는 증상에 대해 스스로 확인해볼 수 있는 방법들을 소개합니다.",
        "subtitle_box" : "외상후스트레스장애(PTSD)",
        "content": "외상후스트레스장애 체크리스트 (Post-Traumatic Stress Disorder Checklist, PCL)는 외상후스트레스장애 (Posttraumatic Stress Disorder, PTSD) 증상을 측정하여 고위험군을 선별하고 임상가의 진단을 돕기 위해 활용되고 있는 자가보고형 척도입니다<sup>163</sup>.<br>측정 용도에 따라 군사용 버전(PCL-military, PCL-M), 민간인을 대상으로 하는 버전(PCL-civilian, PCL-C), 특정 외상을 대상으로 하는 버전(PCL-stressor specific, PCL-S)으로 구분되어 있습니다.<br><br>상당한 충격을 주는 일을 겪었을 때 사람들이 경험할 수 있는 다양한 문제나 불편함에 대한 17개의 목록을 제시하고 있습니다.<br>가장 큰 괴로움을 주었던 사건을 직접 기술하고 그 사건에 대해 각 문항 별 경험 횟수를 표시하게 됩니다(그렇지 않았다/조금 그랬다/어느정도 그랬다/거의 그랬다/항상 그랬다).<br><br>각 문항마다 1점(그렇지 않았다)에서 5점(항상 그랬다)까지 평가합니다.<br>점수가 높을수록 증상이 더 심각하다는 것을 의미합니다(점수 범위: 17~85점).<br><br>외상후스트레스장애 체크리스트에서 각 증상을 대표하는 예시문항은 다음과 같습니다.<br><br><table><tr><th>재경험</th><td>사건에 대한 반복적이고 고통스러운 기억, 생각, 또는 영상들이 떠오름</td></tr><tr><th>회피</th><td>사건에 대해 생각하거나, 이야기하는 것을 피하려 하거나, 사건과 관련된 감정을 느끼지 않으려고 노력함</td></tr><tr><th>생각과 감정의 부정적인 변화</th><td>감정이 무뎌지거나, 가까운 사람들을 사랑하는 감정을 느끼기가 어려움</td></tr><tr><th>과각성</th><td>신경이 예민해서 쉽게 짜증이 나거나 화를 버럭 내게 됨</td></tr></table>",
        "reference": "<sup>163</sup>Park, S. et al., (2016). Reliability and validity of the Korean version of the post-traumatic stress disorder checklist in public firefighters and rescue workers. Korean J Biol Psychiatry, 29-36.<br>"
      }
    ],
    "3-4" : [
      {
        "title" : "3. 뇌기능 저하 자가진단 방법",
        "subtitle_bubble_a" : "업무 스트레스로 인해 겪을 수 있는 증상에 대해 스스로 확인해볼 수 있는 방법들을 소개합니다.",
        "subtitle_box" : "음주습관장애",
        "content": "음주의 위험 수준에 대해서 세계보건기구(World Health Organization, WHO)에서는 술로 인해 유해한 결과가 발생할 위험이 있는 경우를 ‘위험음주’라고 규정했으며, 술로 인해 신체적 혹은 정신적 문제가 1개월 이상 지속되거나 지난 1년간 반복적으로 발생한 경우는 ‘유해음주’로 제시했습니다.<br><br>알코올 사용장애 진단 검사 (Alcohol Use Disorder Identification Test, AUDIT)는 술과 담배를 얼마나 사용하고 있는지에 대한 자가보고형 척도입니다. 위험한 음주습관을 조기에 발견하고 중재하기 위해 음주량과 음주행위, 음주 형태, 정신-사회적 문제 등을 확인할 수 있습니다<sup>164</sup>.<br><br>술을 소비하는 형태와 술에 얼마나 의존하는지, 술과 관련되어 발생할 수 있는 문제들에 대한 10개의 문항으로 구성되어 있습니다.<br>처음 3개의 문항은 위험음주자를 파악하기 위한 것으로 음주량, 음주 빈도, 폭주 빈도에 관한 내용이며, 나머지 문항은 음주 이후의 생리적 의존 증상 및 음주로 인한 유해한 문제의 발생 여부를 파악하기 위한 것입니다.<br><br>알코올 사용장애 진단 검사에서 각 증상을 대표하는 문항의 예시는 다음과 같습니다.<br><br><table><tr><th>음주 형태</th><td>술을 마시면 한 번에 몇 잔 정도 마십니까?</td></tr><tr><th>생리적 의존</th><td>지난 1년간 술을 마신 다음날, 일을 위해 해장술이 필요했던 적은 얼마나 자주 있었습니까?</td></tr><tr><th>음주로 인한 유해한 문제</th><td>음주로 인해 자신이나 다른 사람이 다친 적이 있었습니까?</td></tr></table><br>남성의 경우 10점 이상, 여성의 경우 6점 이상일 때 위험한 음주습관을 가지고 있다고 생각할 수 있습니다. <br>출처: 국립정신건강센터 (2019). 2019 정신건강검진도구 및 사용에 대한 표준 지침<br><br><table><tr><th>성별</th><th>점수</th><th>분류</th><th>결과 해석</th></tr><tr><td rowspan='3' class='text-center'>남</td><td class='text-center'>0~9</td><td class='text-center'>정상음주</td><td>건강하고 안전한 음주습관을 가짐</td></tr><td class='text-center'>10~19</td><td class='text-center'>위험음주</td><td>음주에 대한 단기적인 개입과 지속적 모니터링 필요</td></tr><td class='text-center'>20 이상</td><td class='text-center'>음주습관장애</td><td>신체증상, 불안, 불면, 사회부적응, 우울 등 증상이 나타날 수 있으므로 전문가의 치료적 개입 필요</td></tr><tr><td rowspan='3' class='text-center'>여</td><td class='text-center'>0~5</td><td class='text-center'>정상음주</td><td>건강하고 안전한 음주습관을 가짐</td></tr><td class='text-center'>6~9</td><td class='text-center'>위험음주</td><td>음주에 대한 단기적인 개입과 지속적 모니터링 필요</td></tr><td class='text-center'>10 이상</td><td class='text-center'>음주습관장애</td><td>신체증상, 불안, 불면, 사회부적응, 우울 등 증상이 나타날 수 있으므로 전문가의 치료적 개입 필요</td></tr></table><br>",
        "reference": "<sup>164</sup> 조근호 외., (2009). 위험 음주자의 선별을 위한 한국어판 Alcohol Use Disorders Identification Test (AUDIT-K) 의 최적 절단값. 중독정신의학, 34-40."
      },
    ],
    "3-5" : [
      { 
        "title" : "3. 뇌기능 저하 자가진단 방법",
        "subtitle_bubble_a" : "업무 스트레스로 인해 겪을 수 있는 증상에 대해 스스로 확인해볼 수 있는 방법들을 소개합니다.",
        "subtitle_box" : "인지기능 저하",
        "content": "주관적 기억 감퇴 설문 (Subjective Memory Complaints Questionnaire, SMCQ)은 주관적으로 기억 문제를 호소하지만 객관적인 인지검사에서는 정상범주의 수행을 나타내는 주관적 기억장애를 선별하기 위해 사용됩니다<sup>165</sup>.<br><br>주관적 기억장애는 우울증 및 스트레스와 관련되어 있다는 연구결과가 보고된 바 있으며<sup>166</sup> 외상후스트레스장애가 있는 경우 주의력, 정보처리속도 등 인지기능 결함이 나타날 수 있음을 확인한 연구결과도 있습니다. 이는 심각한 스트레스 상황에서 지속적으로 노출되는 경우 기억력 및 인지기능 저하가 나타날 수 있음을 시사합니다<sup>167</sup>.<br><br>주관적 기억장애로 나타낼 수 있는 다양한 양상들을 반영하는 14개의 질문들로 구성되어 있습니다.<br>처음 4개의 질문은 전반적인 인지 기능을 평가하기 위한 항목이고, 나머지 10개의 질문은 일상생활에서의 인지 기능을 평가하기 위해 고안되었습니다.<br><br>대표적인 예시문항은 다음과 같습니다.<br><br><table><tr><th>전반적인 인지기능 평가</th><td>당신의 기억력은 10년 전에 비해 저하되었습니까?</td></tr><tr><th>일상생활에서의 인지기능 평가</th><td>당신은 물건 둔 곳을 기억하기 어렵습니까?</td></tr></table><br>또한 주의집중력, 기억력, 실행능력 등 세부적인 인지기능을 측정하고 평가할 수 있는 검사도구들이 있습니다.<br><br>주의집중력은 다양한 자극 중에서 자신이 중요하다고 생각하는 자극을 선택적으로 인식하고 주의를 기울이는 능력을 말합니다. 또한 주의집중을 방해하는 외부 자극이 있을 때에도 집중을 유지하는 정신적 과정을 뜻합니다.<br>주의집중력을 평가할 수 있는 도구로는 글자-숫자 나열하기, 시공간폭 검사, 숫자폭 검사, 길 만들기 검사 A 등이 있습니다.<br><br>시공간폭 검사는 공간적으로 다양하게 배열된 상자를 검사도구로 사용하여, 검사자가 짚은 순서대로 또는 그 반대로 짚어나가는 검사이며, 길 만들기 검사 A는 종이에 제시된 숫자 1부터 가장 큰 수까지 순서대로 이어나가는 검사입니다.",
        "img_bottom": "part2_3-2.png",
        "img_bottom_size": "full",
      }, {
        "content": "기억력은 다양한 정보들을 저장하고 유지하며, 필요할 때 다시 불러내는 인지 과정을 의미합니다. 자극의 특성에 따라 시각적 기억력과 언어적 기억력 등이 있고, 기억의 지속 기간에 따라 단기기억과 장기기억으로 구분할 수 있습니다.<br>언어적 기억력을 측정하는 도구로 레이복합도형검사, 언어적 기억력 및 단기 기억력을 평가하는 도구로는 한국판 캘리포니아 언어학습검사가 있습니다.<br><br>레이복합도형검사는 복잡한 도형을 보여준 후 기억하여 그리도록 하는 검사이며, 한국판 캘리포니아 언어학습검사는 물건들의 목록을 불러준 후 단기기억력을 평가하고, 20분 후 물건들의 명칭을 회상하게 하여 장기기억력을 측정하는 검사입니다.",
        "img_bottom": "part2_3-3.png",
        "img_bottom_size": "full",
      }, {
        "content": "실행능력은 특정 과제를 수행하기 위해 미리 계획하고 행동을 조절하는 능력을 말하며, 미래의 특정 목표를 위해 여러 방식들을 생각해내고 이를 실행하는 정신활동을 뜻하기도 합니다.<br>검사 도구로는 스트룹검사와 길 만들기 검사 B 등이 있습니다.<br><br>스트룹 검사는 글자와 색이 일치하지 않는 단어를 제시하고, 글자 색깔을 말하도록 하는 검사이며, 길 만들기 검사 B는 종이에 제시된 숫자와 문자를 번갈아가면서 순서대로 잇는 검사입니다.",
        "img_bottom": "part2_3-4.png",
        "img_bottom_size": "full",
        "reference": "<sup>165</sup> Youn J. C. et al., (2009). Development of the subjective memory complaints questionnaire. Dement Geriatr Cogn Disord, 310-7.<br><sup>166</sup> 윤보라 외., (2008). 주관적기억장애 환자의 미래계획기억: 예비 연구. Dement Neurocogn Disord, 17-22.<br><sup>167</sup> 김선국 외., (2003). 외상후 스트레스 장애 환자의 신경인지기능. 대한생물정신의학회지, 147-58."
      }
    ],
    "4-1" : [
      {
        "title" : "4. 뇌기능 저하 평가 방법",
        "subtitle_box" : "정밀 뇌영상 검사",
        "content" : "소방공무원은 여러 화학적, 물리적, 정신적 유해인자에 반복적으로 노출됩니다. 여러 유해인자로 인해 발생할 수 있는 뇌기능 저하를 객관적으로 평가하여 그에 따라 적절한 치료를 받고 예방하는 것이 중요합니다.<br>객관적인 지표에 따른 뇌기능 평가 방법으로는 정밀 뇌영상 검사, 타액 및 혈액 검사 등이 있습니다.",
        "img_bottom" : "part2_4-1.png",
        "img_bottom_size" : "full",
        "text_add" : "<span class='img-title'>정밀 뇌영상 검사</span>"
      }, {
        "content" : "정밀 뇌영상 검사인 자기공명영상촬영(magnetic resonance imaging, MRI)은 고해상도 자기공명 영상장치를 사용하여 뇌의 구조적, 기능적 이상을 확인할 수 있는 검사입니다<sup>168</sup>.<br>자기공명영상촬영은 X-ray를 사용하는 일반 촬영이나 전산화 단층촬영(computed tomography, CT)과는 다르게 자기장(magnetic field)을 이용하는 방법입니다.<br><br>T1 강조영상(T1-weighted image)은 대표적인 구조적 뇌영상 기법입니다. 뇌의 회백질, 백질, 뇌실 등 조직 간 차이를 신호 차로 반영하여 영상을 획득합니다.<br>지방 성분이 많을수록 신호강도가 높아 백색으로 보이며 백질, 회백질 순으로 높은 신호 강도를 나타냅니다. 뇌 구조를 뚜렷하게 확인할 수 있어 해부학적 정보를 얻는 데 유용합니다.",
        "img_bottom" : "part2_4-2.png",
        "img_bottom_size" : "full",
        "text_add" : "<span class='img-title'>T1 강조영상 예시</span>"
      }, {
        "content" : "기능적 자기공명영상(functional magnetic resonance imaging, fMRI)은 뇌 영역들의 활성에 대한 정보를 얻기 위해 활용하는 영상 기법입니다. 활성화된 뇌 영역에는 혈류가 증가하기 때문에 혈류 증가에 따른 신호의 변화량을 감지하여 뇌의 기능적 영상을 얻게 됩니다.<br>휴식 상태일 때도 활성을 나타내는 뇌 영역들이 있는데, fMRI를 통해 이들의 활성화와 기능적 연결성이 소방공무원에서 외상 사건 등에 의해 어떻게 변화하고 영향을 받는지 확인할 수 있습니다<sup>169</sup>.",
        "img_bottom" : "part2_4-3.png",
        "img_bottom_size" : "full",
        "text_add" : "<span class='img-title'>기능적 자기공명영상 예시</span><span class='img-ref'>그림출처: Lyoo et al., (2011). The neurobiological role of the dorsolateral prefrontal cortex in recovery from trauma: longitudinal brain imaging study among survivors of the South Korean subway disaster. Archives of General Psychiatry, 68(7), 701-13.</span>",
      }, {
        "content" : "2019년 세계적인 전문학술지 ‘British Journal of Psychiatry’에 실린 연구에서는 정서, 인지, 동기부여와 관련된 외부 감각신호를 통합하고 처리하는데 관여한다고 알려진 섬엽(insula)과 공포 반응과 관련된 편도체(amygdala) 사이의 기능적 연결성이 뇌기능 저하에 따른 외상후스트레스장애의 위험을 높이는 뇌 회로로 작용할 수 있다는 점을 제시했습니다.<br>반면, 섬엽(insula)과 외상 사건의 부정적인 감정을 완화시키는 능력에 관여하는 복내측 전전두엽(ventromedial prefrontal cortex) 사이의 기능적 연결성은 외상 사건 경험 이후 회복을 돕는 뇌 회로로서 작용할 수 있다는 점도 밝혔습니다.<br>이는 외상사건과 유해인자에 반복적으로 노출되는 소방공무원에서 정밀 뇌영상 검사를 통해 뇌기능 저하에 의한 외상후스트레스장애 발병과 회복탄력성에 관련된 뇌 회로 지표를 확인했다는 점에서 의미가 있습니다<sup>170</sup>.",
        "img_bottom" : "part2_4-4.png",
        "img_bottom_size" : "full",
        "text_add" : "<span class='img-ref'>그림출처: Jeong H. et al., (2019). Altered functional connectivity in the fear network of firefighters with repeated traumatic stress. The British Journal of Psychiatry, 347-353.</span>",
        "reference" : "<sup>168</sup> 강충환 외, (2014). 자기공명영상학, 도서출판 대학서림<br><sup>169</sup> 대한자기공명의과학회, (2008). 자기공명영상학, ㈜일조각<br><sup>170</sup> Jeong H. et al., (2019). Altered functional connectivity in the fear network of firefighters with repeated traumatic stress. The British Journal of Psychiatry, 347-53."
      }
    ],
    "4-2" : [
      {
        "title" : "4. 뇌기능 저하 평가 방법",
        "subtitle_box" : "혈액 검사",
        "content" : "바이오마커란 정상적인 생물학적 과정이나 병리적 과정 또는 치료적 개입에 대한 약물학적 반응을 객관적으로 측정하고 평가할 수 있는 지표를 의미합니다.<br>바이오마커를 통해 특정 질환에 취약성을 예측하거나 질환을 객관적으로 진단하는데 적용할 수 있습니다.<br><br>예를 들면, 뇌유래영양인자(brain-derived neurotrophic factor, BDNF)는 대표적인 혈액 바이오마커 중 하나로, 우리 뇌를 구성하고 있는 신경세포의 증식과 생존, 분화를 촉진하는 역할을 합니다.<br>혈장 내 BNDF 수준은 외상후스트레스장애 증상의 심각도와 관련이 있다고 알려져 있습니다.<br>또다른 혈액 바이오마커인 high sensitivity C-reactive protein (hsCRP)는 면역반응물질로, 혈액 내의 hsCRP 농도는 외상후스트레스장애 발병의 위험을 예측하는 염증지표로서 널리 연구되고 있습니다<sup>171</sup>.",
        "img_top" : "part2_4-5.png",
        "img_top_size" : "full",
      }, {
        "content" : "염증지표와 뇌기능 저하 증상의 연관성을 확인한 연구에서는 스트레스 반응에 의한 임상적인 현상이 나타나기 전에 만성 염증상태를 반영하는 저준위 염증이 있는 소방공무원에서 고위인지기능 네트워크의 약화 및 외상후증후군 증상과 관련이 있다는 점을 보고하였습니다.<br>해당 결과는 뇌기능 저하 고위험군 및 증상 발병을 예측할 수 있는 염증지표 후보를 발굴했다는 의의가 있습니다<sup>172</sup>.",
        "img_bottom" : "part2_4-6.png",
        "img_bottom_size" : "full",
        "text_add" : "<span class='img-ref'>그림출처: Kim J. et al., (2020). A double-hit of stress and low-grade inflammation on functional brain network mediates posttraumatic stress symptoms. Nature communications, 1-12.</span>",
        "reference" : "<sup>171</sup> Kang H. J. et al., (2015). Peripheral biomarker candidates of posttraumatic stress disorder. Experimental neurobiology, 186-196.<br><sup>172</sup> Kim J. et al., (2020). A double-hit of stress and low-grade inflammation on functional brain network mediates posttraumatic stress symptoms. Nature communications, 1-12."
      }
    ],
    "4-3" : [
      {
        "title" : "4. 뇌기능 저하 평가 방법",
        "subtitle_box" : "타액 코티솔 바이오키트",
        "content" : "흔히 스트레스 호르몬이라고 알려진 코티솔(cortisol)은 단기적인 스트레스 상황에서 분비되는 부신피질 호르몬으로, 다양한 반응을 통해 신체의 스트레스 반응을 적절히 조절합니다.<br>그러나 외상 사건과 유해인자에 반복적으로 노출되는 장기적인 스트레스 상황에서는 코티솔 농도가 과도하게 증가할 수 있습니다. 높은 코티솔 농도는 우울증이나 외상후스트레스장애 발병의 위험과 관련이 있는 것으로 잘 알려져 있습니다<sup>173</sup>.<br>",
        "img_top" : "part2_4-7.png",
        "img_top_size" : "full",
      }, {
        "content" : "이에 따라 업무 상황에서 발생할 수 있는 부정적인 영향을 현장에서 신속히 평가할 수 있도록 타액 내 코티솔 지표를 정확하고 일관성 있게 측정하는 휴대용 바이오키트가 개발되어 있습니다.<br>해당 바이오키트는 코티솔에만 특이적으로 반응하는 분석용 검출기를 통해 높은 감도로 저분자물질인 코티솔을 검출할 수 있습니다.<br>특히, 타액에서 코티솔을 검출하는 방식이므로 채혈 등 번거로운 과정이 필요하지 않습니다. 신호 분석이 용이하며, 결과를 바로 출력해 확인할 수 있어 소방현장에서도 쉽게 사용할 수 있다는 장점이 있습니다.",
        "img_bottom" : "part2_4-8.png",
        "img_bottom_size" : "full",
        "reference" : "<sup>173</sup> McFarlane, A. C. et al., (2011). Cortisol response to acute trauma and risk of posttraumatic stress disorder. Psychoneuroendocrinology, 720-7."
      }
    ],
    "5" : [
      {
        "title" : "인공지능 모델을 통한 뇌기능 저하 평가 방법",
        "subtitle_bubble_q" : "뇌기능 저하가 정말 유해인자 때문인지 어떻게 알 수 있나요?",
        "content" : "소방공무원은 일반 사람들과 달리 생물학적 차이, 노화, 과거 경험, 음주 및 흡연 등에 더하여 추가적인 요인으로 업무 중 노출되는 유해인자에 의해 뇌기능 저하를 경험할 수 있습니다. 따라서 유해인자로 인해 특이적으로 발생할 수 있는 뇌 변화 및 뇌기능 저하 증상을 그 외 원인들에 의한 현상과 감별하는 것은 소방공무원 맞춤형 뇌기능 저하 예방법과 치료법 개발을 위해 중요하며, 더 나아가 공상처리 등의 근거자료로도 사용될 수 있습니다.<br><br>소방공무원의 뇌기능 저하는 유해인자와 개인별 생물학적 차이, 심리사회적 차이, 생활습관 차이가 복합되어 나타날 수 있습니다. 따라서 다른 요인과 구분하여 유해인자만의 영향을 파악하기 위해서는 주기적으로 뇌 변화를 관찰하고 뇌기능 저하를 평가한 자료가 축적되어야 합니다.<br>특히 화재진압, 구급, 구조 등 업무별 유해인자 차이 및 노출기간, 강도에 따른 작용 차이를 확인하기 위해서는 정기적인 검진 자료가 바탕 되어야 합니다.<br>충분한 자료가 구축되어 있을 될 때 유해인자 특이적인 뇌 변화 및 뇌기능 저하 감별 모델은 빅데이터 분석 기법을 통해 개발될 수 있습니다.<br><br>빅데이터 분석이란 다양한 형태의 대용량 자료를 분석하여 문제 해결, 의사결정 등에 필요한 가치 있는 정보를 추출하는 과정입니다<sup>174</sup>. 복잡한 현상을 통찰하고 이해하는 빅데이터 분석 기법의 예로는 군집분석, 연관관계 분석 등이 있습니다.<br><br>군집분석(clustering)은 유사성을 기반으로 비슷한 특성을 가진 개체들끼리 묶어 몇 개의 군집으로 나누는 방법입니다. 군집분석을 통해 분류된 각 집단의 특성을 파악하고, 각 집단의 차이와 관련성을 파악할 수 있습니다<sup>175, 176</sup>.<br><br>연관관계 분석(association analysis)은 대용량 데이터 사이에 숨어있는 관계 및 규칙을 탐색하는 방법입니다. 연관관계 분석의 활용으로 특정 상품을 구입한 사람들이 동시에 구매할 수 있는 다른 상품을 추천하는 것을 예로 들 수 있습니다<sup>177</sup>.<br>해당 방법을 활용하여 소방공무원과 일반 사람들에게서 나타나는 뇌 변화 및 뇌기능 저하 증상을 몇 개의 군집으로 나누어 비교함으로써 각 집단의 특성과 요인 간 관련성을 파악할 수 있습니다. 이를 통해 개인별 유해인자 노출 차이와 생물학적 차이, 생활습관 차이 등 각 요인이 뇌 변화 및 뇌기능 저하에 미치는 영향을 파악하고 구분하는데 기여할 수 있습니다.<br><br>예측 모델 구축을 위한 기법의 예로는 기계학습 및 딥러닝 기법이 있습니다. 기계학습을 통한 예측 모델 구축은 학습과 검증 단계가 필요하며 정확도 높은 알고리즘을 구현하기 위해서는 대규모의 학습 데이터가 필요합니다<sup>178</sup>.<br>학습 방법에는 지도 학습, 비지도 학습, 강화 학습 등이 있으며<sup>179</sup>, 데이터 수가 많을수록 정교한 모델을 구축하고 알고리즘을 일반화하여 새로운 데이터에 대한 예측 정확도를 높일 수 있습니다<sup>180</sup>.<br>유해인자 노출에 따른 소방공무원의 뇌 변화를 반영하는 뇌영상 데이터를 사용하여 유해인자로 인해 어떠한 뇌기능 저하 증상이 나타날 수 있는지 예측할 수 있습니다. 이를 통해 뇌기능 저하를 조기에 예방하고 뇌 건강을 관리할 수 있습니다.",
        "img_bottom" : "part2_4-9.jpg",
        "img_bottom_size" : "mid",
        "reference" : '<sup>174</sup> Hu et al., (2014). Towards scalable systems for big data analytics: a technology tutorial. IEEE Access, 652–87<br><sup>175</sup> Diday et al., (1976). "Clustering analysis" Digital pattern recognition. Springer<br><sup>176</sup> Hu et al., (2014). Toward Scalable Systems for Big Data Analytics: A Technology Tutorial. IEEE access, 652-87<br><sup>177</sup> Kaur et al., (2016). Market Basket Analysis: Identify the changing trends of market data using association rule mining. Procedia comput science, 2016, 78-85<br><sup>178</sup> Zhou et al., (2017). Machine learning on big data: opportunities and challenges. Neurocomputing, 350-61<br><sup>179</sup> Jordan et al., (2015). Machine learning: trends, perspectives, and prospects. Science, 255-60<br><sup>180</sup> Francois et al., (2017). Deep learning with Python. Manning publications company'
      }
    ]
  } 

  constructor(
    private elRef: ElementRef,
    public renderer: Renderer,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController,
    private iab: InAppBrowser) {
    this.selectedContent = this.navParams.get('content');
    this.subcontent = this.navParams.get('subcontent');
  }

  ionViewDidLoad() {
    // json 중 link 연결이 필요한 내용이 첨부되어있을 경우 listen, 클릭 이벤트 발생시 처리
    if (this.elRef.nativeElement.querySelectorAll('a')) {
      let links = this.elRef.nativeElement.querySelectorAll('a');
      for (let link of links) {
        this.renderer.listen(link, 'click', (evt) => {
          this.goLink(link.getAttribute('href'));
          return false;
        });
      }
    }
  }

  openEnlarge(src) {
    let enlargeModal = this.modalCtrl.create( EnlargeModalPage, { src: src });
    enlargeModal.onDidDismiss(data => {
      console.log(data);
    });
    enlargeModal.present();
  }

  goLink(url) {
    if (!url) return;
    this.iab.create(url);
  }

  goList(){
    this.navCtrl.push(this.contentPart2, {}, { animate: true, direction: 'back' })
  }

  goRoot() {
    this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
  }

}
