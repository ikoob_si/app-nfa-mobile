import { Component, OnDestroy, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Platform, Navbar, Content, Keyboard } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
import { HomePage } from '../../home/home';
import { AnswerProvider } from '../../../providers/answer/answer';
import { SuccessPage } from '../../success/success';

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage implements OnDestroy {
  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild(Content) content: Content;

  // * 설문 파일 리스트
  public survey: any;
  public subject = 0;
  public page = 0;

  public alert: any;

  public subjects: any[] = [];
  public answers: any = {};

  public unregisterBackButtonAction: any;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private navParams: NavParams,
    private platform: Platform,
    public api: ApiProvider,
    private awp: AnswerProvider,
    public keyboard: Keyboard
  ) {
    if (this.navParams.data) {
      this.survey = navParams.data;
    }
  }

  ngOnDestroy() {
    if (this.alert) this.alert.dismiss();
    if(this.unregisterBackButtonAction) this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    this.getSubject(this.subject, this.page);

    // * 안드로이드에서 백버튼 클릭 시
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      if (this.survey.id == 1) return;
      this.leaveSurvey();
    }, 2);

    // * 백버튼 클릭 시
    this.navBar.backButtonClick = () => {
      this.leaveSurvey();
    }
  }

  // * 설문을 벗어나고자 할 때
  public leaveSurvey() {
    if(this.alert){
      this.alert.dismiss();
      this.alert = null;
    } else {
      this.alert = this.alertCtrl.create({
        title: '확인',
        subTitle: '설문을 종료하시겠습니까?',
        buttons: [
          {
            text: '취소',
            role: 'cancel',
            cssClass: 'cancel-btn'
          },
          {
            text: '확인',
            cssClass: 'confirm-btn',
            handler: () => {
              this.awp.removeAllAnswers().subscribe(() => {
                this.navCtrl.setRoot(HomePage, {}, { animate: true, direction: 'back' });
              })
            }
          }
        ]
      })
      return this.alert.present();
    }
  }

  // * 설문 불러오기
  public getSubject(subject: number, page: number) {
    this.page = page; // 선택된 스탭 저장
    this.subject = subject; // 선택된 설문 번호 저장
    if (this.subjects[this.subject]) return;  // 가져온 설문이 이미 있을 경우 호출하지 않음
    this.awp.getSubJect(this.survey.id, subject).subscribe((resp: any) => {
      this.subjects[this.subject] = resp; // 설문 저장
    });
  }

  // * 이전 스탭으로 이동
  public prevStep() {
    if (this.page === 0 && this.subject === 0) {
      // * 첫 스탭일 경우
      this.leaveSurvey();
    } else {
      if (this.page === 0) {
        this.content.scrollToTop();
        this.getSubject(this.subject - 1, this.subjects[this.subject - 1].pages.length - 1);
      } else {
        this.content.scrollToTop();
        this.page--;
      }
    }
  }

  // * 다음 스탭으로 이동
  public nextStep() {
    let surveys = this.subjects[this.subject].pages[this.page].surveys;
    // * 유효성 검사 실패
    let invalid = surveys.find(survey => {
      return !survey.valid;
    });
    if (invalid) { // * 유효성 검사 실패
      let alert = this.alertCtrl.create({
        title: '확인',
        subTitle: '답변이 입력되지 않은 설문이 있습니다.<br>설문에 답변해주세요.',
        buttons: [{ text: '확인', cssClass: 'confirm-btn' }]
      })
      return alert.present();
    } else {
      // * 현재 스탭이 현 설문의 마지막 스탭일 경우 다음 설문으로 이동
      if (this.page === this.subjects[this.subject].pages.length - 1) {

        if (this.subject === (this.survey.subjects.length - 1)) {
          // * 마지막 설문일 경우 설문 저장 후 완료 페이지로 이동
          this.sendAnswers();
        } else {
          this.content.scrollToTop();
          this.getSubject(this.subject + 1, 0);
        }
      } else {
        this.content.scrollToTop();
        this.page++;
      }
    }
  }

  // * 설문 종료 / 답변 저장하기
  public sendAnswers() {
    this.awp.getSurveyResult().subscribe((response) => {
      let params: any = {
        surveyType: this.survey.id.toString(),
        answers: this.awp.answers
      }
      if (response.lastId) params.lastId = response.lastId;
      this.awp.postAnswers(params).subscribe((response) => {
        console.log('PostAnswers Result = ', response)
        this.navCtrl.setRoot(SuccessPage, {}, { animate: true, direction: 'forward' });
      })
    })
  }

}
