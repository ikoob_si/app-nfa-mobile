import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { SurveyPage } from './survey/survey';

@Component({
  selector: 'page-surveys',
  templateUrl: 'surveys.html',
})
export class SurveysPage {

  /**
   * * surveyType
   * 초기평가: 1
   * 외상사건평가: 2
   * 추적관찰평가: 3
   */
  public surveyList: any = [
    {
      subjects: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      title: '초기평가',
      id: 1
    },
    {
      subjects: [1, 2, 3, 4],
      title: '외상사건',
      id: 2
    },
    {
      subjects: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
      title: '추적관찰',
      id: 3
    }
  ]

  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
  }

  // 설문 진행하기
  goSurvey(id: number) {
    let result = this.surveyList.find(item => item.id === id);
    if (result) {
      this.navCtrl.push(SurveyPage, result);
    } else {
      this.alertCtrl.create({
        title: '확인',
        message: '설문 아이디를 확인해주세요.',
        buttons: ['확인']
      }).present();
    }
  }

}
