import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-enlarge-modal',
  templateUrl: 'enlarge-modal.html',
})
export class EnlargeModalPage {

  src: string;

  constructor( public navParams: NavParams, public viewCtrl: ViewController) {
    console.log(navParams);
    this.src = navParams.data.src
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
