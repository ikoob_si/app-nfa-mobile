import { NgModule } from '@angular/core';
import { SurveyTextarea1Component } from './survey-textarea1/survey-textarea1';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../pipes/pipes.module';
import { DirectivesModule } from '../directives/directives.module';
import { SurveyCheck1Component } from './survey-check1/survey-check1';
import { SurveyDate1Component } from './survey-date1/survey-date1';
import { SurveyInput1Component } from './survey-input1/survey-input1';
import { SurveyInput2Component } from './survey-input2/survey-input2';
import { SurveyInput3Component } from './survey-input3/survey-input3';
import { SurveyInput4Component } from './survey-input4/survey-input4';
import { SurveyRadio1Component } from './survey-radio1/survey-radio1';
import { SurveyRadio2Component } from './survey-radio2/survey-radio2';
import { SurveyMore1Component } from './survey-more1/survey-more1';
import { SurveyMore2Component } from './survey-more2/survey-more2';
import { SurveyMore3Component } from './survey-more3/survey-more3';
import { SurveyMore4Component } from './survey-more4/survey-more4';
import { SurveyRange1Component } from './survey-range1/survey-range1';

@NgModule({
	declarations: [
		SurveyCheck1Component,
		SurveyDate1Component,
		SurveyInput1Component,
		SurveyInput2Component,
		SurveyInput3Component,
		SurveyInput4Component,
		SurveyTextarea1Component,
		SurveyRadio1Component,
		SurveyRadio2Component,
		SurveyMore1Component,
    	SurveyMore4Component,
		SurveyMore2Component,
		SurveyMore3Component,
    	SurveyRange1Component
	],
	imports: [
		IonicModule,
		CommonModule,
		PipesModule,
		DirectivesModule
	],
	exports: [
		SurveyCheck1Component,
		SurveyDate1Component,
		SurveyInput1Component,
		SurveyInput2Component,
		SurveyInput3Component,
		SurveyInput4Component,
		SurveyTextarea1Component,
		SurveyRadio1Component,
		SurveyRadio2Component,
		SurveyMore1Component,
    	SurveyMore4Component,
		SurveyMore2Component,
		SurveyMore3Component,
    	SurveyRange1Component
	]
})
export class ComponentsModule { }
