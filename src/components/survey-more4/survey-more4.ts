import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerProvider } from '../../providers/answer/answer';

@Component({
  selector: 'survey-more4',
  templateUrl: 'survey-more4.html'
})
export class SurveyMore4Component implements OnInit {
  @Output('changeFn') changeFn: EventEmitter<any> = new EventEmitter();
  @Input('survey') survey: any;

  activation: boolean = false;

  constructor(private awp: AnswerProvider) { }

  ngOnInit() {
    if (this.survey.valid === undefined) this.survey.valid = false;
  }

  public invalidCheck() {
    this.survey.valid = true;
    return Observable.create(observer => {
      let result1 = this.survey.conditionList.find(item => !item.value);
      if (result1) this.survey.valid = false;
      if (!result1 && this.activation) {
        let result2 = this.survey.group.find(item => !item.value);
        if (result2) this.survey.valid = false;
      }
      observer.next();
    });
  }

  // * 설문의 합이 0 이상일 때 활성화 되는 Group 문제
  setConditions() {
    let result = 0;
    let answers = {};
    this.survey.conditionList.forEach((answer) => {
      answers[answer.id] = answer.value;
      result += Number(answer.value);
    });
    this.activation = result > 0;
    if (!this.activation) this.awp.removeAnswers(this.survey.group);
    this.invalidCheck().subscribe(() => {
      this.awp.setAnswers(answers);
      this.changeFn.emit();
    });
  }

  setValue() {
    this.invalidCheck().subscribe(() => {
      this.changeFn.emit();
    });
  }

}
