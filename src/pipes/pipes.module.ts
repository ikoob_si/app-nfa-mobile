import { NgModule } from '@angular/core';
import { SafeHtmlPipe } from './safe-html/safe-html';
import { BirthdayPipe } from './birthday/birthday';
import { PhoneNumPipe } from './phone-num/phone-num';
@NgModule({
	declarations: [SafeHtmlPipe,
    BirthdayPipe,
    PhoneNumPipe],
	imports: [],
	exports: [SafeHtmlPipe,
    BirthdayPipe,
    PhoneNumPipe]
})
export class PipesModule {}
