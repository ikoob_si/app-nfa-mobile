import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'birthday',
})
export class BirthdayPipe implements PipeTransform {
  transform(value: string, ...args) {
    return value.replace(/([0-9]{4})([0-9]{2})([0-9]{2})/, '$1-$2-$3');
  }
}
