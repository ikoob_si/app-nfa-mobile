import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phoneNum',
})
export class PhoneNumPipe implements PipeTransform {
  transform(value: string, ...args) {
    return value.replace(/(^02.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/, '$1-$2-$3');
  }
}
 