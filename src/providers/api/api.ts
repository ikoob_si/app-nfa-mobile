import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiProvider {

  BASE_URL = 'http://nfa-api.cade.education:8041';

  constructor(public http: HttpClient) { }

  public getStorageUser() {
    let user: any = '';
    if (localStorage.getItem('nfa')) user = JSON.parse(localStorage.getItem('nfa'));
    else if (sessionStorage.getItem('nfa')) user = JSON.parse(sessionStorage.getItem('nfa'));
    return user;
  }

  public getToken() {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    if (this.getStorageUser().token) {
      headers = headers.append('Authorization', 'Bearer ' + this.getStorageUser().token)
    }
    return { headers: headers }
  }

  get(endpoint: string): Observable<any> {
    let opt = this.getToken();
    return this.http.get(this.BASE_URL + endpoint, opt)
      .pipe(catchError(this.handleError));
  }

  post(endpoint: string, body?: any): Observable<any> {
    let opt = this.getToken();
    return this.http.post(this.BASE_URL + endpoint, body, opt)
      .pipe(catchError(this.handleError));
  }

  put(endpoint: string, body?: any): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + this.getStorageUser().token
    });
    let options = { headers: headers };
    return this.http.put(this.BASE_URL + endpoint, body, options)
      .pipe(catchError(this.handleError));
  }

  delete(endpoint: string): Observable<any> {
    let opt = this.getToken();
    return this.http.delete(this.BASE_URL + endpoint, opt)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return Observable.throw(error);
  }


}
