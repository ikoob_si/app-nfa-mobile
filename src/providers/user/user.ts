import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiProvider } from '../api/api';

@Injectable()
export class UserProvider {

  constructor(private api: ApiProvider) { }

  public login(params: { email: string, password: string }): Observable<any> {
    let endpoint: string = '/api/v1/login/patient';
    return this.api.post(endpoint, params);
  }

  public logout(): Observable<any> {
    return Observable.create(observer => {
      if (localStorage.getItem('nfa')) localStorage.removeItem('nfa');
      if (sessionStorage.getItem('nfa')) sessionStorage.removeItem('nfa');
      observer.next();
    });
  }

  public join(params: { email: string, password: string, fullName: string, birthday: string, gender: string, phone: string }) {
    let endpoint: string = '/api/v1/patients';
    return this.api.post(endpoint, params);
  }

  public sendEmail(params: { target: string }) { 
    let endpoint = '/api/v1/find/code/email';
    return this.api.post(endpoint, params);
  }

  public sendTelNumber(params: { patientId: string, phone: string }) {
    let endpoint = '/api/v1/find/tel';
    return this.api.post(endpoint, params);
  }

  public findId(params: { fullName: string, birthday: string, gender: string }) {
    let endpoint = '/api/v1/find/email';
    return this.api.post(endpoint, params);
  }

  public resetPw(params: { email: string, phone: string }) {
    let endpoint = '/api/v1/find/password';
    return this.api.post(endpoint, params);
  }

  public getUserInfo(patientId: string) {
    let endpoint = `/api/v1/patients/${patientId}`;
    return this.api.get(endpoint);
  }

  public setUserInfo(params: { fullName: string, gender: string, birthday: string,  phone: string }) {
    let patient = this.api.getStorageUser();
    let endpoint = `/api/v1/patients/${patient.id}`;
    return this.api.put(endpoint, params);
  }

  public setPw(params: { oldPassword: string, password: string }) {
    let patient = this.api.getStorageUser();
    let endpoint = `/api/v1/patients/${patient.id}/password`;
    return this.api.put(endpoint, params);
  }

  public signout(params: { password: string }) {
    let patient = this.api.getStorageUser();
    let endpoint = `/api/v1/patients/${patient.id}/delete`;
    return this.api.put(endpoint, params);
  }

}
