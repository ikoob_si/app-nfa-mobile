import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ApiProvider } from '../api/api';


@Injectable()
export class AnswerProvider {

  public answers: any = {}

  constructor(public http: HttpClient, private api: ApiProvider) { }

  /**
   * * 설문 불러오기
   * @param survey 
   * @param subject 
   */
  public getSubJect(survey: number, subject: number): Observable<any> {
    return this.http.get(`assets/jsons/survey${survey}/subject${subject}.json`)
      .pipe(catchError(this.handleError));
  }

  // * 설문 답변 가져오기
  public getAnswers(): Observable<any> {
    return Observable.create(observer => {
      observer.next(this.answers);
    });
  }

  // * 설문 변경에 따라 실시간 변경됨.
  public setAnswers(answers: any) {
    Object.keys(answers).forEach(key => {
      if (answers[key] && answers[key].length) {
        this.answers[key] = answers[key]
      } else {
        delete this.answers[key];
      }
    });
    console.log('answers = ', this.answers);
  }

  // * 저장된 설문 답변 초기화
  public removeAnswers(surveys) {
    surveys.forEach(survey => {
      switch (survey.type) {
        case 'input4':
          survey.list.forEach(item => {
            if (item.list) this.removeAnswers(survey.list);
          });
          break;
        case 'radio2':
          survey.active = 0;
          break;
        case 'more1':
          survey.list.forEach(item => {
            if (!item.surveys) return;
            this.removeAnswers(item.surveys);
          });
          break;
        case 'more2':
          if (survey.lists) {
            survey.lists.forEach(list => {
              if (list) this.removeAnswers(list);
            });
            survey.lists = [];
          }
          break;
      }
      delete this.answers[survey.id];
      delete this.answers[`${survey.id}_t`];
      delete survey.value;
      delete survey.checked;
      delete survey.selected;
      if (survey.list) this.removeAnswers(survey.list);
    });
  }

  // * 설문 모든 답변 삭제하기
  public removeAllAnswers(): Observable<any> {
    return Observable.create(observer => {
      this.answers = {};
      observer.next();
    });
  }

  // * Result값 가져오기 ( 1: 초기평가 필요, 2: 외상평가 필요, 3: 일반 )
  public getSurveyResult() {
    let patient = this.api.getStorageUser();
    let endpoint = `/api/v1/patient/${patient.id}/result`;
    return this.api.get(endpoint);
  }

  /**
   * * 설문 답변 저장하기
   * * 초기설문은 가입 후 반드시 진행하여야한다. lastId 불필요
   * * 키오스크 설문 완료시 외상사건 경험 유무(trauma1)가 있을 경우 외상사건 평가를 반드시 진행해야한다. lastId 반드시 필요!!
   * * GET /api/v1/patient/{patientId}/result 
   * * result 가 3일 경우에는 lastId가 불필요
   * @param surveyType
   * @param lastId
   */
  public postAnswers(params: any): Observable<any> {
    let patient = this.api.getStorageUser();
    let endpoint = `/api/v1/patient/${patient.id}/result`;
    return this.api.post(endpoint, params);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return Observable.throw(error);
  }
}
