![소방관 지킴이](src/assets/imgs/logo.png)


# 소방관 지킴이

![ionic](https://img.shields.io/badge/Ionic-3.9.9-%23FFB789.svg)
![Version](https://img.shields.io/badge/AppVersion-v0.0.1-%23FFEDAF.svg)


소방공무원 뇌기능 회복 매뉴얼 제공을 위한 모바일 어플리케이션 및 건강관리 시스템.

책자로 제작하는 소방공무원 뇌기능 회복 매뉴얼 제공하여 언제 어디서나 시간과 장소의 제한에 상관없이 원하는 정보를 쉽게 찾을 수 있으며 장소와 시간 제약에 구애받지 않고 메뉴얼의 정보를 열람할 수 있는 시스템.


## 목차

1. [개발환경](#개발환경)
2. [Getting Started](#getting-started)
3. [Device Started](#device-started)
4. [Android Build](#android-build)
5. [Project structure](#project-structure)
6. [Error](#error)
7. [License](#license)


## 개발환경

```
Ionic:

   Ionic CLI          : 6.11.8
   Ionic Framework    : ionic-angular 3.9.9
   @ionic/app-scripts : 3.2.4

Cordova:

   Cordova CLI       : 9.0.0 (cordova-lib@9.0.1)
   Cordova Platforms : android 8.1.0
   Cordova Plugins   : cordova-plugin-ionic-keyboard 2.2.0, cordova-plugin-ionic-webview 4.2.1, (and 5 other plugins)

Utility:

   cordova-res (update available: 0.15.1) : 0.8.1
   native-run (update available: 1.1.0)   : 0.3.0

System:

   Android SDK Tools : 25.2.3
   ios-deploy        : 1.10.0
   ios-sim           : ios-sim/9.0.0 darwin-x64 node-v12.16.1
   NodeJS            : v12.16.1
   npm               : 6.14.4
```


## Getting Started


- Download the installer for [Node.js](https://nodejs.org/) 6 or greater.
- Install the ionic CLI & cordova globally: ```npm install -g ionic cordova```

```bash
$ npm i
$ ionic serve

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. 
The app will automatically reload if you change any of the source files.
```


### Device Started

Android
```
$ ionic cordova run android --device --livereload -c --address=0.0.0.0
```

## Android Build

### 1. Splash & icon 설정
~~~
$ ionic cordova resources
~~~

### 2. 빌드
~~~
$ ionic cordova build android --prod --release
~~~

### 3. 키스토어가 없을 경우 아래 명령어로 생성
~~~
keytool -genkey -v -keystore nfa.jks -alias ikoob -keyalg RSA -keysize 2048 -validity 10000
~~~

### 4. 서명되지 않은 APK에 서명하기
~~~
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore nfa.jks ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ikoob
~~~

### 5. apk 파일 추출

[참조](https://ionicframework.com/docs/v3/intro/deploying/)
~~~
/Users/eunhyeko/Library/Android/sdk/build-tools/29.0.2/zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk nfa.apk
/Users/koober/Library/Android/sdk/build-tools/29.0.2/zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk nfa.apk
~~~


## Project structure
```bash
src
├── app                             #
│   ├── app.component.ts            #
│   ├── app.html                    #
│   ├── app.module.ts               # 사용중인 모듈 및 페이지 관리
│   ├── app.scss                    # Custom Style (Common)
│   └── main.ts                     #
├── assets                          #
│   ├── icon                        # 아이콘 이미지
│   ├── imgs                        #
│   │   ├── part1                   # Part1 이미지
│   │   ├── part2                   # Part2 이미지
│   │   └── part3                   # Part3 이미지
│   └── jsons                       #
│       ├── subject_sample.json     # Survey Type(Component)별 Json 형식 Sample
│       ├── nfa_survey_codes.json   # 문항 코드
│       ├── survey1                 # Survey1(초기평가) Subject별 Json
│       ├── survey2                 # Survey2(외상사건) Subject별 Json
│       └── survey3                 # Survey3(추적관찰) Subject별 Json
├── components                      #
│   ├── pinch-zoom                  # 이미지 Pinch-zoom 라이브러리
│   ├── survey-check1               # Survey Check Type 1 // 설문 타입별 설명은 각 컴포넌트의 ts파일 내에 있음
│   ├── survey-date1                # Survey Date Type 1
│   ├── survey-input1               # Survey Input Type 1
│   ├── survey-input2               # Survey Input Type 2
│   ├── survey-input3               # Survey Input Type 3
│   ├── survey-input4               # Survey Input Type 4
│   ├── survey-more1                # Survey More Type 1
│   ├── survey-more2                # Survey More Type 2
│   ├── survey-more3                # Survey More Type 3
│   ├── survey-more4                # Survey More Type 4
│   ├── survey-radio1               # Survey Radio Type 1
│   ├── survey-radio2               # Survey Radio Type 2
│   ├── survey-range1               # Survey Range Type 1
│   └── survey-textarea1            # Survey Textarea Type 1
├── directives                      # 
│   └── for-id                      # label을 button화 하여 클릭했을 때 input에 focus하는 directive 
├── index.html                      # 
├── manifest.json                   # 
├── pages                           # 
│   ├── account                     # 
│   │   ├── find                    # 아이디/비밀번호 찾기
│   │   ├── join                    # 회원가입 기본 정보 입력
│   │   │   ├── email-cert          # 이메일 인증
│   │   │   ├── join-info           # 회원가입 안내
│   │   │   ├── join-policy         # 이용약관 동의
│   │   │   └── join-success        # 회원가입 완료 
│   │   └── login                   # 로그인
│   ├── contents                    # 
│   │   ├── part1                   # 매뉴얼(Part1/유해인자) 버튼 리스트 
│   │   │   └── part1-detail        # 매뉴얼(Part1/유해인자) 상세 내용
│   │   ├── part2                   # 매뉴얼(Part2/뇌기능저하) 버튼 리스트 
│   │   │   └── part2-detail        # 매뉴얼(Part2/뇌기능저하) 상세 내용
│   │   └── part3                   # 매뉴얼(Part3/치료및예방) 버튼 리스트 
│   │       └── part3-detail        # 매뉴얼(Part3/치료및예방) 상세 내용
│   ├── enlarge-modal               # 이미지 확대 모달
│   ├── home                        # 홈 화면
│   ├── settings                    # 설정 메뉴 목록
│   │   ├── info                    # 회원정보
│   │   ├── info-update             # 회원정보 수정
│   │   ├── personal-code           # 개인식별코드
│   │   └── sign-out                # 회원 탈퇴
│   ├── success                     # 설문 완료 안내
│   └── surveys                     # 설문 메뉴 목록
│       └── survey                  # 설문하기
├── pipes                           # 
│   ├── birthday                    # 생일 형식 pipe
│   ├── phone-num                   # 휴대폰 번호 형식 pipe
│   └── safe-html                   # JSON text의 스타일 적용 pipe
├── providers                       # 
│   ├── answer                      # 설문 서비스
│   └── api                         # 서버 연동 서비스
└── theme                           # Theme, Global Style, Ionic Style Initialization
```

---
## Error

### Splash & Icon 설정시
```
$ ionic cordova resources
$ npm install -g cordova-res

npm WARN checkPermissions Missing write access to /usr/local/lib/node_modules
npm ERR! path /usr/local/lib/node_modules
npm ERR! code EACCES
npm ERR! errno -13
.
.
.
```
아래 실행

```
$ sudo chown -R $USER /usr/local/lib/node_modules
```

---

## License

Copyright © 2020 iKooB Inc. All Rights Reserved.
